//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class MyVouchers: Codable {
    var available                : [Offers]?
    var used                     : [Offers]?
    var expired                  : [Offers]?
}
