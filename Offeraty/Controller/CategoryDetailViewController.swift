//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class CategoryDetailViewController: BaseViewController {
    // MARK: - Constants
    let cellWidth = UIScreen.main.bounds.width - 40
    let sectionSpacing = Double(8.0)
    let cellSpacing = (0.1 / 16) * UIScreen.main.bounds.width
    
    @IBOutlet weak var backBtn: UIButton!
    
    
    //MARK: UIImageView Outlet
    @IBOutlet weak var profileImageView: UIImageView!
    
    //MARK: CollectionView Outlet
    @IBOutlet weak var todayDealCollectionView: UICollectionView!
    @IBOutlet weak var limitedCollectionView: UICollectionView!
    @IBOutlet weak var allOffersCollectionView: UICollectionView!
    @IBOutlet weak var tableV: UITableView!
    
    
    //MARK: UIView Outlets
    @IBOutlet weak var allOffView: UIView!
    @IBOutlet weak var limitedView: UIView!
    @IBOutlet weak var dealOfDayView: UIView!
    //MARK: UILabel Outlets
    @IBOutlet weak var recommendedLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var offersObject : Offers?
    var allOffersArr = [Offers]()
    var recommendedOffersArr = [Offers]()
    
    let group = DispatchGroup()
    var user = Cache.extractUserObject(keyCurrentUser)
    
    var recommendedTotalPages = 0
    var allOfferTotalPages = 0
    var originalHeaderSize = CGFloat(477)
    
    var recommendedPageNumber = 0
    var allOfferPageNumber = 0
    
    let refreshControl = UIRefreshControl()
    
    var expired = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.register(UINib(nibName: "FavouriteCell", bundle: nil), forCellReuseIdentifier: "FavouriteCell")
        let layout = PagingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(sectionSpacing))
        layout.itemSize = CGSize(width: cellWidth, height: 110)
        layout.minimumLineSpacing = 8
        limitedCollectionView.collectionViewLayout = layout
        limitedCollectionView.decelerationRate = .fast
        addReloadView()
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        OfferatySingleton.shared.showHud()
        getOffers(forPagination: false)
        getAllOffers(forPagination: false)
        self.group.notify(queue: .main, execute: { // executed after all async calls finish
            print("done with all async calls")
            if self.expired {
                self.expired = false
                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                if (guestCheck ?? false) {
                    self.logOut()
                } else {
                    self.getRefreshedToken(from: "get", id: 0)
                }
            } else {
                self.setHeightOfHeader()
            }
            OfferatySingleton.shared.hideHud()
        })
    }
    
    func setHeightOfHeader() {
        var height: CGFloat = 0.0
        if (self.offersObject?.deal_of_the_day?.count ?? 0) > 0 {
            self.dealOfDayView.visibility = .visible
            height = height + 162
        } else {
            self.dealOfDayView.visibility = .gone
        }
        if (self.offersObject?.limited_time_offer?.count ?? 0) > 0 {
            self.limitedView.visibility = .visible
            height = height + 148
        } else {
            self.limitedView.visibility = .gone
        }
        if (self.allOffersArr.count) > 0 {
            self.allOffView.visibility = .visible
            height = height + 160
        } else {
            self.allOffView.visibility = .gone
        }
        if (self.recommendedOffersArr.count) > 0 {
            self.recommendedLabel.visibility = .visible
        } else {
            self.recommendedLabel.visibility = .gone
        }
        height = height + 38
        self.tableV.tableHeaderView?.frame.size = CGSize(width: self.tableV.frame.width, height: height)
        self.animateChanges()
        UIView.performWithoutAnimation {
            self.todayDealCollectionView.reloadData()
            self.limitedCollectionView.reloadData()
            self.allOffersCollectionView.reloadData()
            self.tableV.reloadData()
        }
        let indexPath = IndexPath(row: 0, section: 0)
        if self.offersObject?.deal_of_the_day?.indices.contains(0) ?? false {
            self.todayDealCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        self.viewWillLayoutSubviews()
    }
    
    func addReloadView() {
        refreshControl.tintColor = AppColor.appPrimaryColor
        let attributes = [NSAttributedString.Key.foregroundColor: AppColor.appPrimaryColor,NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 13.0)!] as [NSAttributedString.Key : Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh",attributes: attributes)
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableV.refreshControl = refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        offersObject = nil
        allOffersArr.removeAll()
        recommendedOffersArr.removeAll()
        recommendedPageNumber = 0
        allOfferPageNumber = 0
        getOffers(forPagination: false)
        getAllOffers(forPagination: false)
        self.group.notify(queue: .main, execute: { // executed after all async calls finish
            print("done with all async calls")
            if self.expired {
                self.expired = false
                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                if (guestCheck ?? false) {
                    self.logOut()
                } else {
                    self.getRefreshedToken(from: "get", id: 0)
                }
            } else {
                self.setHeightOfHeader()
            }
            self.refreshControl.endRefreshing()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.CategoryDetailViewController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        user = Cache.extractUserObject(keyCurrentUser)
        profileImageView.makeRoundedView()
        profileImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        profileImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name ?? "Guest".localized
        checkLangauge()
        hideNavigationBar()
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.backBtn.setImage(backRight, for: .normal)
        } else {
            self.backBtn.setImage(backLeft, for: .normal)
        }
    }
    
    func animateChanges() {
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func getAllOffers(forPagination: Bool)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            if !forPagination {
                self.group.leave()
            }
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            var params = [String:AnyObject]()
            params["user_id"]  = (user?.user_id ?? 0) as AnyObject
            if !forPagination {
                group.enter()
            }
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.allOffers)/\(lang)?page=\(allOfferPageNumber)&size=10", params: params, shouldAddLoader: false,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let arr = try JSONDecoder().decode([Offers].self, from: userData)
                        if let totalPages = json[keyTotalPages] as? Int {
                            self?.allOfferTotalPages = totalPages
                        }
                        for i in arr {
                            self?.allOffersArr.append(i)
                        }
                        if forPagination {
                            UIView.performWithoutAnimation {
                                self?.allOffersCollectionView.reloadData()
                            }
                        } else {
                            self?.group.leave()
                        }
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        self?.group.leave()
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                self.allOffersCollectionView.reloadData()
                OfferatySingleton.shared.hideHud()
                if !forPagination {
                    self.group.leave()
                }
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                self.expired = true
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getRefreshedToken(from: String, id:Int?)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "unfav" {
                                self?.callUnFavouriteAPI(offerID: id)
                            }  else if from == "fav" {
                                self?.callFavouriteAPI(offerID: id)
                            } else {
                                self?.getData()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func getOffers(forPagination: Bool)  {
        if !(Utilities.isConnected()) {
            DispatchQueue.main.async {
                Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
                if !forPagination {
                    self.group.leave()
                }
            }
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            DispatchQueue.main.async {
                if !forPagination {
                    self.group.enter()
                }
            }
            let url = "\(APIConstants.homeItems)/\(lang)?page=\(recommendedPageNumber)"
            var params = [String:AnyObject]()
            params["user_id"]  = (user?.user_id ?? 0) as AnyObject
            OffaratyAPIManager.request(withAPIName: "\(url)&size=10&recomended_offers=true&deal_of_the_day=true&limited_time_offer=true", params: params, shouldAddLoader: false,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let obj = try JSONDecoder().decode(Offers.self, from: userData)
                        self?.offersObject = obj
                        DispatchQueue.main.async {
                            if let totalPages = json[keyTotalPages] as? Int {
                                self?.recommendedTotalPages = totalPages
                            }
                            for i in self?.offersObject?.recomended_offers ?? [] {
                                self?.recommendedOffersArr.append(i)
                            }
                            if forPagination {
                                UIView.performWithoutAnimation {
                                    self?.tableV.reloadData()
                                }
                            } else {
                                self?.group.leave()
                            }
                        }
                    }
                    
                    catch {
                        DispatchQueue.main.async {
                            print("could not decode \(error)")
                            self?.group.leave()
                        }
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                DispatchQueue.main.async {
                    self.tableV.reloadData()
                    OfferatySingleton.shared.hideHud()
                    if !forPagination {
                        self.group.leave()
                    }
                    if let detail = errorResponse?[keyMessage] as? String {
                        if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                            if let code = errorResponse?[keyCode] as? Int {
                                if code == 401 || code == 407 || code == 409 {
                                    self.logOut()
                                } else if code == 403 {
                                    self.expired = true
                                }
                            } else {
                                self.logOut()
                            }
                        } else {
                            self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                        }
                    } else if let msg = errorResponse?[keyMessage] as? String {
                        self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                    }
                }
            }
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func viewAllBtn(_ sender: Any) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllOffersViewController") as? AllOffersViewController
        {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    func callFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        var params = [String:AnyObject]()
        params["user_id"]  = (user?.user_id ?? 0) as AnyObject
        params["offer_id"]  = (offerID ?? 0) as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.favourite, params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = Key.Messages.AddedToFav.localized
                    popVC.subTitleT = Key.Messages.OfferAddedFavourites.localized
                    popVC.popUpType = .double
                    popVC.additionalCheck = "fav"
                    popVC.popUpImage = UIImage(named: "added to favourite")
                    popVC.btnText1 = Key.Messages.GoToFav.localized
                    popVC.btnText2 = Key.Messages.CloseTitle.localized
                    popVC.delegate = self
                    self?.tabBarController?.present(popVC, animated: true)
                }
                if let index = self?.recommendedOffersArr.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.recommendedOffersArr[index].favourite = true
                    UIView.performWithoutAnimation {
                        self?.tableV.reloadData()
                    }
                }
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "fav", id: offerID)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func callUnFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        let params = [String:AnyObject]()
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.unFavourite)?user_id=\(user?.user_id ?? 0)&offer_id=\(offerID ?? 0)", params: params,shouldAddLoader: true,isDeleteRequest: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                Utilities.showALertWithTag(title: Key.Messages.FavouriteTitle.localized, message: Key.Messages.OfferRemovedFavourites.localized)
                if let index = self?.recommendedOffersArr.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.recommendedOffersArr[index].favourite = false
                    UIView.performWithoutAnimation {
                        self?.tableV.reloadData()
                    }
                }
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "unfav", id: offerID)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
}
extension CategoryDetailViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == todayDealCollectionView {
            return self.offersObject?.deal_of_the_day?.count ?? 0
        } else if collectionView == limitedCollectionView {
            return self.offersObject?.limited_time_offer?.count ?? 0
        } else if collectionView == allOffersCollectionView {
            return self.allOffersArr.count
        } else {
            return self.recommendedOffersArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == todayDealCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "todayCell", for: indexPath) as! InterestCell
            cell.loadDeal(dealObject: self.offersObject?.deal_of_the_day?[indexPath.row])
            return cell
        } else if collectionView == limitedCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "limitedCell", for: indexPath) as! LimitedTimeCell
            cell.delegate = self
            cell.loadDeal(dealObject: self.offersObject?.limited_time_offer?[indexPath.row])
            return cell
        } else if collectionView == allOffersCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllOfferCell", for: indexPath) as! AllOfferCell
            cell.loadOffer(offer: self.allOffersArr[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealCell", for: indexPath) as! DealCell
            cell.delegate = self
            cell.loadOffer(offer: self.recommendedOffersArr[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Locked".localized
                popVC.subTitleT = "Login to see offer details".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                popVC.btnText1 = "Login".localized
                popVC.btnText2 = "Cancel".localized
                popVC.delegate = self
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            if collectionView == todayDealCollectionView {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
                {
                    secondViewController.dealOfTheDayObject = self.offersObject?.deal_of_the_day?[indexPath.row]
                    secondViewController.offerID = "\(offersObject?.deal_of_the_day?[indexPath.row].offer_id ?? 0)"
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            } else if collectionView == limitedCollectionView {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
                {
                    secondViewController.dealOfTheDayObject = self.offersObject?.limited_time_offer?[indexPath.row]
                    secondViewController.offerID = "\(offersObject?.limited_time_offer?[indexPath.row].offer_id ?? 0)"
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            } else if collectionView == allOffersCollectionView {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
                {
                    secondViewController.offerObject = self.allOffersArr[indexPath.row]
                    secondViewController.offerID = "\(allOffersArr[indexPath.row].offer_id ?? 0)"
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            } else {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
                {
                    secondViewController.offerObject = self.recommendedOffersArr[indexPath.row]
                    secondViewController.offerID = "\(recommendedOffersArr[indexPath.row].offer_id ?? 0)"
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            }
        }
    }
    
    func checkPageAndCallApiCollection() {
        if allOfferPageNumber < allOfferTotalPages {
            allOfferPageNumber = allOfferPageNumber + 1
            getAllOffers(forPagination: true)
        }
    }
    
    func checkPageAndCallApiTable() {
        if recommendedPageNumber < recommendedTotalPages {
            recommendedPageNumber = recommendedPageNumber + 1
            DispatchQueue.global().async {
                self.getOffers(forPagination: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == limitedCollectionView {
            return CGSize(width: collectionView.frame.size.width, height: 102)
        } else if collectionView == allOffersCollectionView {
            let yourWidth = CGFloat(209)
            let yourHeight = CGFloat(98)
            
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else {
            if (Utilities.getIsArabic() ?? false) {
                return CGSize(width: CGFloat(77), height: CGFloat(110))
            } else {
                return CGSize(width: CGFloat(77), height: CGFloat(102))
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == allOffersCollectionView {
            return 20
        } else if collectionView == limitedCollectionView {
            return cellSpacing
        } else {
            return 0
        }
    }
    
}
extension CategoryDetailViewController: DealCellDelegate {
    func favBtnTapped(offer: Offers?) {
        if offer?.favourite ?? false {
            callUnFavouriteAPI(offerID: offer?.offer_id)
        } else {
            callFavouriteAPI(offerID: offer?.offer_id)
        }
    }
}
extension CategoryDetailViewController: LimitedTimeCellDelegate {
    func offerButtonTapped(offer: DealOfTheDay?) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Locked".localized
                popVC.subTitleT = "Login to see offer details".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                popVC.btnText1 = "Login".localized
                popVC.btnText2 = "Cancel".localized
                popVC.delegate = self
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
            {
                secondViewController.fromLimited = true
                secondViewController.dealOfTheDayObject = offer
                secondViewController.offerID = "\(offer?.offer_id ?? 0)"
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
}
extension CategoryDetailViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recommendedOffersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.loadOffer(offer: self.recommendedOffersArr[indexPath.row], fromFav: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Locked".localized
                popVC.subTitleT = "Login to see offer details".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                popVC.btnText1 = "Login".localized
                popVC.btnText2 = "Cancel".localized
                popVC.delegate = self
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
            {
                secondViewController.offerObject = self.recommendedOffersArr[indexPath.row]
                secondViewController.offerID = "\(recommendedOffersArr[indexPath.row].offer_id ?? 0)"
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if indexPath.row == ((self.recommendedOffersArr.count) - 1) {
    //
    //        }
    //    }
}
extension CategoryDetailViewController: AlertPopUpVCDelegate {
    func buttonTapped(type: ButtonTappedType, additional: String) {
        if type == .ok && additional == "" {
            Cache.removeAll()
            appDelegate.initRootView()
        } else if type == .ok {
            self.tabBarController?.selectedIndex = 2
        }
    }
}
extension CategoryDetailViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(self.allOffersCollectionView.contentOffset.x >= (self.allOffersCollectionView.contentSize.width - self.allOffersCollectionView.bounds.size.width)) {
            self.checkPageAndCallApiCollection()
        }
        if ((self.tableV.contentOffset.y + self.tableV.frame.size.height) >= self.tableV.contentSize.height)
        {
            DispatchQueue.main.async {
                self.checkPageAndCallApiTable()
            }
        }
    }
}
