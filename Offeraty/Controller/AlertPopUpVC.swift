//
//  AlertPopUpVC.swift
//  Offeraty
//
//  Created by Fuad Waheed on 20/09/2021.
//
enum PopUpButtonType {
    case single
    case double
}
enum ButtonTappedType {
    case ok
    case cancel
}
protocol AlertPopUpVCDelegate: AnyObject {
    func buttonTapped(type: ButtonTappedType, additional: String)
}
import UIKit

class AlertPopUpVC: BaseViewController {
    
    //MARK: ImageView Outlets
    @IBOutlet weak var imageV: UIImageView!
    
    //MARK: Labels Outlets
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    
    //MARK: UIView Outlets
    @IBOutlet weak var twoButtonView: UIView!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var okBtnSingle: UIButton!
    @IBOutlet weak var okBtnDouble: UIButton!
    @IBOutlet weak var cancelBtnDouble: UIButton!
    
    //MARK: NSLayoutConstraints Outlets
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    var titleT = ""
    var subTitleT = ""
    var btnText1 = ""
    var btnText2 = ""
    var imHeight: CGFloat?
    var imWidth: CGFloat?
    var popUpImage : UIImage?
    var additionalCheck = ""
    var popUpType : PopUpButtonType = .single
    
    weak var delegate: AlertPopUpVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.text = titleT
        subHeadingLabel.text = subTitleT
        imageV.image = popUpImage
        if popUpType == .single {
            okBtnSingle.setTitle(btnText1, for: .normal)
        } else {
            twoButtonView.fadeIn()
            okBtnDouble.setTitle(btnText1, for: .normal)
            cancelBtnDouble.setTitle(btnText2, for: .normal)
        }
        if imHeight != nil && imWidth != nil {
            imageWidth.constant = imWidth!
            imageHeight.constant = imHeight!
        }
        if btnText1 == Key.Messages.GoToFav.localized {
            okBtnDouble.titleLabel?.font =  UIFont(name: "Effra-Regular", size: 17.0)!
            cancelBtnDouble.titleLabel?.font =  UIFont(name: "Effra-Regular", size: 17.0)!
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func okBtnSingleTap(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.buttonTapped(type: .ok, additional: self.additionalCheck)
        })
    }
    @IBAction func okBtnDoubleTap(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.buttonTapped(type: .ok, additional: self.additionalCheck)
        })
    }
    @IBAction func cancelBtnDoubleTap(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.buttonTapped(type: .cancel, additional: self.additionalCheck)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
    }
}
