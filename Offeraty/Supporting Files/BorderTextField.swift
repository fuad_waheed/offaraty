//
//  BorderTextField.swift
//  Offeraty
//
//  Created by Fuad Waheed on 16/03/2021.
//
import Foundation
@IBDesignable
open class BorderTextField: UITextField,UITextFieldDelegate {
    
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
    }
    required public override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.borderColor = AppColor.appTextFieldBorderColor.cgColor
    }
    public func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext {
            IQKeyboardManager.shared.goNext()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 5 {
            var check1 = false
            var check2 = false
            
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            check1 = allowedCharacters.isSuperset(of: characterSet)
            
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            check2 = newString.length <= maxLength
            
            if check1 && check2
            {
                if string == "" {
                    return true
                } else if textField.text?.count == 0 && string != "5"{
                    return false
                } else {
                    return true
                }
            }
            else
            {
                return false
            }
        }
        return true
    }
}
