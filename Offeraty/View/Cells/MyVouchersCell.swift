//
//  MyVouchersCell.swift
//  Offeraty
//
//  Created by Fuad Waheed on 02/06/2021.
//

import UIKit
protocol MyVouchersCellDelegate: AnyObject {
    func writeReviewTapped(offer: Offers?)
    func mainButtonTapped(offer: Offers?)
}

class MyVouchersCell: UITableViewCell {
    
    //MARK: UIImage Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var onlineStoreImage: UIImageView!
    @IBOutlet weak var dateImage: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var redeemedLabel: UILabel!
    
    //MARK: UIView Outlets
    @IBOutlet weak var mainView: UIView!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var reviewButton: UIButton!
    
    var offerObject: Offers?
    
    weak var delegate: MyVouchersCellDelegate?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadOffer(offer: Offers?, type:MyVoucherType, lang: String) {
        self.offerObject = offer
        productImageView.contentMode = .scaleToFill
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        productImageView.sd_setImage(with: URL(string: offer?.offer_image ?? ""),placeholderImage:placeIm)
        productImageView.makeRoundedView()
        brandNameLabel.text = offer?.offer_title
        productNameLabel.text = offer?.order_detail?.voucher_number
        if lang == "ar" {
            arrowImage.image = bigLeft
        } else {
            arrowImage.image = bigRight
        }
        switch type {
        case .available:
            let text = "Purchased on:".localized
            redeemedLabel.text = "\(text) \(getDateString(date: offer?.order_detail?.purchasedon))"
            arrowImage.alpha = 1
            reviewButton.alpha = 0
            if offer?.purchase_type == "online" {
                onlineStoreImage.alpha = 1
                if (offer?.can_review ?? true) {
                    reviewButton.alpha = 1
                } else {
                    reviewButton.alpha = 0
                }
            } else {
                onlineStoreImage.alpha = 0
            }
            dateImage.image = UIImage(named: "icon-valid")
        case .used:
            let text = "Redeemed on:".localized
            redeemedLabel.text = "\(text) \(getDateString(date: offer?.order_detail?.redeemedon))"
            arrowImage.alpha = 0
            reviewButton.alpha = 1
            reviewButton.setTitle("Write a review".localized, for: .normal)
            if offer?.purchase_type == "online" {
                reviewButton.alpha = 0
                onlineStoreImage.alpha = 1
            } else {
                onlineStoreImage.alpha = 0
                if (offer?.can_review ?? false) {
                    reviewButton.alpha = 1
                } else {
                    reviewButton.alpha = 0
                }
            }
            dateImage.image = UIImage(named: "icon redeemd")
        case .expired:
            dateImage.image = UIImage(named: "icon-valid")
            let text = "Expired on:".localized
            redeemedLabel.text = "\(text) \(offer?.end_date ?? "")"
            arrowImage.alpha = 0
            reviewButton.alpha = 0
        }
    }
    
    func getDateString(date: String?) -> String {
        var str = ""
        if date != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
            if let dateOb = formatter.date(from: date!) {
                formatter.dateFormat = "yyyy-MM-dd"
                str = formatter.string(from: dateOb)
                return str
            } else {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                guard let newDate = formatter.date(from: date!) else {
                    return ""
                }
                formatter.dateFormat = "yyyy-MM-dd"
                str = formatter.string(from: newDate)
                return str
            }
            
        }
        return str
    }
    
    @IBAction func writeReviewButton(_ sender: Any) {
        if delegate != nil {
            delegate?.writeReviewTapped(offer: self.offerObject)
        }
    }
    
    @IBAction func mainButtonTap(_ sender: Any) {
        if delegate != nil {
            delegate?.mainButtonTapped(offer: self.offerObject)
        }
    }
}
