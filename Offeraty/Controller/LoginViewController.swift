//
//  LoginViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 17/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit
import CoreLocation
import LocalAuthentication
import KeychainSwift
import FirebaseMessaging
import BEMCheckBox
import XLPagerTabStrip
import NewRelic

class LoginViewController: BaseViewController {
    
    //MARK: TextFields Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var tokenTextField: UITextField!
    
    //MARK: Labels Outlets
    @IBOutlet weak var SubHeadingLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var tokenLabel: UILabel!
    @IBOutlet weak var rememberMeLabel: UILabel!
    @IBOutlet weak var loginWithLabel: UILabel!
    
    //MARK: Buttons Outlets
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    @IBOutlet weak var eyeBtn: UIButton!
   
    //MARK: Images Outlets
    @IBOutlet weak var tokenIconImage: UIImageView!
    @IBOutlet weak var touchImage: UIImageView!
    @IBOutlet weak var eyeBtnImage: UIImageView!
    
    //MARK: Views Outlets
    @IBOutlet weak var orView: UIView!
    @IBOutlet weak var touchIdBtn: UIView!
    @IBOutlet weak var emailBorderLine: UIView!
    @IBOutlet weak var passBorderLine: UIView!
    @IBOutlet weak var tokenBorderLine: UIView!
    
    
    @IBOutlet weak var checkBox: BEMCheckBox!
    
    var offerID = ""
    var viewCheck = false
    let auth = BiometricIDAuth()
    let keyChain = KeychainSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkBox.boxType = .square
        initializeBiometric()
        #if DEBUG
        self.emailTextField.text = "fuadwaheed@gmail.com"
        self.passwordTextField.text = "Fanta@12345"
        self.tokenTextField.text = "8975"
        #endif
        checkLangauge()
        getLocation()
        hideTabBar()
        viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    fileprivate func initializeBiometric() {
        if Utilities.getIsTouchIdEnabled() {
            touchIdBtn.alpha = 1
        } else {
            touchIdBtn.alpha = 0.4
        }
        let bioMetricType = auth.biometricType()
        if bioMetricType == .touchID {
            touchImage.image = UIImage(named: "icon-touchID")
            loginWithLabel.text = "Login with Touch ID".localized
        }
        else if bioMetricType == .faceID {
            touchImage.image = UIImage(named: "icon-Faceid")
            loginWithLabel.text = "Login with Face ID".localized
        }
//        let touchBool = auth.canEvaluatePolicy()
//        if touchBool {
//            if Utilities.getIsUserLoggedIn() && Utilities.getIsTouchIdEnabled() {
//                loginWithBioMetric()
//            }
//        }
    }
    func loginWithBioMetric() {
        
        self.view.endEditing(true)
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!".localized
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async { [weak self] in
                    if success {
                        print("biometric success")
                        if let userEmail = self?.keyChain.get(kUserEmail),let userPassword = self?.keyChain.get(kUserPassword),let token = self?.keyChain.get(kClientToken) {
                            self?.emailTextField.text = userEmail
                            self?.passwordTextField.text = userPassword
                            self?.tokenTextField.text = token
                            self?.callLoginAPI()
                        }
                    } else {
                        // If user cancels fingerprint or authentication is failed then control comes here
                    }
                }
            }
        } else {
            let bioMetricType = auth.biometricType()
            var currentBioMetric = ""
            if bioMetricType == .touchID {
                currentBioMetric = "Touch ID".localized
            }
            else if bioMetricType == .faceID {
                currentBioMetric = "Face ID".localized
            }
            let notAText = "not available".localized
            let configText = "Your device is not configured for".localized
            let alert = UIAlertController(title: "\(currentBioMetric) \(notAText)", message: "\(configText) \(currentBioMetric).", preferredStyle: UIAlertController.Style.alert)
            let ok = UIAlertAction(title: "Settings".localized, style: .default) { (action:UIAlertAction) in
                LoginViewController.self.openUrlString(urlString: UIApplication.openSettingsURLString, customErrorMessage: "Unable to open settings")
            }
            let close = UIAlertAction(title: Key.Messages.CloseTitle.localized, style: .default) { (action:UIAlertAction) in
            }
            alert.addAction(ok)
            alert.addAction(close)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    static func openUrlString(urlString:String,customErrorMessage:String) {
        if !urlString.isEmpty {
            if let url = URL(string: urlString){
                let application = UIApplication.shared
                if application.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:],
                                              completionHandler: {
                                                (success) in
                                                if success {
                                                    print("Opened: \(urlString)")
                                                }
                                                else {
                                                    print("Failed to open \(urlString)")
                                                }
                    })
                }
            }
            
        }
    }
    @IBAction func touchIdBtnTapped(_ sender: Any) {
        if touchIdBtn.alpha == 1 {
            self.loginWithBioMetric()
        } else {
            let bioMetricType = auth.biometricType()
            if bioMetricType == .touchID {
                Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: "Touch ID is not enabled. You can enable it from the settings. If you previously enabled it then it is disabled due to session expiry".localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Info)
            }
            else if bioMetricType == .faceID {
                Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: "Face ID is not enabled. You can enable it from the settings. If you previously enabled it then it is disabled due to session expiry".localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Info)
            }
            
        }
        
    }
    
    @IBAction func eyeBtn(_ sender: Any)
    {
        if(viewCheck)
        {
            viewCheck = false
            self.passwordTextField.isSecureTextEntry = true
            setImage(image: closeEye)
        }
        else
        {
            viewCheck = true
            self.passwordTextField.isSecureTextEntry = false
            setImage(image: openEye)
        }
    }
    
    func setImage(image: UIImage?) {
        UIView.transition(with: eyeBtn,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.eyeBtnImage.image = image  },
                          completion: nil)
    }
    
    func getLocation() {
        FLocationManager.shared.start { (info) in
            print(info.longitude ?? 0.0)
            print(info.latitude ?? 0.0)
            print(info.address ?? "")
            print(info.city ?? "")
            print(info.zip ?? "")
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.rememberMeLabel.textAlignment = .right
            self.emailTextField.textAlignment = .right
            self.passwordTextField.textAlignment = .right
            self.tokenTextField.textAlignment = .right
            self.nextBtn.setImage(goNext, for: .normal)
        } else {
            self.rememberMeLabel.textAlignment = .left
            self.emailTextField.textAlignment = .left
            self.passwordTextField.textAlignment = .left
            self.tokenTextField.textAlignment = .left
            self.nextBtn.setImage(goPrevious, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        offerID = Cache.extractString(keyDynamicLink) ?? ""
        let check = Cache.extractBool(keyRemeberCheck)
        if check ?? false {
            if let userEmail = self.keyChain.get(kUserEmail),let userPassword = self.keyChain.get(kUserPassword),let token = self.keyChain.get(kClientToken) {
                self.emailTextField.text = userEmail
                self.passwordTextField.text = userPassword
                self.tokenTextField.text = token
            }
            checkBox.setOn(true, animated: false)
        } else {
            checkBox.setOn(false, animated: false)
        }
    }
    
    @IBAction func forgotButtonTapped(_ sender: Any) {
        let actionsheet = UIAlertController(title:nil,message:nil,preferredStyle:UIAlertController.Style.actionSheet)
        
        let subview = actionsheet.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.backgroundColor = UIColor.white
        alertContentView.layer.cornerRadius = 15
        
        actionsheet.addAction(UIAlertAction(title: "Forgot Password".localized, style: .default, handler: { (action: UIAlertAction!) in
            self.goToForgot(tokenCheck: false)
        }))
        
        actionsheet.addAction(UIAlertAction(title: "Forgot Token".localized, style: .default, handler: { (action: UIAlertAction!) in
            self.goToForgot(tokenCheck: true)
        }))
        
        actionsheet.addAction(UIAlertAction(title: Key.Messages.CancelTitle.localized, style: .cancel, handler:nil))
        
        actionsheet.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
        actionsheet.popoverPresentationController?.sourceRect = (sender as! UIButton).frame
        
        self.present(actionsheet, animated: true) {
            print("option menu presented")
        }

    }
    
    func goToForgot(tokenCheck: Bool) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotViewController") as? ForgotViewController {
            secondViewController.tokenCheck = tokenCheck
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    @IBAction func guestButtonTapped(_ sender: Any) {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            callGuestAPI()
        }
    }
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        Utilities.showAlert(title: "", message: Key.Messages.EmployerTokenDescription.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Info)
    }
    @IBAction func check(_ sender: BEMCheckBox) {
        if(sender.on == true) {
            Cache.save(true, forKey: keyRemeberCheck)
            self.keyChain.set((self.emailTextField.text!), forKey: kUserEmail)
            self.keyChain.set((self.passwordTextField.text!), forKey: kUserPassword)
            self.keyChain.set((self.tokenTextField.text!), forKey: kClientToken)
        } else {
            Cache.save(false, forKey: keyRemeberCheck)
        }
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        
        
        if(self.verifyAllFields() == false){
            return
        }
        
        callLoginAPI()
        
    }
    
    func callLoginAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        var params = [String:AnyObject]()
        params["token"]  = tokenTextField.text as AnyObject
        params["email"]  = emailTextField.text as AnyObject
        params["password"] = passwordTextField.text as AnyObject
        params["push_token"] = token as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.login)/\(lang)", params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                do {
                    let userDataDic = try JSONDecoder().decode(User.self, from: userData)
                    Cache.saveUserObject(userDataDic, forKey: keyCurrentUser)
                    Cache.save(userDataDic.access_token ?? "", forKey: keyAccessToken)
                    Utilities.setIsUserLoggedIn(isUserLoggedIn: true)
                    if self?.emailTextField.text != self?.keyChain.get(kUserEmail) || self?.passwordTextField.text != self?.keyChain.get(kUserPassword) {
                        Utilities.setIsTouchIdEnabled(isTouchIdEnabled: false)
                    }
                    self?.keyChain.set((self?.emailTextField.text!)!, forKey: kUserEmail)
                    self?.keyChain.set((self?.passwordTextField.text!)!, forKey: kUserPassword)
                    self?.keyChain.set((self?.tokenTextField.text!)!, forKey: kClientToken)
                    self?.showCategories(user: userDataDic)
                }
                
                catch {
                    print("could not decode \(error)")
                    Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func callGuestAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        
        var params = [String:AnyObject]()
        params["os"]  = "iOS" as AnyObject
        params["device_name"]  = UIDevice.modelName as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.guestLogin, params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                do {
                    let userDataDic = try JSONDecoder().decode(User.self, from: userData)
                    Cache.saveUserObject(userDataDic, forKey: keyCurrentUser)
                    Cache.save(true, forKey: kIsGuestLoggedIn)
                    Cache.save(userDataDic.access_token ?? "", forKey: keyAccessToken)
                    Utilities.setIsUserLoggedIn(isUserLoggedIn: true)
                    self?.goToHomeForGuest()
                }
                
                catch {
                    print("could not decode \(error)")
                    Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func goToHomeForGuest() {
        FirebaseUtility.screenEvent(.GuestLoginViewController)
        let mainStoryboard = UIStoryboard(name: "App", bundle: Bundle.main)
        if let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
        {
            vc.selectedIndex = 0
            if let navController = vc.viewControllers?[0] as? UINavigationController {
                if let controller = navController.topViewController as? HomeViewController {
                    controller.offerID = offerID
                }
            }
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
            
        }
    }
    
    func showCategories(user: User?) {
        FirebaseUtility.screenEvent(.LoginViewController)
        if user?.user_interests_list != nil && (user?.user_interests_list?.count ?? 0) > 0 {
            let mainStoryboard = UIStoryboard(name: "App", bundle: Bundle.main)
            if let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
            {
                vc.selectedIndex = 0
                if let navController = vc.viewControllers?[0] as? UINavigationController {
                    if let controller = navController.topViewController as? HomeViewController {
                        controller.offerID = offerID
                    }
                }
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.makeKeyAndVisible()
                
            }
        } else {
            let mainStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
            if let secondViewController = mainStoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController {
                secondViewController.offerID = offerID
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
    
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            return false
        }
        else if(self.emailTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.EmailError.localized)
            return false
        }
        else if(Utilities.isValidEmail(email: self.emailTextField.text!) == false){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.InvalidEmailError.localized)
            return false
        }
        else if(self.passwordTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.PassError.localized)
            return false
        }
        else if(self.tokenTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.TokenError.localized)
            return false
        }
        return true
    }
    
    
}
extension LoginViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        Cache.save(locValue.latitude, forKey: "lat")
        Cache.save(locValue.longitude, forKey: "long")
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailLabel.textColor = AppColor.appLoginSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            emailBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            passBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else if textField == passwordTextField {
            emailLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else {
            emailLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginSelected
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailLabel.textColor = AppColor.appLoginUnSelected
        passLabel.textColor = AppColor.appLoginUnSelected
        tokenLabel.textColor = AppColor.appLoginUnSelected
        emailBorderLine.backgroundColor = AppColor.appBorderGray
        passBorderLine.backgroundColor = AppColor.appBorderGray
        tokenBorderLine.backgroundColor = AppColor.appBorderGray
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tokenTextField {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension LoginViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "Login".localized)
    }
}
