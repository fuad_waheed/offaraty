//
//  Constants.swift
//  Offeraty
//
//  Created by Fuad Waheed on 22/03/2021.
//

import Foundation


//MARK: Image Constants
let mapIcon = UIImage(named: "map")
let listIcon = UIImage(named: "icon_list_view")
let backRight = UIImage(named: "next")
let backLeft = UIImage(named: "back")
let blueNext = UIImage(named: "blueNext")
let goNext = UIImage(named: "leftGo")
let goPrevious = UIImage(named: "rightGo")
let blueBack = UIImage(named: "blueBack")
let smallRight = UIImage(named: "rightArrow")
let smallLeft = UIImage(named: "leftArrow")
let bigRight = UIImage(named: "rightSlimArrow")
let bigLeft = UIImage(named: "leftSlimArrow")
let selectedCircleTick = UIImage(named: "selected")
let fav = UIImage(named: "Icon feather-heart-filled")
let notFav = UIImage(named: "Icon feather-heart-1")
let imagePlaceHolder = UIImage(named: "imagePlaceHolder")
let imagePlaceHolderArabic = UIImage(named: "imagePlaceHolder-arabic")
let dpPlaceHolder = UIImage(named: "dpPlaceHolder")
let googleApiKey = "AIzaSyBtBceouEfZgctljU21rVfYZJtYzBgAegg"
let openEye = UIImage(named: "Icon-eye-open")
let closeEye = UIImage(named: "Icon-eye-close-1")
