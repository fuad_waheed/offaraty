//
//  LocationTableCell.swift
//  AirMiles
//
//  Created by Waqar Khalid on 4/3/20.
//  Copyright © 2020 Merit Incentive. All rights reserved.
//

import UIKit

class ReviewTableCell: UITableViewCell {
    
    //MARK: UIImageView Outlets
    @IBOutlet weak var userImageImagView: UIImageView!
    
    //MARK: UILabels Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userLocationLabel: UILabel!
    @IBOutlet weak var userReviewLabel: UILabel!
    
    //MARK: RatingView Outlets
    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(isArabicCheck:Bool, obj: Reviews?) {
        self.userNameLabel.text = obj?.user_name
        let reviewOptionsStr = obj?.review_options?.map { String($0) }
            .joined(separator: ", ")
        self.userLocationLabel.text = reviewOptionsStr
        if self.userLocationLabel.text! == "" {
            self.userLocationLabel.text = "Label"
            self.userLocationLabel.alpha = 0
        } else {
            self.userLocationLabel.alpha = 1
        }
        self.userReviewLabel.text = obj?.review
        self.ratingView.rating = Double(obj?.rating ?? 0)
        ratingView.isUserInteractionEnabled = false
        if (Utilities.getIsArabic() ?? false) {
            self.ratingView.semanticContentAttribute = .forceRightToLeft
        } else {
            self.ratingView.semanticContentAttribute = .forceLeftToRight
        }
    }

}
