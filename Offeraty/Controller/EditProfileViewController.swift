//
//  ProfileViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 02/06/2021.
//

import UIKit
import TOCropViewController
import TagsList

class EditProfileViewController: BaseViewController {

    //MARK: UIImage Outlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userImageBg: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    //MARK: UIView Outlets
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var userNaemBorderLine: UIView!
    @IBOutlet weak var emailBorderLine: UIView!
    @IBOutlet weak var phoneBorderLine: UIView!
    @IBOutlet weak var mainView: UIView!
    var user = Cache.extractUserObject(keyCurrentUser)
    private var croppingStyle = TOCropViewCroppingStyle.circular
    
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    var randomStrings: [String] = [
        "Donec", "consectetur", "metus", "non", "sollicitudin", "condimentum",
        "Cras sodales lobortis neque at scelerisque", "Proin tempor eleifend massa", "Curabitur",
        "semper tortor eget"
    ]
    var dataSource: DefaultTagsListDataSource!
    var tagsListView: TagsListProtocol = TagsList()
    
    var image : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        userImageBg.contentMode = .scaleAspectFill
        tagView.addSubview(tagsListView)
        tagsListView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tagsListView.centerXAnchor.constraint(equalTo: tagView.centerXAnchor),
            tagsListView.leadingAnchor.constraint(equalTo: tagView.leadingAnchor, constant: 8),
            tagsListView.trailingAnchor.constraint(equalTo: tagView.trailingAnchor, constant: -8),
            tagsListView.topAnchor.constraint(equalTo: tagView.topAnchor, constant: 8),
            tagsListView.bottomAnchor.constraint(equalTo: tagView.bottomAnchor, constant: -8)
        ])
        tagsListView.itemsConfiguration.sideImageEverytimeDisplaying = false
        tagsListView.itemsConfiguration.xButtonEverytimeDisplaying = false
        tagsListView.minimumInteritemSpacing = 15
        tagsListView.itemsConfiguration.borderMarginHorizontal = 10
        tagsListView.contentOrientation = .verticalSizeToFit
        dataSource = DefaultTagsListDataSource(tagsListView)

        tagsListView.tagsListDelegate = self
    }
    
    func addPlusBtn() {
        let item = TagViewItem()
        item.title = "Add More".localized
        item.backgroundColor = AppColor.appGreenColor
        item.titleColor = .white
        item.xButtonBackgroundColor = .clear
        item.titleFont = UIFont(name: "Effra-Regular", size: 12.0)!
        item.xButtonImage = UIImage(named: "plusBtn")
        item.xButtonDisplaying = true
        dataSource.appendTag(item)
    }
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            nameLabel.textAlignment = .right
            emailLabel.textAlignment = .right
            mobileLabel.textAlignment = .right
            self.userNameField.textAlignment = .right
            self.emailField.textAlignment = .right
            self.mobileField.textAlignment = .right
            self.backButton.setImage(backRight, for: .normal)
        } else {
            nameLabel.textAlignment = .left
            emailLabel.textAlignment = .left
            mobileLabel.textAlignment = .left
            self.userNameField.textAlignment = .left
            self.emailField.textAlignment = .left
            self.mobileField.textAlignment = .left
            self.backButton.setImage(backLeft, for: .normal)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        showTabBar()
        getUserData()
        loadUserData()
        checkLangauge()
    }
    
    func getUserData()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.getUser)/\(lang)?user_id=\(user?.user_id ?? 0)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let obj = try JSONDecoder().decode(User.self, from: userData)
                        Cache.saveUserObject(obj, forKey: keyCurrentUser)
                        self?.loadUserData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "get")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func loadUserData() {
        user = Cache.extractUserObject(keyCurrentUser)
        dataSource.removeAllTags()
        for i in user?.user_interests_list ?? [] {
            let item = TagViewItem()
            item.title = i.category?.name
            item.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1.00)
            item.titleColor = .darkGray
            item.titleFont = UIFont(name: "Effra-Regular", size: 12.0)!
            item.xButtonImage = UIImage(named: "crossBtn")
            item.xButtonBackgroundColor = .clear
            item.xButtonDisplaying = false
            dataSource.appendTag(item)
        }
        addPlusBtn()
        self.userNameLabel.text = user?.user_name
        self.userNameField.text = user?.user_name
        self.emailField.text = user?.email
        self.mobileField.text = user?.mobile?.replacingOccurrences(of: "+966", with: "")
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userImageBg.sd_setImage(with: URL(string: user?.user_image ?? ""))
    }
    
    @IBAction func editProfilePictureButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            print("Image Tapped")
            
            // image source alert
            let imagePickController = UIImagePickerController()
            imagePickController.delegate = self
            
            let actionsheet = UIAlertController(title:nil,message:nil,preferredStyle:UIAlertController.Style.actionSheet)
            
            let subview = actionsheet.view.subviews.first! as UIView
            let alertContentView = subview.subviews.first! as UIView
            alertContentView.backgroundColor = UIColor.white
            alertContentView.layer.cornerRadius = 15
            
            actionsheet.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { (action: UIAlertAction!) in
                
                if UIImagePickerController.isSourceTypeAvailable(.camera)
                {
                    imagePickController.sourceType = .camera
                    self.present(imagePickController, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertController(title:Key.Messages.WarningTitle.localized, message: Key.Messages.CameraNotAvailable.localized, preferredStyle:UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title:Key.Messages.OKTitle.localized,style: UIAlertAction.Style.default, handler:nil)); self.present(alert, animated:true, completion:nil)
                }
                
                
            }))
            
            actionsheet.addAction(UIAlertAction(title: "Photo Library".localized, style: .default, handler: { (action: UIAlertAction!) in imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
            }))
            
            actionsheet.addAction(UIAlertAction(title: Key.Messages.CancelTitle.localized, style: .cancel, handler:nil))
            
            actionsheet.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
            actionsheet.popoverPresentationController?.sourceRect = (sender as! UIButton).frame
            
            self.present(actionsheet, animated: true) {
                print("option menu presented")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.UpdateProfileViewController)
    }
    
    @IBAction func saveProfileButtonTapped(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        
        callUpdateAPI()
    }
    
    func callUpdateAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        let number = "+966" + mobileField.text!
        var params = [String:AnyObject]()
        params["user_id"]  = "\(user?.user_id ?? 0)" as AnyObject
        params["mobile"]  = number as AnyObject
        params["user_name"] = userNameField.text as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.updateProfile)/\(lang)", params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:image, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.getUserData()
                Utilities.showAlert(title: "", message: "Profile Successfully Updated.".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "up")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }

        }
    }
    
    func getRefreshedToken(from: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "get" {
                                self?.getUserData()
                            } else {
                                self?.callUpdateAPI()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func verifyAllFields() -> Bool {
        
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            return false
        }
        else if(self.userNameField.text?.isEmpty == true) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.UsernameError.localized)
            return false
        }
        else if(self.mobileField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.MobileError.localized)
            return false
        }
        return true
    }

}
extension EditProfileViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)
        {
            self.userImageView.image = image
            self.userImageBg.image = image
            self.userImageView.makeRoundedView()
            self.image = image
            picker.dismiss(animated: true, completion: nil)
//            let cropViewController = TOCropViewController(croppingStyle: croppingStyle,image: image)
//            cropViewController.delegate = self
//            cropViewController.aspectRatioPreset = .presetSquare; //Set the initial aspect ratio as a square
//            cropViewController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
//            cropViewController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
//            cropViewController.aspectRatioPickerButtonHidden = true
//
//            if croppingStyle == .circular {
//                if picker.sourceType == .camera {
//                    picker.dismiss(animated: true, completion: {
//                        self.present(cropViewController, animated: true, completion: nil)
//                    })
//                } else {
//                    picker.pushViewController(cropViewController, animated: true)
//                }
//            }
//            else { //otherwise dismiss, and then present from the main controller
//                picker.dismiss(animated: true, completion: {
//                    self.present(cropViewController, animated: true, completion: nil)
//                    //self.navigationController!.pushViewController(cropController, animated: true)
//                })
//            }
        }
        else
        {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: "Something wrong with the camera!".localized)
        }
        
        
    }
    
    
    //dismiss source picker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {

        picker.dismiss(animated: true, completion: nil)

    }
//
//    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int)
//    {
//        // 'image' is the newly cropped version of the original image
//
//        updateImageViewWithImage(image, fromCropViewController: cropViewController)
//
//    }
//
//    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
//
//        updateImageViewWithImage(image, fromCropViewController: cropViewController)
//    }
//
//    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: TOCropViewController)
//    {
//        self.userImageView.image = image
//        self.userImageBg.image = image
//        self.userImageView.makeRoundedView()
//        self.image = image
//        cropViewController.dismiss(animated: true, completion: nil)
//    }
}
extension EditProfileViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == userNameField {
            nameLabel.textColor = AppColor.appLoginSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            mobileLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            emailBorderLine.backgroundColor = AppColor.appProfileBorderGray
            phoneBorderLine.backgroundColor = AppColor.appProfileBorderGray
        } else if textField == emailField {
            nameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginSelected
            mobileLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appProfileBorderGray
            emailBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            phoneBorderLine.backgroundColor = AppColor.appProfileBorderGray
        } else {
            nameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            mobileLabel.textColor = AppColor.appLoginSelected
            userNaemBorderLine.backgroundColor = AppColor.appProfileBorderGray
            emailBorderLine.backgroundColor = AppColor.appProfileBorderGray
            phoneBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        nameLabel.textColor = AppColor.appLoginUnSelected
        emailLabel.textColor = AppColor.appLoginUnSelected
        mobileLabel.textColor = AppColor.appLoginUnSelected
        userNaemBorderLine.backgroundColor = AppColor.appProfileBorderGray
        emailBorderLine.backgroundColor = AppColor.appProfileBorderGray
        phoneBorderLine.backgroundColor = AppColor.appProfileBorderGray
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext {
            IQKeyboardManager.shared.goNext()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobileField {
            var check1 = false
            var check2 = false
            
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            check1 = allowedCharacters.isSuperset(of: characterSet)
            
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            check2 = newString.length <= maxLength
            
            if check1 && check2
            {
                if string == "" {
                    return true
                } else if textField.text?.count == 0 && string != "5"{
                    return false
                } else {
                    return true
                }
            }
            else
            {
                return false
            }
        }
        return true
    }
}
extension EditProfileViewController: TagsListDelegate {
    func tagsListCellTouched(_ TagsList: TagsListProtocol, index: Int) {
        print("\nCell with index: \(index) was touched")
    }

    func tagsListCellXButtonTouched(_ TagsList: TagsListProtocol, index: Int) {
        print("\nxButton from cell with tag index: \(index) was touched")
        if index == user?.user_interests_list?.count ?? 0 {
            addPlusBtn()
            let mainStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
            if let secondViewController = mainStoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController {
                secondViewController.editCheck = true
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
}
