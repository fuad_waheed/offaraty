//
//  AppDelegate.swift
//  Offeraty
//
//  Created by Fuad Waheed on 16/03/2021.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseDynamicLinks
import Siren
import UserNotifications
import NewRelic

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigationController: AppNavigationController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setDomainEnironment()
        NRLogger.setLogLevels(NRLogLevelALL.rawValue)
        NewRelic.enableFeatures([.NRFeatureFlag_CrashReporting,.NRFeatureFlag_InteractionTracing,.NRFeatureFlag_NetworkRequestEvents,.NRFeatureFlag_DefaultInteractions,.NRFeatureFlag_ExperimentalNetworkingInstrumentation,.NRFeatureFlag_RequestErrorEvents])
        if AppConstants.isDemoMode {
            NewRelic.start(withApplicationToken:"eu01xx89bb97acbfb87cd4abfe763a687d619a3b01-NRMA")
        }
        else{
            NewRelic.start(withApplicationToken:"eu01xx6134cd3cf9afebf981e6a68c8e66fdcfc91d-NRMA")
        }
        
        
        FirebaseApp.configure()
        registerForPushNotifications()
        UNUserNotificationCenter.current().delegate = self
        GMSServices.provideAPIKey(googleApiKey)
        IQKeyboardManager.shared.enable = true
        window = UIWindow.init(frame: UIScreen.main.bounds)
        var style = ToastStyle()
        style.messageColor = UIColor.white
        style.messageAlignment = .center
        style.backgroundColor = AppColor.appPrimaryColor
        ToastManager.shared.style = style
        if (Utilities.getIsArabic() ?? false)  {
            PPLocalization.sharedInstance.setLanguage(language: "ar")
        }else {
            PPLocalization.sharedInstance.setLanguage(language: "en")
        }
        initRootView()
        checkVersion()
        return true
    }
    
    func checkVersion()
    {
        let siren = Siren.shared
        siren.rulesManager = RulesManager(globalRules: .critical)
        siren.apiManager = APIManager(countryCode: "SA")
        siren.wail()
    }
    
    func getlanguageDirection() {
        let dir = PPLocalization().getlanguageDirection()
        if  dir == .leftToRight {
            let semantic: UISemanticContentAttribute = .forceLeftToRight
            UITabBar.appearance().semanticContentAttribute = semantic
            UIView.appearance().semanticContentAttribute = semantic
            UITextView.appearance().semanticContentAttribute = semantic
            UITextField.appearance().semanticContentAttribute = semantic
            UIButton.appearance().semanticContentAttribute = semantic
            UINavigationBar.appearance().semanticContentAttribute = semantic
            self.navigationController.navigationBar.semanticContentAttribute  = semantic
        }
        else {
            let semantic: UISemanticContentAttribute = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute = semantic
            UIView.appearance().semanticContentAttribute = semantic
            UITextView.appearance().semanticContentAttribute = semantic
            UITextField.appearance().semanticContentAttribute = semantic
            UIButton.appearance().semanticContentAttribute = semantic
            UINavigationBar.appearance().semanticContentAttribute = semantic
            self.navigationController.navigationBar.semanticContentAttribute  = semantic
        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
    }
    func setDomainEnironment()  {
    #if Staging
        AppConstants.isDemoMode = true
        
    #else
        AppConstants.isDemoMode = false
        
    #endif
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func initRootView() {
        let user = Cache.extractUserObject(keyCurrentUser)
        if user == nil {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
            if let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: "IntroViewController") as? IntroViewController
            {
                navigationController = AppNavigationController.init(rootViewController: loginPageView)
                navigationController?.setNavigationBarHidden(true, animated: false)
                window?.rootViewController = navigationController
            }
            self.getlanguageDirection()
        } else {
            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
            if (guestCheck ?? false) {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "App", bundle: nil)
                if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                {
                    navigationController = AppNavigationController.init(rootViewController: tabBar)
                    navigationController?.setNavigationBarHidden(true, animated: false)
                    window?.rootViewController = navigationController
                }
            } else if user?.user_interests_list == nil || (user?.user_interests_list?.count ?? 0) == 0 {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
                if let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
                {
                    navigationController = AppNavigationController.init(rootViewController: loginPageView)
                    navigationController?.setNavigationBarHidden(true, animated: false)
                    window?.rootViewController = navigationController
                }
            } else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "App", bundle: nil)
                if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                {
                    navigationController = AppNavigationController.init(rootViewController: tabBar)
                    navigationController?.setNavigationBarHidden(true, animated: false)
                    window?.rootViewController = navigationController
                }
            }
            self.getlanguageDirection()
        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
    }
}
extension AppDelegate {
    func applicationWillTerminate(_ application: UIApplication) {
        Cache.remove(forKey: keyDynamicLink)
    }
}
extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        print("Custom url spotted = \(url.absoluteString)")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
        {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }
        else{
            return false
        }
        
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        if let incomingUrl = userActivity.webpageURL
        {
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl, completion:
                                                                                { (dynamicLink,error) in
                                                                                    guard error == nil else
                                                                                    {
                                                                                        print("Found an error \(error?.localizedDescription ?? "")")
                                                                                        return
                                                                                    }
                                                                                    if let dynamicLink = dynamicLink
                                                                                    {
                                                                                        self.handleIncomingDynamicLink(dynamicLink)
                                                                                    }
                                                                                    
                                                                                })
            if linkHandled
            {
                return true
            }
            else{
                return false
            }
        }
        
        return false
    }
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        if dynamicLink.matchType == .unique
        {
            print("Unique")
        }
        
        guard let url = dynamicLink.url else {
            print("dynamic link url not correct")
            return
        }
        
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false),let queryItems = components.queryItems else { return }
        
        if components.path == "/mobile"
        {
            if let offerIDQueryItem = queryItems.first(where: {$0.name == "offerID"})
            {
                guard let offerID = offerIDQueryItem.value else {
                    return
                }
                let user = Cache.extractUserObject(keyCurrentUser)
                if user == nil {
                    Cache.save(offerID, forKey: keyDynamicLink)
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
                    if let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: "IntroViewController") as? IntroViewController
                    {
                        navigationController = AppNavigationController.init(rootViewController: loginPageView)
                        navigationController?.setNavigationBarHidden(true, animated: false)
                        window?.rootViewController = navigationController
                    }
                } else {
                    let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                    if (guestCheck ?? false) {
                        Utilities.showAlert(title: "", message: "Login to see offer details".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                        {
                            navigationController = AppNavigationController.init(rootViewController: tabBar)
                            navigationController?.setNavigationBarHidden(true, animated: false)
                            window?.rootViewController = navigationController
                        }
                        
                    } else if user?.user_interests_list == nil || (user?.user_interests_list?.count ?? 0) == 0 {
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
                        if let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
                        {
                            navigationController = AppNavigationController.init(rootViewController: loginPageView)
                            loginPageView.offerID = offerID
                            navigationController?.setNavigationBarHidden(true, animated: false)
                            window?.rootViewController = navigationController
                        }
                    } else {
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                        {
                            navigationController = AppNavigationController.init(rootViewController: tabBar)
                            if let navController = tabBar.viewControllers?[0] as? UINavigationController {
                                if let controller = navController.topViewController as? HomeViewController {
                                    controller.offerID = offerID
                                }
                            }
                            navigationController?.setNavigationBarHidden(true, animated: false)
                            window?.rootViewController = navigationController
                        }
                    }
                }
            }
        }
        
        print("Dynamic URL received = ..\(url.absoluteString)")
    }
}
extension AppDelegate:UNUserNotificationCenterDelegate
{
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let dict = notification.request.content.userInfo as NSDictionary
        print("Handle push from background or closed\(notification.request.content.userInfo)")
        print(dict)
        if let notifDict = dict.object(forKey: "aps") as? NSDictionary {
            if let additionalDict = notifDict.object(forKey: "alert") as? NSDictionary {
                var titleText = ""
                var bodyText = ""
                if let title = additionalDict.object(forKey: "title") as? String {
                    titleText = title
                }
                if let body = additionalDict.object(forKey: "body") as? String {
                    bodyText = body
                }
                if titleText != "" && bodyText != "" {
                    Utilities.showNotification(withTitle: titleText, withMessage: bodyText)
                }
            }
        }
        completionHandler([.badge, .sound])
    }
    
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("Handle push from background or closed\(response.notification.request.content.userInfo)")
        let dict = response.notification.request.content.userInfo as NSDictionary
        var screenName = ""
        var notifOfferID = ""
        if let screen = dict.object(forKey: "title") as? String {
            screenName = screen
        }
        if let offerID = dict.object(forKey: "body") as? String {
            notifOfferID = offerID
        }
        let user = Cache.extractUserObject(keyCurrentUser)
        if user != nil {
            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
            if (guestCheck ?? false) {
                Utilities.showAlert(title: "", message: "Login to see offer details".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                {
                    navigationController = AppNavigationController.init(rootViewController: tabBar)
                    navigationController?.setNavigationBarHidden(true, animated: false)
                    window?.rootViewController = navigationController
                }
                
            } else if user?.user_interests_list == nil || (user?.user_interests_list?.count ?? 0) == 0 {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "OnBoarding", bundle: nil)
                if let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
                {
                    navigationController = AppNavigationController.init(rootViewController: loginPageView)
                    loginPageView.offerID = notifOfferID
                    navigationController?.setNavigationBarHidden(true, animated: false)
                    window?.rootViewController = navigationController
                }
            } else {
                if screenName.contains("vouchers") || screenName.contains("rate") {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                    {
                        tabBar.selectedIndex = 3
                        navigationController = AppNavigationController.init(rootViewController: tabBar)
                        navigationController?.setNavigationBarHidden(true, animated: false)
                        window?.rootViewController = navigationController
                    }
                }
                else if screenName.contains("detail") {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    if let tabBar = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
                    {
                        navigationController = AppNavigationController.init(rootViewController: tabBar)
                        if let navController = tabBar.viewControllers?[0] as? UINavigationController {
                            if let controller = navController.topViewController as? HomeViewController {
                                controller.offerID = notifOfferID
                            }
                        }
                        navigationController?.setNavigationBarHidden(true, animated: false)
                        window?.rootViewController = navigationController
                    }
                }
            }
        }
        completionHandler()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print(userInfo as NSDictionary)
        
        
    }
}
