//
//  ForgotViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 03/06/2021.
//

import UIKit
import OTPFieldView
import KeychainSwift
import GradientCircularProgress

protocol OTPVerificationVCDelegate: AnyObject {
    func goToLogin()
}

class OTPVerificationVC: BaseViewController {
    
    //MARK: TextFields Outlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    
    
    //MARK: Images Outlets
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var confimPassLabel: UILabel!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var resendBtn: UIButton!
    
    //MARK: OTPField Outlets
    @IBOutlet weak var otpField: OTPFieldView!
    
    @IBOutlet weak var passBorderLine: UIView!
    @IBOutlet weak var confirmPassBorderLine: UIView!
    
    @IBOutlet weak var eyeBtn1: UIButton!
    @IBOutlet weak var eyeBtn2: UIButton!
    @IBOutlet weak var resetPassBtn: UIButton!
    @IBOutlet weak var eyeBtn1Image: UIImageView!
    @IBOutlet weak var eyeBtn2Image: UIImageView!
    
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var confirmPassView: UIView!
    
    weak var delegate: OTPVerificationVCDelegate?
    
    var viewCheck = false
    var viewCheck2 = false
    var bgTask = UIBackgroundTaskIdentifier(rawValue: 0)
    
    var params = [String:AnyObject]()
    let keyChain = KeychainSwift()
    var otpText = ""
    var email = ""
    
    var timerValue = 0.0
    
    var tokenCheck = false
    
    private var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var timer: Timer?
    var totalTime = 180
    
    @IBOutlet weak var progressViewParent: UIView!
    let progress = GradientCircularProgress()
    var progressView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        progressViewParent.backgroundColor = .clear
        progressView = progress.showAtRatio(frame: progressViewParent.frame, display: true, style: TimerStyle())
        progressViewParent.addSubview(progressView!)
        progressView?.center = progressViewParent.convert(progressViewParent.center, from:progressViewParent.superview)
        self.progress.updateMessage(message: "")
        setUpView()
        checkLangauge()
        checkToken()
        hideResendBtn()
        startOtpTimer()
    }
    
    func checkToken() {
        if tokenCheck {
            resetPassBtn.setTitle("Confirm".localized, for: .normal)
            passView.visibility = .gone
            confirmPassView.visibility = .gone
        } else {
            resetPassBtn.setTitle("Reset Password".localized, for: .normal)
            passView.visibility = .visible
            confirmPassView.visibility = .visible
        }
    }
    
    @IBAction func eyeBtn(_ sender: Any)
    {
        if(viewCheck)
        {
            viewCheck = false
            self.passwordTextField.isSecureTextEntry = true
            setImage(image: closeEye, button: eyeBtn1Image)
        }
        else
        {
            viewCheck = true
            self.passwordTextField.isSecureTextEntry = false
            setImage(image: openEye, button: eyeBtn1Image)
        }
    }
    
    @IBAction func eyeBtn2(_ sender: Any)
    {
        if(viewCheck2)
        {
            viewCheck2 = false
            self.confirmPassTextField.isSecureTextEntry = true
            setImage(image: closeEye, button: eyeBtn2Image)
        }
        else
        {
            viewCheck2 = true
            self.confirmPassTextField.isSecureTextEntry = false
            setImage(image: openEye, button: eyeBtn2Image)
        }
    }
    
    func setImage(image: UIImage?, button: UIImageView) {
        UIView.transition(with: button,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { button.image = image },
                          completion: nil)
    }
    
    func hideResendBtn() {
        resendBtn.alpha = 0.5
        resendBtn.isUserInteractionEnabled = false
    }
    
    func showResendBtn() {
        resendBtn.alpha = 1
        resendBtn.isUserInteractionEnabled = true
    }
    
    private func startOtpTimer() {
        stopOTPTimer()
        self.progress.updateRatio(CGFloat(self.timerValue))
        bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.bgTask)
            })
        self.totalTime = 180
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        if timer != nil {
            RunLoop.current.add(timer!, forMode: .default)
        }
    }
    @objc func updateTimer() {
        timerValue = timerValue + 0.005555555555556
        progress.updateRatio(CGFloat(timerValue))
        print(self.totalTime)
        self.countDownLabel.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            showResendBtn()
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    private func stopOTPTimer() {
        timer?.invalidate()
    }
    
    func setUpView() {
        self.otpField.fieldsCount = 6
        self.otpField.fieldBorderWidth = 1
        self.otpField.defaultBorderColor = UIColor(red: 0.74, green: 0.75, blue: 0.76, alpha: 1.00)
        self.otpField.filledBorderColor = AppColor.appGreenColor
        self.otpField.cursorColor = AppColor.appGreenColor
        self.otpField.displayType = .roundedCorner
        self.otpField.cornerRadius = 6
        self.otpField.fieldSize = 36
        self.otpField.separatorSpace = 15
        self.otpField.fieldFont = UIFont(name: "Effra-Regular", size: 20.0)!
        self.otpField.shouldAllowIntermediateEditing = false
        self.otpField.delegate = self
        self.otpField.initializeUI()
    }

    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.passwordTextField.textAlignment = .right
            self.confirmPassTextField.textAlignment = .right
        } else {
            self.passwordTextField.textAlignment = .left
            self.confirmPassTextField.textAlignment = .left
        }
        let text = "Please enter the OTP sent you to your email".localized
        self.subHeadingLabel.text = "\(text) \(email)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    func resendOTP()  {
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        self.view.endEditing(true)
        let email = params["email"] as? String
        let token = params["token"] as? String
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.changePassword)/otp/\(lang)?email=\(email ?? "")&token=\(token ?? "")", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.hideResendBtn()
                self?.startOtpTimer()
                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
            } else {
                Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func resendTokenOTP()  {
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        self.view.endEditing(true)
        let email = params["email"] as? String
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.forgotToken)/otp/\(lang)?email=\(email ?? "")", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.hideResendBtn()
                self?.startOtpTimer()
                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
            } else {
                Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func callChangeAPI()  {
        self.view.endEditing(true)
        
        params["otp"]  = self.otpText as AnyObject
        params["password"] = passwordTextField.text as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.changePassword, params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.timer?.invalidate()
                Utilities.showAlert(title: "", message: "Password successfully changed.".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self?.stopOTPTimer()
                if self?.email == self?.keyChain.get(kUserEmail) {
                    self?.keyChain.set((self?.passwordTextField.text!)!, forKey: kUserPassword)
                }
                Utilities.setIsTouchIdEnabled(isTouchIdEnabled: false)
                FirebaseUtility.screenEvent(.ForgotPasswordViewController)
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.goToLogin()
                })
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func callChangeTokenAPI()  {
        self.view.endEditing(true)
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        params["otp"]  = self.otpText as AnyObject
        params["token"] = nil
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.forgotToken)/\(lang)", params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                self?.timer?.invalidate()
                Utilities.showAlert(title: "", message: json[keyMessage] as? String ?? "Token sent in email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self?.stopOTPTimer()
                FirebaseUtility.screenEvent(.ForgotTokenViewController)
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.goToLogin()
                })
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    @IBAction func resetPasswordButtonTapped(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        if tokenCheck {
            callChangeTokenAPI()
        } else {
            callChangeAPI()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if tokenCheck {
            FirebaseUtility.screenEvent(.ForgotTokenOTPViewController)
        } else {
            FirebaseUtility.screenEvent(.ForgotPasswordOTPViewController)
        }
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showAlert(title: "", message: Key.Messages.NoInternet.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            return false
        } else if self.otpText == "" || self.otpText.count < 5 {
            Utilities.showAlert(title: "", message: Key.Messages.WrongOTP.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            return false
        } else if !tokenCheck {
            if(self.passwordTextField.text?.isEmpty == true){
                Utilities.showAlert(title: "", message: Key.Messages.PassError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                return false
            } else if !(passwordTextField.text?.isValidPassword ?? true)  {
                Utilities.showAlert(title: "", message: Key.Messages.RegexFailedPassError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                return false
            } else if(self.confirmPassTextField.text?.isEmpty == true){
                Utilities.showAlert(title: "", message: Key.Messages.confirmPassError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                return false
            } else if(self.passwordTextField.text! != self.confirmPassTextField.text!){
                Utilities.showAlert(title: "", message: Key.Messages.PassMismatchError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                return false
            }
        }
        return true
    }
    
    @IBAction func resendButtonTapped(_ sender: Any) {
        if !(Utilities.isConnected())
        {
            Utilities.showAlert(title: "", message: Key.Messages.NoInternet.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        } else {
            if tokenCheck {
                resendTokenOTP()
            } else {
                resendOTP()
            }
        }
    }
}
extension OTPVerificationVC: OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String)
    {
        otpText = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return false
    }
}
