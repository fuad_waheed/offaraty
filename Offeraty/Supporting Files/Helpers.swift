//
//  Constants.swift
//  GameVU
//
//  Created by Jawad Waheed on 13/09/2017.
//  Copyright © 2017 esource. All rights reserved.
//

import UIKit
import CDAlertView
import Firebase
import FirebaseAnalytics
import BRYXBanner

let appDelegate = UIApplication.shared.delegate as! AppDelegate

enum ControllerNames : String {
    
    typealias RawValue = String
    
    case MyVouchersViewController = "MyVouchers"
    case Redeem = "Redeem"
    case LocationViewController = "LocationDirections"
    case Logout = "Logout"
    case SupportViewController = "Support"
    case AboutAppViewController = "AboutApp"
    case PrivacyPolicy = "PrivacyPolicy"
    case TermsConditions = "TermsConditions"
    case AboutOffaraty = "aboutOffaraty"
    case ChangePasswordViewController = "ChangePassword"
    case BaseWebViewController = "BaseWebViewController"
    case TravelShopViewController = "TravelShop"
    case SettingsViewController = "Settings"
    case ForgotPasswordOTPViewController = "forgotPasswordOTP"
    case ForgotTokenOTPViewController = "forgotTokenOTP"
    case SignupOTPController = "registrationOTP"
    case ForgotPasswordViewController = "forgotPassword"
    case ForgotTokenViewController = "forgotToken"
    case UpdateProfileViewController = "UpdateProfile"
    case FavoritesViewController = "favourites"
    case FilterViewController = "filter"
    case LoginViewController = "login"
    case GuestLoginViewController = "guestLogin"
    case SignupViewController = "sign_up"
    case HomeViewController = "Home"
    case CategoryDetailViewController = "categoryDetail"
    case OfferDetailViewController = "offerDetail"
    case AllOffersViewController = "allOffers"
    case SearchViewController = "search"
    case CategoriesViewController = "categories"
    case ScanViewController = "scanQR"
    case ReviewViewController = "review"
    case ProfileViewController = "profile"
}


struct AppColor {
    
    static let appPrimaryColor =  UIColor(red: 0.30, green: 0.36, blue: 0.42, alpha: 1.00)
    static let appPrimaryPurpleColor = UIColor(red: 0.14, green: 0.03, blue: 0.44, alpha: 1.00)
    static let appPrimaryGreenColor = UIColor(red: 0.00, green: 0.69, blue: 0.60, alpha: 1.00)
    static let appTextFieldBorderGrayColor = UIColor(red: 0.84, green: 0.84, blue: 0.84, alpha: 1.00)
    static let appGreenColor = UIColor(red: 0.16, green: 0.60, blue: 0.54, alpha: 1.00)
    static let appTextFieldBorderColor = UIColor(red: 0.18, green: 0.60, blue: 0.54, alpha: 1.00)
    static let appWhiteColor =  UIColor.white
    static let appBlueColor =  UIColor(red: 0.14, green: 0.03, blue: 0.44, alpha: 1.00)
    static let appOrangeColor =  UIColor(red: 0.77, green: 0.66, blue: 0.60, alpha: 1.00)
    static let appGrayColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.00)
    static let appDarkGrayColor = UIColor(red: 0.44, green: 0.44, blue: 0.44, alpha: 1.00)
    static let appLoginSelected = UIColor(red: 0.26, green: 0.26, blue: 0.26, alpha: 1.00)
    static let appLoginUnSelected = UIColor(red: 0.45, green: 0.49, blue: 0.57, alpha: 1.00)
    static let appBorderGray = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00)
    static let appProfileBorderGray = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00)
    static let appNewGreen = UIColor(red: 0.00, green: 0.69, blue: 0.60, alpha: 1.00)
    static let unSelectedReviewColor = UIColor(red: 0.92, green: 0.92, blue: 0.93, alpha: 1.00)
    
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
}
enum AlertType {
    case Success,
         Failure,
         Caution,
         Info,
         Congrats
}

struct APPURL {
    
    private struct Domains {
        static let Dev = "http://65.0.143.67:8015/"
        static let Live = "http://65.0.143.67:8015/"
    }
    
    private  struct Routes {
        static let Api = "/user_management/apis/"
    }
    
    private  static let Domain = Domains.Dev
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route
    
    static var Login: String {
        return BaseURL + "login_for_access_token"
    }
    
}

struct Screen {
    static func width() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static func height() -> CGFloat {
        return UIScreen.main.bounds.height
    }
}

struct Key {
    
    struct Messages{
        static let ErrorTitle = "Error"
        static let SuccessTitle = "Success"
        static let FavouriteTitle = "Favourite"
        static let AlertTitle = "Alert"
        static let OKTitle = "OK"
        static let DoneTitle = "Done"
        static let CloseTitle = "Close"
        static let WarningTitle = "Warning"
        static let CancelTitle = "Cancel"
        static let CameraNotAvailable = "Camera Not Available"
        static let TokenExpiredLoginAgain = "Session expired. Login again!"
        static let EmployerTokenDescription = "Enter the token provided by your employer"
        static let SomethingWentWrongError = "Something went wrong.Try again!"
        static let NameError = "Please enter your name."
        static let EmailError = "Please enter your email."
        static let MobileError = "Please enter your mobile number."
        static let InvalidEmailError = "Please enter a valid email."
        static let UsernameError = "Username is required"
        static let PassError = "Please enter your password."
        static let RegexFailedPassError = "Password must be 8-20 characters in length and have at least 1 uppercase alphabet, 1 lower case alphabet, 1 number and 1 special character."
        static let confirmPassError = "Please enter confirm password."
        static let PassMismatchError = "Password didn't match."
        static let TokenError = "Token is required."
        static let NoInternet = "Check your connection and Try Again."
        static let WrongOTP = "Enter 6 digit OTP"
        static let RatingError = "Kindly fill all ratings"
        static let OfferRemovedFavourites = "Item removed from Favourites successfully!"
        static let OfferAddedFavourites = "Item added to Favourites successfully!"
        static let GoToFav = "GO TO FAVOURITES"
        static let AddedToFav = "Added to favourite"
        static let AboutTextEnglish = "Riyad Bank is one of the largest financial institutions in the Kingdom of Saudi Arabia and the Middle East. Established in 1957, with a paid-up capital of SAR 30 billion. Our professional and dedicated staff base has mainly driven our success throughout the years. With more than 5,600 employees, we take pride of being among the Saudi organizations with the highest national employment rate of 93%.\nWe provide a comprehensive range of products and services fully compliant with the Islamic Sharia to meet the needs of both retail and corporate customers, including small and medium-size enterprises. We play a leading role in various areas of finance and investment around Saudi Arabia, that is why we are distinguished as a leading financier and arranger of syndicated loans in the oil, petrochemicals and most of the Kingdom's notable infrastructure projects.\nWe offer innovative and remarkable financing solutions through a network of more than 340 branches, 79 of which are ladies branches and more than 32,000 POS, in addition to more than 2,592 ATMs well distributed in strategic & carefully selected locations around the Kingdom. And since global expansion is essential for some of our customers outside the Kingdom, a branch in London and offices in Houston (USA) and Singapore help in supporting the international banking needs of such customers.\nOur electronic banking services (web-based and mobile applications) use the latest electronic technologies to address the banking needs of our customers with utmost ease, convenience and security.\nRiyad Capital is a leading player in the IPO advisory business and asset management, having won numerous investment awards in Saudi Arabia in categories ranging from \"best mutual fund performance\" to \"best fund manager\"."
        static let AboutTextArabic = "بنك الرياض هو أحد أكبر المؤسسات المالية العريقة فيبنك الرياض هو أحد أكبر المؤسسات المالية العريقة في المملكة العربية السعودية والشرق الأوسط، بدأ نشاطه في العام 1957م، ويبلغ رأس المال 30 مليار ريال سعودي.\nيعود الفضل في نجاحنا وتطور أعمالنا إلى المهنية العالية لموظفينا البالغ عددهم أكثر من 5,600 موظف وموظفة نفخر بهم في تحقيق نسبة مواطنة تصل الى 93% هي الأعلى ضمن المنشآت المالية العاملة في السوق السعودي.\nنقدم في بنك الرياض مجموعة متكاملة من الخدمات والمنتجات المصرفية الإسلامية والتقليدية لعملائنا من الأفراد والشركات والمؤسسات الناشئة، ونحرص في ذلك على توظيف قاعدتنا الرأسمالية القوية وخبراتنا العريقة للقيام بدور متميز في مجال التمويل، وقد برز بنك الرياض كبنك رائد نظم وشارك في العديد من عمليات التمويل المشتركة لمختلف القطاعات العاملة في صناعات النفط والغاز والبتروكيماويات وعدد من أبرز مشاريع البنية التحتية في المملكة العربية السعودية.\nيقدم بنك الرياض لعملائه منتجات وخدمات مصرفية وتمويلية متميزة ومبتكرة من خلال شبكة فروع تضم أكثر من (340) فرعاً، منها 79 فرعاً مخصص للسيدات وأكثر من 2,592 جهاز صراف آلي وأكثر من 32,000 من أجهزة نقاط البيع، بالإضافة الى قاعدة عملاء كبيرة بين المصارف السعودية.\nتواجد بنك الرياض خارج المملكة تلبية لاحتياجات عملائه من خلال فرع لندن بالمملكة المتحدة ووكالة هيوستن بالولايات المتحدة ومكتب تمثيلي في سنغافورة حيث يتم تقديم خدمات مصرفية ومالية متكاملة مصممة خصيصاً لخدمة مصالح عملائنا وتلبية احتياجاتهم في أماكن تواجدهم.\nنواكب في بنك الرياض تطلعات عملائنا بتقديم خدمات إلكترونية متقدمة (انترنت وتطبيقات للأجهزة الذكية للأفراد والشركات) ونحرص على تقديم أحدث التقنيات ليتمكن عملاءنا من تنفيذ عملياتهم المصرفية من أي مكان بسهولة ويسر وأمان.\nتقوم الرياض المالية (الذراع الاستثماري لبنك الرياض) بدورها المتميز في الخدمات الاستشارية والاستثمارية وإدارة الصناديق وتقديم المشورة وأنشطة الاكتتابات الأولية، وقد حققت العديد من جوائز الأداء الاستثماري أفضل أداء صندوق مشترك و أفضل مدير صندوق،"
    }
}
class FirebaseUtility {
    class func screenEvent(_ screenName: ControllerNames) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenName: screenName.rawValue,
                                        AnalyticsParameterScreenClass: screenName.rawValue])
    }
}

struct Utilities {
    
    static func isValidEmail(email:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:email)
    }
    
    static func getIsTouchIdEnabled() -> Bool {
        return Cache.extractBool(kIsTouchIDEnabled) ?? false
    }
    
    static func setIsTouchIdEnabled(isTouchIdEnabled:Bool) {
        Cache.save(isTouchIdEnabled, forKey: kIsTouchIDEnabled)
    }
    
    static func isConnected () -> Bool {
        // print("validate calendar: \(testStr)")
        if (ConnectionCheck.isConnectedToNetwork())
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    static func getIsArabic() -> Bool? {
        return Cache.extractBool(keyIsArabic)
    }
    
    static func setIsArabic(_ status: Bool) {
        Cache.save(status, forKey: keyIsArabic)
    }
    
    static func setIsUserLoggedIn(isUserLoggedIn:Bool) {
        defaults.set(isUserLoggedIn, forKey: kIsUserLoggedIn)
        defaults.synchronize()
    }
    static func getIsUserLoggedIn() -> Bool {
        if let value = defaults.value(forKey: kIsUserLoggedIn) as? Bool {
            return value
        }
        else{
            return false
        }
    }
    static func getGuestIsLoggedIn() -> Bool {
        if let value = defaults.value(forKey: kIsGuestLoggedIn) as? Bool {
            return value
        }
        else{
            return false
        }
    }
    
    @discardableResult static func showAlert(title:String,message:String?="",doneButtonTitle:String? = "Done".localized,alertType:AlertType) -> CDAlertView? {
        guard let rootViewController = UIApplication.shared.keyWindow!.rootViewController  else {
            return nil
        }
        DispatchQueue.main.async {
            rootViewController.view.endEditing(true)
        }
        
        var alert = CDAlertView()
        switch alertType {
        case .Info:
            alert = CDAlertView(title: title, message: message, type: .notification)
        case .Success:
            alert = CDAlertView(title: title, message: message, type: .success)
        case .Failure:
            alert = CDAlertView(title: title, message: message, type: .error)
        case .Congrats:
            alert = CDAlertView(title: title, message: message, type: .notification)
        case .Caution:
            alert = CDAlertView(title: title, message: message, type: .warning)
        }
        let action = CDAlertViewAction(title: doneButtonTitle?.localized)
        alert.isTextFieldHidden = true
        alert.add(action: action)
        alert.hideAnimations = { (center, transform, alpha) in
            transform = .identity
            alpha = 0
        }
        DispatchQueue.main.async {
            alert.show() { (alert) in
                print("completed")
            }
        }
        
        return alert
    }
    
    static func showALertWithTag(title:String, message:String?, buttonTitle: String? = Key.Messages.OKTitle.localized)
    {
        var alertController = UIAlertController()
        alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertController.Style.alert)
        
        let ok:UIAlertAction = UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default) { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        let topViewController = appDelegate.window?.rootViewController
        topViewController?.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Banner Helpers
    static func showError(withMessage:String) {
        let banner = Utilities.banner(title:Key.Messages.ErrorTitle.localized, message: withMessage, bgColor: UIColor.systemRed)
        banner.show(duration: 2.0)
    }
    
    static func showWarning(withMessage:String) {
        let banner = Utilities.banner(title:Key.Messages.WarningTitle.localized, message: withMessage, bgColor: UIColor.systemYellow)
        banner.textColor = UIColor.black
        banner.show(duration: 2.0)
    }
    
    static func showSuccess(withMessage:String) {
        let banner = Utilities.banner(title:Key.Messages.SuccessTitle.localized, message: withMessage, bgColor: UIColor.systemGreen)
        banner.show(duration: 2.0)
    }
    
    static func showAlert(withMessage:String) {
        let banner = Utilities.banner(title:Key.Messages.AlertTitle.localized, message: withMessage, bgColor: UIColor.systemBlue)
        banner.show(duration: 2.0)
    }
    
    static func showNotification(withTitle:String, withMessage:String) {
        let banner = Utilities.banner(title:withTitle, message: withMessage, bgColor: AppColor.appPrimaryPurpleColor)
        banner.show(duration: 3.0)
    }
    
    static func banner(title:String, message:String, bgColor:UIColor) -> Banner {
        let banner = Banner(title: title, subtitle: message, image: nil, backgroundColor:bgColor)
        banner.springiness = .heavy
        banner.position = .top
        banner.adjustsStatusBarStyle = true
        banner.dismissesOnSwipe = true
        banner.dismissesOnTap = true
        banner.textColor = UIColor.white
        banner.minimumHeight = 64
        
        return banner
    }
}


struct Cache {
    static func save(_ value:Any, forKey:String) {
        UserDefaults.standard.setValue(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func saveUserObject(_ value:User, forKey:String) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    static func extractUserObject(_ forKey:String) -> User?
    {
        if let data = UserDefaults.standard.value(forKey:forKey) as? Data
        {
            let savedObject = try? PropertyListDecoder().decode(User.self, from: data)
            return savedObject
            
        }
        else
        {
            return nil
        }
    }
    
    
    static func extractString(_ forKey:String) -> String? {
        let tmp = UserDefaults.standard.string(forKey: forKey)
        return tmp
    }
    
    static func extractBool(_ forKey:String) -> Bool? {
        let tmp = UserDefaults.standard.bool(forKey: forKey)
        return tmp
    }
    
    static func extractDate(_ forKey:String) -> Date? {
        let tmp = UserDefaults.standard.object(forKey: forKey) as? Date
        return tmp
    }
    
    static func extractDouble(_ forKey:String) -> Double? {
        let tmp = UserDefaults.standard.object(forKey: forKey) as? Double
        return tmp
    }
    
    static func extractInt(_ forKey:String) -> Int? {
        let tmp = UserDefaults.standard.integer(forKey: forKey)
        return tmp
    }
    
    static func extractIntArray(_ forKey:String) -> [Int]? {
        let tmp = UserDefaults.standard.array(forKey: forKey) as? [Int]
        return tmp
    }
    
    static func remove(forKey:String) {
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    static func removeAll()
    {
        let dict = UserDefaults.standard.dictionaryRepresentation() as NSDictionary
        for key in dict.allKeys {
            if ((key as? String ?? "") != keyLanguageDiection) && ((key as? String ?? "") != keyIsArabic) && ((key as? String ?? "") != kIsTouchIDEnabled) && ((key as? String ?? "") != keyRemeberCheck)  {
                UserDefaults.standard.removeObject(forKey: key as! String)
            }
        }
        UserDefaults.standard.synchronize()
    }
    
    
}
// MARK: - Extensions
//
extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            return keyWindow?.safeAreaInsets.bottom ?? 0 > 0
        }
        return false
    }

}
extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    var releaseVersionNumberPretty: String {
        return "v\(releaseVersionNumber ?? "1.0.0")"
    }
}
extension String {
    func whiteSpacesRemoved() -> String {
        return self.filter { $0 != Character(" ") }
    }
}
extension UITextView {
    
    func sizeToFitHeight() {
        let size:CGSize = self.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        var frame:CGRect = self.frame
        frame.size.height = size.height
        self.frame = frame
    }
}
extension UITableView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: reloadData)
            { _ in completion() }
    }
}
extension UICollectionView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: reloadData)
            { _ in completion() }
    }
}
extension NSString {
    var isValidPassword: Bool {
        let passwordRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,20}$"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: self)
    }
}
extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return UIFont(name: "Effra-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: 16) }
    var normalFont:UIFont { return UIFont(name: "Effra-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPhone13,1":                              return "iPhone 12 mini"
            case "iPhone13,2":                              return "iPhone 12"
            case "iPhone13,3":                              return "iPhone 12 Pro"
            case "iPhone13,4":                              return "iPhone 12 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "AudioAccessory5,1":                       return "HomePod mini"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
extension UIView {
    
    func makeRoundedView() {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
extension UIView {
    func fadeTo(_ alpha: CGFloat, duration: TimeInterval? = 0.3) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration != nil ? duration! : 0.3) {
                self.alpha = alpha
            }
        }
    }
    
    func fadeIn(_ duration: TimeInterval? = 0.3) {
        fadeTo(1.5, duration: duration)
    }
    func fadeOut(_ duration: TimeInterval? = 0.3) {
        fadeTo(0.0, duration: duration)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}

extension UIView {
    
    enum Visibility: String {
        case visible = "visible"
        case invisible = "invisible"
        case gone = "gone"
    }
    
    var visibility: Visibility {
        get {
            let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)
            if let constraint = constraint, constraint.isActive {
                return .gone
            } else {
                return self.isHidden ? .invisible : .visible
            }
        }
        set {
            if self.visibility != newValue {
                self.setVisibility(newValue)
            }
        }
    }
    
    @IBInspectable
    var visibilityState: String {
        get {
            return self.visibility.rawValue
        }
        set {
            let _visibility = Visibility(rawValue: newValue)!
            self.visibility = _visibility
        }
    }
    
    private func setVisibility(_ visibility: Visibility) {
        let constraints = self.constraints.filter({$0.firstAttribute == .height && $0.constant == 0 && $0.secondItem == nil && ($0.firstItem as? UIView) == self})
        let constraint = (constraints.first)
        
        switch visibility {
        case .visible:
            constraint?.isActive = false
            self.isHidden = false
            break
        case .invisible:
            constraint?.isActive = false
            self.isHidden = true
            break
        case .gone:
            self.isHidden = true
            if let constraint = constraint {
                constraint.isActive = true
            } else {
                let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 0)
                // constraint.priority = UILayoutPriority(rawValue: 999)
                self.addConstraint(constraint)
                constraint.isActive = true
            }
            self.setNeedsLayout()
            self.setNeedsUpdateConstraints()
        }
    }
}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    } }
extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
extension UIScrollView {
    
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }
    
    // Bonus: Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
    
    // Bonus: Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
public extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
// MARK: - IBDesignable Extensions
@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    func addShadow(shadowColor: CGColor = UIColor.gray.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
    }
    @IBInspectable var masksToBounds:Bool {
        set {
            layer.masksToBounds = newValue
        }
        get {
            return layer.masksToBounds
        }
    }
}


