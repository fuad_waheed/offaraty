//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class AllOffersViewController: BaseViewController {
    
    @IBOutlet weak var allOffersTableView: UITableView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var userProfilePicture: UIImageView!
    var allOffersArr = [Offers]()
    
    var arr = Cache.extractIntArray(keySelectedCategories)
    
    var categoryID : Int?
    var rating : Int?
    var limitedCheck : Bool?
    var dealOfTheDayCheck : Bool?
    var cityID : Int?
    var offerType : String?
    var filtersObj : Filters?
    var user = Cache.extractUserObject(keyCurrentUser)
    
    var page = 0
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    
    var allOfferPageNumber = 0
    var allOfferTotalPages = 0
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allOffersTableView.register(UINib(nibName: "FavouriteCell", bundle: nil), forCellReuseIdentifier: "FavouriteCell")
        getFilters()
        getAllOffers(showLoader: true)
        addReloadView()
        allOffersTableView.estimatedRowHeight = 1000
        // Do any additional setup after loading the view.
    }
    
    func addReloadView() {
        refreshControl.tintColor = AppColor.appPrimaryColor
        let attributes = [NSAttributedString.Key.foregroundColor: AppColor.appPrimaryColor,NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 13.0)!] as [NSAttributedString.Key : Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh",attributes: attributes)
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        allOffersTableView.refreshControl = refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        allOfferPageNumber = 0
        getAllOffers(showLoader: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        user = Cache.extractUserObject(keyCurrentUser)
        userProfilePicture.makeRoundedView()
        userProfilePicture.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userProfilePicture.sd_setImage(with: URL(string: user?.user_image ?? ""), placeholderImage: dpPlaceHolder)
        userNameLabel.text = user?.user_name
        hideNavigationBar()
        checkLangauge()
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.backButton.setImage(backRight, for: .normal)
        } else {
            self.backButton.setImage(backLeft, for: .normal)
        }
    }
    func getFilters()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.filters)/\(lang)", params: params, shouldAddLoader: false,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.filtersObj = try JSONDecoder().decode(Filters.self, from: userData)
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "filter")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getAllOffers(showLoader: Bool)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            let params = [String:AnyObject]()
            var url = "\(APIConstants.allOffers)/\(lang)?page=\(allOfferPageNumber)&size=10"
            if categoryID != nil {
                url = url + "&category_ids=\(categoryID!)"
            }
            if dealOfTheDayCheck != nil {
                url = url + "&deal_of_the_day=\(dealOfTheDayCheck!)"
            }
            if limitedCheck != nil {
                url = url + "&limited_time_offer=\(limitedCheck!)"
            }
            if cityID != nil {
                url = url + "&city_ids=\(cityID!)"
            }
            if offerType != nil {
                url = url + "&redemption_type=\(offerType!)"
            }
            if rating != nil {
                url = url + "&rating=\(rating!)"
            }
            url = url + "&user_id=\(user?.user_id ?? 0)"
            if showLoader {
                OfferatySingleton.shared.showHud()
            }
            OffaratyAPIManager.request(withAPIName: url, params: params, shouldAddLoader: false,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let arr = try JSONDecoder().decode([Offers].self, from: userData)
                        if self?.allOfferPageNumber == 0 {
                            self?.allOffersArr.removeAll()
                        }
                        for i in arr {
                            self?.allOffersArr.append(i)
                        }
                        if let totalPages = json[keyTotalPages] as? Int {
                            self?.allOfferTotalPages = totalPages
                        }
                        self?.allOffersTableView.reloadData()
                        self?.refreshControl.endRefreshing()
                        OfferatySingleton.shared.hideHud()
                        if (self?.allOffersArr.count ?? 0) == 0 {
                            self?.showNoOfferFound()
                        }
                    }
                    
                    catch {
                        self?.allOffersTableView.reloadData()
                        self?.refreshControl.endRefreshing()
                        OfferatySingleton.shared.hideHud()
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: json["message"] as? String ?? Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                self.allOffersTableView.reloadData()
                self.refreshControl.endRefreshing()
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "all")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func showNoOfferFound() {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.titleT = "No offers found".localized
            popVC.subTitleT = "Try different filters"
            popVC.popUpImage = UIImage(named: "no offer")
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    func getRefreshedToken(from: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "filter" {
                                self?.getFilters()
                            } else {
                                self?.getAllOffers(showLoader: true)
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    @IBAction func filterBtn(_ sender: Any) {
        if let popVC = storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.delegate = self
            popVC.filtersObj = self.filtersObj
            popVC.categoryID = self.categoryID
            popVC.rating = self.rating
            popVC.limitedCheck = self.limitedCheck
            popVC.dealOfTheDayCheck = self.dealOfTheDayCheck
            popVC.cityID = self.cityID
            popVC.offerType = self.offerType
            popVC.delegate = self
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    func callFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        var params = [String:AnyObject]()
        params["user_id"]  = (user?.user_id ?? 0) as AnyObject
        params["offer_id"]  = (offerID ?? 0) as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.favourite, params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = Key.Messages.AddedToFav.localized
                    popVC.subTitleT = Key.Messages.OfferAddedFavourites.localized
                    popVC.popUpType = .double
                    popVC.additionalCheck = "fav"
                    popVC.popUpImage = UIImage(named: "added to favourite")
                    popVC.btnText1 = Key.Messages.GoToFav.localized
                    popVC.btnText2 = Key.Messages.CloseTitle.localized
                    popVC.delegate = self
                    self?.tabBarController?.present(popVC, animated: true)
                }
                if let index = self?.allOffersArr.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.allOffersArr[index].favourite = true
                    UIView.performWithoutAnimation {
                        self?.allOffersTableView.reloadData()
                    }
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func callUnFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        let params = [String:AnyObject]()
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.unFavourite)?user_id=\(user?.user_id ?? 0)&offer_id=\(offerID ?? 0)", params: params,shouldAddLoader: true,isDeleteRequest: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                Utilities.showALertWithTag(title: Key.Messages.FavouriteTitle.localized, message: Key.Messages.OfferRemovedFavourites.localized.localized)
                if let index = self?.allOffersArr.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.allOffersArr[index].favourite = false
                    UIView.performWithoutAnimation {
                        self?.allOffersTableView.reloadData()
                    }
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.AllOffersViewController)
    }
    
    
}
extension AllOffersViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allOffersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.loadOffer(offer: self.allOffersArr[indexPath.row], fromFav: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Locked".localized
                popVC.subTitleT = "Login to see offer details".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                popVC.btnText1 = "Login".localized
                popVC.btnText2 = "Cancel".localized
                popVC.delegate = self
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
            {
                secondViewController.offerObject = self.allOffersArr[indexPath.row]
                secondViewController.offerID = "\(allOffersArr[indexPath.row].offer_id ?? 0)"
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (allOffersArr.count - 1) {
            self.checkPageAndCallApi()
        }
    }
}
extension AllOffersViewController: AlertPopUpVCDelegate {
    func buttonTapped(type: ButtonTappedType, additional: String) {
        if type == .ok && additional == "" {
            Cache.removeAll()
            appDelegate.initRootView()
        } else if type == .ok {
            self.tabBarController?.selectedIndex = 2
        }
    }
}
extension AllOffersViewController: DealCellDelegate {
    func favBtnTapped(offer: Offers?) {
        if offer?.favourite ?? false {
            callUnFavouriteAPI(offerID: offer?.offer_id)
        } else {
            callFavouriteAPI(offerID: offer?.offer_id)
        }
    }
}
extension AllOffersViewController: UIScrollViewDelegate {
    func checkPageAndCallApi() {
        if allOfferPageNumber < allOfferTotalPages {
            allOfferPageNumber = allOfferPageNumber + 1
            getAllOffers(showLoader: true)
        }
    }
}
extension AllOffersViewController: FilterViewControllerDelegate {
    func applyFilter(catID: Int?,rating: Int?,limited: Bool?,dealDay: Bool?,cityID:Int?,redemptionType:String?) {
        self.allOffersArr.removeAll()
        self.allOffersTableView.reloadData()
        self.categoryID = catID
        self.rating = rating
        self.limitedCheck = limited
        self.dealOfTheDayCheck = dealDay
        self.cityID = cityID
        self.offerType = redemptionType
        self.allOfferPageNumber = 0
        self.getAllOffers(showLoader: true)
    }
    
    
}
extension AllOffersViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
