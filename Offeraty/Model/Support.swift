//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Support: Codable {
    var id                : Int?
    var address           : String?
    var contact_no        : String?
    var whatsapp_no       : String?
    var email             : String?
}
