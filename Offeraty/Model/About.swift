//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class About: Codable {
    var id                  : Int?
    var content_key         : String?
    var content_value       : String?
}
