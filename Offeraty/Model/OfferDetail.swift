//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class OfferDetail: Codable {
    var transaction_id        : Int?
    var voucher_number        : String?
    var purchasedon           : String?
    var offer_pin             : String?
    var redeemedon            : String?
    var redeem_code           : String?
    var reference_no          : String?
}
