//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


struct User: Codable {
    var access_token                 : String?
    var token_type                   : String?
    var unique_id                    : String?
    var updatedon                    : String?
    var createdon                    : String?
    var tenant_id                    : Int?
    var email                        : String?
    var first_name                   : String?
    var dob                          : String?
    var gender                       : String?
    var last_name                    : String?
    var user_name                    : String?
    var mobile                       : String?
    var nationality                  : String?
    var device_name                  : String?
    var isfacebook                   : Int?
    var isapple                      : Int?
    var isgoogle                     : Int?
    var istwitter                    : Int?
    var allow_push                   : Int?
    var receive_email                : Int?
    var user_image                   : String?
    var language                     : String?
    var social_token                 : String?
    var device_guide                 : String?
    var device_info                  : String?
    var os                           : String?
    var ip_address                   : String?
    var user_id                      : Int?
    var user_interests_list          : [Interests]?
}
