//
//  ReviewOptionsCell.swift
//  Offeraty
//
//  Created by Fuad Waheed on 10/10/2021.
//

import UIKit

class ReviewOptionsCell: UICollectionViewCell {
    
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    func loadData(index: Int, option: ReviewOptions?, selectedOp: [ReviewOptions]) {
        titleTextLabel.text = option?.review_text
        if selectedOp.contains(where: {$0.id == option?.id}) {
            titleTextLabel.textColor = .white
            mainView.backgroundColor = AppColor.appNewGreen
        } else {
            titleTextLabel.textColor = AppColor.appLoginUnSelected
            mainView.backgroundColor = AppColor.unSelectedReviewColor
        }
    }
    
}
