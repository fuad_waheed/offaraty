//
//  SignUpViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 17/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit
import FirebaseMessaging
import XLPagerTabStrip

class SignUpViewController: BaseViewController {
    
    //MARK: TextFields Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var tokenTextField: UITextField!
    
    //MARK: Images Outlets
    @IBOutlet weak var tokenIconImage: UIImageView!
    
    //MARK: Labels Outlets
    @IBOutlet weak var SubHeadingLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var confimPassLabel: UILabel!
    @IBOutlet weak var tokenLabel: UILabel!
    
    //MARK: UIView Outlets
    @IBOutlet weak var userNaemBorderLine: UIView!
    @IBOutlet weak var emailBorderLine: UIView!
    @IBOutlet weak var phoneBorderLine: UIView!
    @IBOutlet weak var passBorderLine: UIView!
    @IBOutlet weak var confirmPassBorderLine: UIView!
    @IBOutlet weak var tokenBorderLine: UIView!
    
    @IBOutlet weak var eyeBtn1: UIButton!
    @IBOutlet weak var eyeBtn2: UIButton!
    @IBOutlet weak var eyeBtn1Image: UIImageView!
    @IBOutlet weak var eyeBtn2Image: UIImageView!
    
    var viewCheck = false
    var viewCheck2 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLangauge()
        viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func eyeBtn(_ sender: Any)
    {
        if(viewCheck)
        {
            viewCheck = false
            self.passwordTextField.isSecureTextEntry = true
            setImage(image: closeEye, button: eyeBtn1Image)
        }
        else
        {
            viewCheck = true
            self.passwordTextField.isSecureTextEntry = false
            setImage(image: openEye, button: eyeBtn1Image)
        }
    }
    
    @IBAction func eyeBtn2(_ sender: Any)
    {
        if(viewCheck2)
        {
            viewCheck2 = false
            self.confirmPasswordTextField.isSecureTextEntry = true
            setImage(image: closeEye, button: eyeBtn2Image)
        }
        else
        {
            viewCheck2 = true
            self.confirmPasswordTextField.isSecureTextEntry = false
            setImage(image: openEye, button: eyeBtn2Image)
        }
    }
    
    func setImage(image: UIImage?, button: UIImageView) {
        UIView.transition(with: button,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { button.image = image },
                          completion: nil)
    }
    
    @IBAction func countryCodePickerBtn(_ sender: Any) {
    }
    
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.userNameTextField.textAlignment = .right
            self.emailTextField.textAlignment = .right
            self.mobileTextField.textAlignment = .right
            self.passwordTextField.textAlignment = .right
            self.confirmPasswordTextField.textAlignment = .right
            self.tokenTextField.textAlignment = .right
        } else {
            self.userNameTextField.textAlignment = .left
            self.emailTextField.textAlignment = .left
            self.mobileTextField.textAlignment = .left
            self.passwordTextField.textAlignment = .left
            self.confirmPasswordTextField.textAlignment = .left
            self.tokenTextField.textAlignment = .left
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        callSignUpAPI()
    }
    
    func callSignUpAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        let number = "+966" + mobileTextField.text!
        var params = [String:AnyObject]()
        params["user_name"]  = userNameTextField.text! as AnyObject
        params["email"]  = emailTextField.text! as AnyObject
        params["mobile"]  = number as AnyObject
        params["password"] = passwordTextField.text! as AnyObject
        params["token"]  = tokenTextField.text! as AnyObject
        params["device_name"]  = UIDevice.modelName as AnyObject
        params["os"]  = "iOS" as AnyObject
        params["push_token"] = token as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.signup_user)/otp/\(lang)?email=\(emailTextField.text!)&token=\(tokenTextField.text ?? "")", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                if let dataString = json[keyOfData] as? String {
//                    Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                    params["encrypted_string"] = dataString as AnyObject
                    self?.verifyUser(params: params)
                }
                else {
                    Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    func verifyUser(params: [String:AnyObject]) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "OneTimeVerificationVC") as? OneTimeVerificationVC
        {
            secondViewController.email = emailTextField.text!
            secondViewController.params = params
            secondViewController.delegate = self
            secondViewController.modalPresentationStyle = .overCurrentContext
            self.present(secondViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        Utilities.showAlert(title: "", message: Key.Messages.EmployerTokenDescription.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Info)
    }
    
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            return false
        }
        else if(self.userNameTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.UsernameError.localized)
            return false
        }
        else if(self.emailTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.EmailError.localized)
            return false
        }
        else if(Utilities.isValidEmail(email: self.emailTextField.text!) == false){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.InvalidEmailError.localized)
            return false
        }
        else if(self.mobileTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.MobileError.localized)
            return false
        }
        else if(self.passwordTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.PassError.localized)
            return false
        }
        else if !(passwordTextField.text?.isValidPassword ?? true)  {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.RegexFailedPassError.localized)
            return false
        }
        else if(self.confirmPasswordTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.confirmPassError.localized)
            return false
        }
        else if(self.passwordTextField.text! != self.confirmPasswordTextField.text!){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.PassMismatchError.localized)
            return false
        }
        return true
    }
    
    
}
extension SignUpViewController: OneTimeVerificationVCDelegate {
    func goToHome() {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
}
extension SignUpViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == userNameTextField {
            userNameLabel.textColor = AppColor.appLoginSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            phoneLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            confimPassLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            phoneBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appBorderGray
            confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else if textField == emailTextField {
            userNameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginSelected
            phoneLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            confimPassLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appBorderGray
            emailBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            phoneBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appBorderGray
            confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else if textField == mobileTextField {
            userNameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            phoneLabel.textColor = AppColor.appLoginSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            confimPassLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appBorderGray
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            phoneBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            passBorderLine.backgroundColor = AppColor.appBorderGray
            confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else if textField == passwordTextField {
            userNameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            phoneLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginSelected
            confimPassLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appBorderGray
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            phoneBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        }  else if textField == confirmPasswordTextField {
            userNameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            phoneLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            confimPassLabel.textColor = AppColor.appLoginSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            userNaemBorderLine.backgroundColor = AppColor.appBorderGray
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            phoneBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appBorderGray
            confirmPassBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else {
            userNameLabel.textColor = AppColor.appLoginUnSelected
            emailLabel.textColor = AppColor.appLoginUnSelected
            phoneLabel.textColor = AppColor.appLoginUnSelected
            passLabel.textColor = AppColor.appLoginUnSelected
            confimPassLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginSelected
            userNaemBorderLine.backgroundColor = AppColor.appBorderGray
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            phoneBorderLine.backgroundColor = AppColor.appBorderGray
            passBorderLine.backgroundColor = AppColor.appBorderGray
            confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        userNameLabel.textColor = AppColor.appLoginUnSelected
        emailLabel.textColor = AppColor.appLoginUnSelected
        phoneLabel.textColor = AppColor.appLoginUnSelected
        passLabel.textColor = AppColor.appLoginUnSelected
        confimPassLabel.textColor = AppColor.appLoginUnSelected
        tokenLabel.textColor = AppColor.appLoginUnSelected
        userNaemBorderLine.backgroundColor = AppColor.appBorderGray
        emailBorderLine.backgroundColor = AppColor.appBorderGray
        phoneBorderLine.backgroundColor = AppColor.appBorderGray
        passBorderLine.backgroundColor = AppColor.appBorderGray
        confirmPassBorderLine.backgroundColor = AppColor.appBorderGray
        tokenBorderLine.backgroundColor = AppColor.appBorderGray
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext {
            IQKeyboardManager.shared.goNext()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobileTextField {
            var check1 = false
            var check2 = false
            
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            check1 = allowedCharacters.isSuperset(of: characterSet)
            
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            check2 = newString.length <= maxLength
            
            if check1 && check2
            {
                if string == "" {
                    return true
                } else if textField.text?.count == 0 && string != "5"{
                    return false
                } else {
                    return true
                }
            }
            else
            {
                return false
            }
        } else if textField == tokenTextField {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension SignUpViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "Sign Up".localized)
    }
}
