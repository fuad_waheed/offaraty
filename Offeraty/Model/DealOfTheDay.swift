//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class DealOfTheDay: Codable {
    var offer_id                : Int?
    var offer_title             : String?
    var offer_image             : String?
    var sponsor_image           : String?
    var rating                  : String?
    var review                  : String?
    var offer_detail            : String?
    var brand_description       : String?
    var terms_condition         : String?
    var category                : Categories?
    var order_detail            : OfferDetail?
    var banner_image            : String?
    var dod_image               : String?
}
