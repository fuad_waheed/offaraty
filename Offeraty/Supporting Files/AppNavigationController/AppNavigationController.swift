 
 //
//  AppNavigationController.swift
//  Keyur Italiya
//
//  
//  Copyright © 2016 Jtechappz. All rights reserved.
//

import UIKit

class AppNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    override var shouldAutorotate: Bool { return self.viewControllers.last!.shouldAutorotate }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { return self.viewControllers.last!.supportedInterfaceOrientations
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
    }

}
