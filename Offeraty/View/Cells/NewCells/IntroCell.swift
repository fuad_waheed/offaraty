//
//  OnBoardingCell.swift
//  Vaultdax
//
//  Created by Fuad Waheed on 25/06/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var introImage: UIImageView!
    @IBOutlet weak var descLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func loadData(head:String,image:String,desc:String) {
        titleTextLabel.text = head
        introImage.image = UIImage(named: image)
        descLabel.text = desc
    }

}
