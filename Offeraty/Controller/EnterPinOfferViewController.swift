//
//  EnterPinOfferViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 06/06/2021.
//

import UIKit
import OTPFieldView
import HMSegmentedControl

class EnterPinOfferViewController: BaseViewController {
    
    //MARK: OTPField Outlets
    @IBOutlet weak var otpField: OTPFieldView!
    
    @IBOutlet weak var voucherView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    var otpText = ""
    
    //MARK: UITextField Outlets
    @IBOutlet weak var voucherField: UITextField!
    
    @IBOutlet weak var boxHeight: NSLayoutConstraint!
    
    
    //MARK: UIImage Outlets
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var voucherLabel: UILabel!
    
    //MARK: UITextView Outlets
    @IBOutlet weak var termsTextView: UITextView!
    
    @IBOutlet weak var onlineStoreCodeView: UIView!
    
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scanQRButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    
    //MARK: UISegmentControl Outlets
    @IBOutlet weak var optionsSegmentView: HMSegmentedControl!
    var user = Cache.extractUserObject(keyCurrentUser)
    var offerObject : Offers?
    var dealOfTheDayObject : DealOfTheDay?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSegmentControl()
        if (offerObject?.purchase_type == "online") {
            voucherView.visibility = .gone
            subLabel.text = "This is your online store redeem code".localized
            onlineStoreCodeView.alpha = 1
            orLabel.visibility = .gone
            scanQRButton.visibility = .gone
            voucherField.text = offerObject?.order_detail?.redeem_code
            boxHeight.constant = 108
            reviewButton.alpha = 1
        } else {
            subLabel.text = "Type the store’s PIN".localized
            reviewButton.alpha = 0
            setUpView()
        }
        
        loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.Redeem)
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.backButton.setImage(backRight, for: .normal)
            titleLabel.textAlignment = .right
            descLabel.textAlignment = .right
            subLabel.textAlignment = .right
            termsTextView.textAlignment = .right
        } else {
            self.backButton.setImage(backLeft, for: .normal)
            titleLabel.textAlignment = .left
            descLabel.textAlignment = .left
            subLabel.textAlignment = .left
            termsTextView.textAlignment = .left
        }
        let attrs = [
            NSAttributedString.Key.font : UIFont(name: "Effra-Regular", size: 14.0)!,
            NSAttributedString.Key.foregroundColor : AppColor.appGreenColor,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:"Scan QR Code".localized, attributes:attrs)
        attributedString.append(buttonTitleStr)
        scanQRButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    func loadData() {
        dealImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        if offerObject?.banner_image != nil || dealOfTheDayObject?.banner_image != nil {
            dealImageView.sd_setImage(with: URL(string: offerObject?.banner_image ?? dealOfTheDayObject?.banner_image ?? ""), placeholderImage:placeIm)
        } else {
            dealImageView.sd_setImage(with: URL(string: offerObject?.offer_image ?? dealOfTheDayObject?.offer_image ?? ""), placeholderImage:placeIm)
        }
        titleLabel.text = offerObject?.merchant?.business_name
        descLabel.text = offerObject?.offer_title ?? dealOfTheDayObject?.offer_title
        termsTextView.text = offerObject?.terms_condition
        voucherLabel.text = offerObject?.order_detail?.voucher_number
        
    }
    override func viewWillAppear(_ animated: Bool) {
        checkLangauge()
        user = Cache.extractUserObject(keyCurrentUser)
        userNameLabel.text = user?.user_name ?? "Guest".localized
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
    }
    func setUpSegmentControl() {
        optionsSegmentView.sectionTitles = ["T&C".localized]
        optionsSegmentView.autoresizingMask = [.flexibleRightMargin, .flexibleWidth]
        optionsSegmentView.selectionStyle = .textWidthStripe
        optionsSegmentView.selectionIndicatorLocation = .bottom
        optionsSegmentView.selectionIndicatorColor = AppColor.appPrimaryPurpleColor
        optionsSegmentView.selectionIndicatorHeight = 2
        optionsSegmentView.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "#747C91") ?? .darkGray,
            NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 17.0)!
        ]
        optionsSegmentView.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.appNewGreen,
            NSAttributedString.Key.font: UIFont(name: "Effra-Medium", size: 17.0)!
        ]
        optionsSegmentView.backgroundColor = .clear
        optionsSegmentView.segmentWidthStyle = .dynamic
        optionsSegmentView.type = .text
    }
    
    func setUpView() {
        self.otpField.fieldsCount = 5
        self.otpField.fieldBorderWidth = 1
        self.otpField.defaultBorderColor = UIColor(red: 0.74, green: 0.75, blue: 0.76, alpha: 1.00)
        self.otpField.filledBorderColor = AppColor.appNewGreen
        self.otpField.cursorColor = AppColor.appNewGreen
        self.otpField.displayType = .roundedCorner
        self.otpField.cornerRadius = 5
        self.otpField.fieldSize = 42
        self.otpField.fieldFont = UIFont(name: "Effra-Regular", size: 20.0)!
        self.otpField.shouldAllowIntermediateEditing = false
        self.otpField.delegate = self
        self.otpField.initializeUI()
    }
    
    func callRedeemApi()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        var transactionID : Int?
        var voucherNumber : String?
        if self.dealOfTheDayObject != nil {
            transactionID = self.dealOfTheDayObject?.order_detail?.transaction_id
            voucherNumber = self.dealOfTheDayObject?.order_detail?.voucher_number
        } else {
            transactionID = self.offerObject?.order_detail?.transaction_id
            voucherNumber = self.offerObject?.order_detail?.voucher_number
        }
        
        var params = [String:AnyObject]()
        params["transaction_id"]  = transactionID as AnyObject
        params["voucher_number"]  = voucherNumber as AnyObject
        params["store_pin"]  = self.otpText as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.Spend)/\(lang)", params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.showSuccess()
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken()
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func showSuccess() {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "RedeemSuccessPopUpVC") as? RedeemSuccessPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.delegate = self
            popVC.offerObject = self.offerObject
            popVC.dealOfTheDayObject = self.dealOfTheDayObject
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    func getRefreshedToken()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.callRedeemApi()
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    @IBAction func infoButton(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.titleT = "Voucher".localized
            popVC.subTitleT = "This is your voucher code".localized
            popVC.popUpType = .single
            popVC.popUpImage = UIImage(named: "voucher-grpahic")
            if self.tabBarController != nil {
                self.tabBarController?.present(popVC, animated: true)
            } else {
                self.present(popVC, animated: true)
            }
            
        }
    }
    
    @IBAction func writeReviewBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.offerObject = self.offerObject
            popVC.dealOfTheDayObject = self.dealOfTheDayObject
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    @IBAction func copyOnlineVoucherCode(_ sender: Any) {
        if offerObject?.order_detail?.redeem_code != nil {
            UIPasteboard.general.string = offerObject?.order_detail?.redeem_code
            self.navigationController?.view.makeToast("Redeem Code copied to clipboard.", duration: 2.0, position: .bottom)
        }
    }
    
    @IBAction func scanQRButtonTapped(_ sender: Any) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScannerViewController") as? ScannerViewController {
            secondViewController.dealOfTheDayObject = self.dealOfTheDayObject
            secondViewController.offerObject = self.offerObject
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }

}
extension EnterPinOfferViewController: OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        otpText = otp
        callRedeemApi()
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return false
    }
    
    func goToCatDetail() {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let secondViewController = storyBoard.instantiateViewController(withIdentifier: "CategoryDetailViewController") as? CategoryDetailViewController
        {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
}
extension EnterPinOfferViewController: RedeemSuccessPopUpVCDelegate {
    func goTo(screen: GoToScreen) {
        if screen == .review {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.delegate = self
                popVC.offerObject = self.offerObject
                popVC.dealOfTheDayObject = self.dealOfTheDayObject
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            self.goToCatDetail()
        }
    }
}
extension EnterPinOfferViewController: ReviewViewControllerDelegate {
    func goToHome(reviewed: Bool) {
        if reviewed {
            let alert = UIAlertController(title: "Reviews".localized, message: "Thank you for your valuable review.\nCheck out more exciting offers waiting for you !".localized, preferredStyle: UIAlertController.Style.alert)
            let ok = UIAlertAction(title: Key.Messages.OKTitle.localized, style: .default) { (action:UIAlertAction) in
                self.goToCatDetail()
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.goToCatDetail()
        }
    }
}
