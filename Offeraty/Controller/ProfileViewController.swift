//
//  ProfileViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 02/06/2021.
//

import UIKit
import TagsList

class ProfileViewController: BaseViewController {
    
    var randomStrings: [String] = [
        "Donec", "consectetur", "metus", "non", "sollicitudin", "condimentum",
        "Cras sodales lobortis neque at scelerisque", "Proin tempor eleifend massa", "Curabitur",
        "semper tortor eget"
    ]
    var dataSource: DefaultTagsListDataSource!

    //MARK: UIImage Outlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userBgImageView: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var tagView: UIView!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var mobileField: UITextField!
    
    @IBOutlet weak var mainView: UIView!
    
    
    var tagsListView: TagsListProtocol = TagsList()
    var user = Cache.extractUserObject(keyCurrentUser)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        userBgImageView.contentMode = .scaleAspectFill
        tagView.addSubview(tagsListView)
        tagsListView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tagsListView.centerXAnchor.constraint(equalTo: tagView.centerXAnchor),
            tagsListView.leadingAnchor.constraint(equalTo: tagView.leadingAnchor, constant: 8),
            tagsListView.trailingAnchor.constraint(equalTo: tagView.trailingAnchor, constant: -8),
            tagsListView.topAnchor.constraint(equalTo: tagView.topAnchor, constant: 8),
            tagsListView.bottomAnchor.constraint(equalTo: tagView.bottomAnchor, constant: -8)
        ])
        tagsListView.itemsConfiguration.sideImageEverytimeDisplaying = false
        tagsListView.itemsConfiguration.xButtonEverytimeDisplaying = false
        tagsListView.minimumInteritemSpacing = 15
        tagsListView.itemsConfiguration.borderMarginHorizontal = 10
        tagsListView.contentOrientation = .verticalSizeToFit
        dataSource = DefaultTagsListDataSource(tagsListView)

        tagsListView.tagsListDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserData()
        loadUserData()
        checkLangauge()
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            nameLabel.textAlignment = .right
            emailLabel.textAlignment = .right
            mobileLabel.textAlignment = .right
            nameField.textAlignment = .right
            emailField.textAlignment = .right
            mobileField.textAlignment = .right
        } else {
            nameLabel.textAlignment = .left
            emailLabel.textAlignment = .left
            mobileLabel.textAlignment = .left
            nameField.textAlignment = .left
            emailField.textAlignment = .left
            mobileField.textAlignment = .left
        }
    }
    
    func getUserData()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.getUser)/\(lang)?user_id=\(user?.user_id ?? 0)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let obj = try JSONDecoder().decode(User.self, from: userData)
                        Cache.saveUserObject(obj, forKey: keyCurrentUser)
                        self?.loadUserData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken()
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getRefreshedToken()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.getUserData()
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func loadUserData() {
        user = Cache.extractUserObject(keyCurrentUser)
        dataSource.removeAllTags()
        for i in user?.user_interests_list ?? [] {
            let item = TagViewItem()
            item.title = i.category?.name
            item.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1.00)
            item.titleColor = .darkGray
            item.titleFont = UIFont(name: "Effra-Regular", size: 12.0)!
            item.xButtonDisplaying = false
            dataSource.appendTag(item)
        }
        self.nameField.text = user?.user_name
        self.userNameLabel.text = user?.user_name
        self.emailField.text = user?.email
        self.mobileField.text = user?.mobile
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userBgImageView.sd_setImage(with: URL(string: user?.user_image ?? ""))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.ProfileViewController)
    }
    
    @IBAction func editProfileButtonTapped(_ sender: Any) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }

}
extension ProfileViewController: TagsListDelegate {
    func tagsListCellTouched(_ TagsList: TagsListProtocol, index: Int) {
        print("\nCell with index: \(index) was touched")
    }

    func tagsListCellXButtonTouched(_ TagsList: TagsListProtocol, index: Int) {
        print("\nxButton from cell with tag index: \(index) was touched")
    }
}
