//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit
protocol LimitedTimeCellDelegate: AnyObject {
    func offerButtonTapped(offer: DealOfTheDay?)
}

class LimitedTimeCell: UICollectionViewCell {

    //MARK: UILabel Outlets
    @IBOutlet weak var interestTitle: UILabel!
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var bannerImage: UIImageView!
    
    var offerObject: DealOfTheDay?
    
    weak var delegate: LimitedTimeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadDeal(dealObject:DealOfTheDay?) {
        self.offerObject = dealObject
        self.interestTitle.text = dealObject?.offer_title
        if dealObject?.banner_image != nil {
            bannerImage.alpha = 1
            interestTitle.alpha = 0
            bookBtn.alpha = 0
            bannerImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            bannerImage.contentMode = .scaleAspectFill
            var placeIm = imagePlaceHolder
            if (Utilities.getIsArabic() ?? false) {
                placeIm = imagePlaceHolderArabic
            }
            bannerImage.sd_setImage(with: URL(string: dealObject?.banner_image ?? ""), placeholderImage:placeIm)
        } else {
            bannerImage.alpha = 0
            interestTitle.alpha = 1
            bookBtn.alpha = 1
        }
    }

    @IBAction func offerTapped(_ sender: Any) {
        if delegate != nil {
            delegate?.offerButtonTapped(offer: self.offerObject)
        }
    }
}
