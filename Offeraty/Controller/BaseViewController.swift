//
//  BaseVC.swift
//  Stokke
//
//  Created by Jawad Waheed on 15/11/2018.
//
import KeychainSwift

class BaseViewController: UIViewController {
    
    let sharedDelegate = UIApplication.shared.delegate as! AppDelegate
    let size = CGSize(width: 40, height: 40)
    let keyChained = KeychainSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //Hide keyBoard by touch outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func transparentNavBar()
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func showNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func hideNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func hideTabBar() {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showTabBar() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func dismissPresentedController() {
        dismiss(animated: true, completion: nil)
    }
    
    func popBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addLeftNavigationButtonWithTitle(_ title : String) {
        navigationItem.leftBarButtonItem = nil
        let leftbarButton = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(leftNavigationButtonPressed))
        navigationItem.leftBarButtonItem = leftbarButton;
    }
    
    func addLeftNavigationButtonWithImage(_ image : String) {
        navigationItem.leftBarButtonItem = nil
        let rightbarButton = UIBarButtonItem(image: UIImage.init(named: image), style: .plain, target: self, action: #selector(leftNavigationButtonPressed))
        navigationItem.leftBarButtonItem = rightbarButton;
    }
    
    func addRightNavigationButtonWithTitle(_ title : String) {
        navigationItem.rightBarButtonItem = nil
        let rightbarButton = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(rightNavigationButtonPressed))
        navigationItem.rightBarButtonItem = rightbarButton;
    }
    
    func addRightNavigationButtonWithImage(_ image : String) {
        navigationItem.rightBarButtonItem = nil
        let rightbarButton = UIBarButtonItem(image: UIImage.init(named: image), style: .plain, target: self, action: #selector(rightNavigationButtonPressed))
        navigationItem.rightBarButtonItem = rightbarButton;
    }

    func removeRightBarButton() {
        navigationItem.rightBarButtonItem = nil
    }

    func removeLeftBarButton() {
        navigationItem.leftBarButtonItem = nil
    }
    
    func hideBackButton() {
        navigationItem.hidesBackButton = true
    }
    
    func showBackButton() {
        navigationItem.hidesBackButton = false
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToSearch(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "App", bundle: Bundle.main)
        if let secondViewController = mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    func logOut() {
        Utilities.setIsTouchIdEnabled(isTouchIdEnabled: false)
        self.keyChained.set("", forKey: kUserEmail)
        self.keyChained.set("", forKey: kUserPassword)
        self.keyChained.set("", forKey: kClientToken)
        Cache.removeAll()
        
        let alert = UIAlertController(title:Key.Messages.AlertTitle.localized, message: "Your token has expired/changed please login manually once with new token to proceed.".localized, preferredStyle:UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title:Key.Messages.OKTitle.localized,style: UIAlertAction.Style.default, handler:{_ in 
            
            appDelegate.initRootView()
            
        }));
        self.present(alert, animated:true, completion:nil)
    }
    
    @objc func leftNavigationButtonPressed() {
        
    }
    
    @objc func rightNavigationButtonPressed() {
        
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
