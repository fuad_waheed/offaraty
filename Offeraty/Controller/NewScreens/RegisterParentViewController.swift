//
//  ViewController.swift
//  DryCleanerDriver
//
//  Created by Fuad Waheed on 30/05/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RegisterParentViewController: ButtonBarPagerTabStripViewController {
    
    //MARK: NSLayoutConstraints Outlets
    @IBOutlet weak var languageBtnWidth: NSLayoutConstraint!
    
    //MARK: Labels Outlets
    @IBOutlet weak var languageLabel: UILabel!
    
    override func viewDidLoad() {
        settingtabbar()
        super.viewDidLoad()
        checkLangauge()
        viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.languageLabel.text = "عربى"
            self.languageBtnWidth.constant = 64
        } else {
            self.languageLabel.text = "English"
            self.languageBtnWidth.constant = 82
        }
    }
    func settingtabbar() {
        settings.style.buttonBarHeight = 1
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = AppColor.appPrimaryPurpleColor
        settings.style.buttonBarItemFont = UIFont(name: "Effra-Medium", size: 17.0) ?? .systemFont(ofSize: 17.0)
        settings.style.selectedBarHeight = 1.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = AppColor.appGrayColor
            newCell?.label.textColor = AppColor.appPrimaryGreenColor
        }
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "signInChild")
        let child2 = UIStoryboard(name: "OnBoarding", bundle: nil).instantiateViewController(withIdentifier: "signUpChild")
        return [child1 ,child2]
    }
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged && toIndex > -1 && toIndex < viewControllers.count {
            _ = viewControllers[toIndex] as! IndicatorInfoProvider // swiftlint:disable:this force_cast
            UIView.performWithoutAnimation({ [weak self] () -> Void in
                guard let me = self else { return }
                me.navigationItem.title =  ""
            })
        }
    }
    @IBAction func languageBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        let languageArr = ["English","عربى"]
        var currentIndex = 0
        if let index = languageArr.firstIndex(where: {$0 == (self.languageLabel.text ?? "")}) {
            currentIndex = index
        }
        let picker = ActionSheetStringPicker(title: "Select Language".localized,
                                     rows: languageArr,
                                     initialSelection: currentIndex,
                                     doneBlock: { picker, value, index in
                                        self.languageLabel.text = languageArr[value]
                                        if value == 0 {
                                            PPLocalization.sharedInstance.setLanguage(language: "en")
                                            appDelegate.getlanguageDirection()
                                            Utilities.setIsArabic(false)
                                        } else {
                                            PPLocalization.sharedInstance.setLanguage(language: "ar")
                                            appDelegate.getlanguageDirection()
                                            Utilities.setIsArabic(true)
                                        }
                                        appDelegate.initRootView()
                                        return
                                     },
                                     cancel: { picker in
                                        return
                                     },
                                     origin: sender)
        let okButton = UIButton()
        let cancelButton = UIButton()
        okButton.setTitleColor(UIColor.systemBlue, for: .normal)
        cancelButton.setTitleColor(UIColor.systemBlue, for: .normal)
        okButton.setTitle(Key.Messages.DoneTitle.localized, for: .normal)
        cancelButton.setTitle(Key.Messages.CancelTitle.localized, for: .normal)
        let customDoneButton = UIBarButtonItem.init(customView: okButton)
        let customCancelButton = UIBarButtonItem.init(customView: cancelButton)
        picker?.setDoneButton(customDoneButton)
        picker?.setCancelButton(customCancelButton)
        picker?.show()
    }
}

