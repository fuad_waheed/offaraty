//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class SubCategory: Codable {
    var subcategory_id   : Int?
    var name          : String?
    var category_id   : Int?
    var orderby         : String?
    var createdon          : String?
    var image         : String?
}
