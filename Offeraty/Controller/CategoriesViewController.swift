//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class CategoriesViewController: BaseViewController {
    
    //MARK: CollectionView Outlet
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    //MARK: Button Outlet
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    var offerID = ""
    
    var selectedCategoriesIndex = [Int]()
    var categoriesArray : [Categories]?
    var editCheck = false
    let user = Cache.extractUserObject(keyCurrentUser)
    
    //MARK: Labels Outlets
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryCollectionView.register(UINib(nibName: "InterestCell", bundle: nil), forCellWithReuseIdentifier: "InterestCell")
        subHeadingLabel.text = "\(user?.user_name ?? "")"
        checkLangauge()
        getCategories()
        // Do any additional setup after loading the view.
    }
    
    func getCategories()  {
        
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.categoryList)/\(lang)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.categoriesArray = try JSONDecoder().decode([Categories].self, from: userData)
                        if self?.editCheck ?? false {
                            self?.preSelectIDs()
                        }
                        self?.categoryCollectionView.reloadData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "gets")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getRefreshedToken(from: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "get" {
                                self?.getCategories()
                            } else {
                                self?.confirm()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func confirm() {
        var catStr = ""
        for i in selectedCategoriesIndex {
            if self.categoriesArray?.indices.contains(i) ?? false {
                catStr = catStr + "\(self.categoriesArray?[i].category_id ?? 0),"
            }
        }
        if catStr != "" {
            catStr = String(catStr.dropLast())
        }
        if editCheck {
            callUpdateUserInterestsAPI(ids: catStr, url: APIConstants.updaetUserInterests)
        } else {
            callSaveUserInterestsAPI(ids: catStr, url: APIConstants.userInterests)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.CategoriesViewController)
    }
    
    func callSaveUserInterestsAPI(ids: String, url:String)  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        
        var params = [String:AnyObject]()
        params["category_ids"]  = "{\"list_int\":[\(ids)]}" as AnyObject
        params["user_id"]  = user?.user_id as AnyObject
        params["language"] = lang as AnyObject
        OffaratyAPIManager.request(withAPIName: url, params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                if self?.editCheck ?? false {
                    self?.navigationController?.popViewController(animated: true)
                } else {
                    self?.goToHome()
                }
                
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "save")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func callUpdateUserInterestsAPI(ids: String, url:String)  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        
        var params = [String:AnyObject]()
        params["category_ids"]  = "{\"list_int\":[\(ids)]}" as AnyObject
        params["user_id"]  = user?.user_id as AnyObject
        params["language"] = lang as AnyObject
        OffaratyAPIManager.request(withAPIName: url, params: params,shouldAddLoader: true,isPutRequest: true, urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                if self?.editCheck ?? false {
                    self?.navigationController?.popViewController(animated: true)
                } else {
                    self?.goToHome()
                }
                
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "update")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func goToHome() {
        let mainStoryboard = UIStoryboard(name: "App", bundle: Bundle.main)
        if let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
        {
            vc.selectedIndex = 0
            if let navController = vc.viewControllers?[0] as? UINavigationController {
                if let controller = navController.topViewController as? HomeViewController {
                    controller.offerID = offerID
                }
            }
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.window?.rootViewController = vc
            appDelegate.window?.makeKeyAndVisible()
            
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.nextButton.setImage(goNext, for: .normal)
            self.previousButton.setImage(backRight, for: .normal)
        } else {
            self.nextButton.setImage(goPrevious, for: .normal)
            self.previousButton.setImage(backLeft, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        if editCheck {
            previousButton.alpha = 1
            hideTabBar()
            headerLabel.visibility = .gone
        } else {
            previousButton.alpha = 0
        }
    }
    
    func preSelectIDs() {
        for i in 0 ..< (self.categoriesArray?.count ?? 0) {
            if self.user?.user_interests_list?.contains(where: {$0.category?.category_id == self.categoriesArray?[i].category_id}) ?? false {
                self.selectedCategoriesIndex.append(i)
            }
        }
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        
        if selectedCategoriesIndex.isEmpty {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: "Please select at least one\ncategory of interest".localized)
        } else {
            confirm()
            
        }
    }
    
    
    @IBAction func catInfoBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.titleT = "Choose interest".localized
            popVC.subTitleT = "Sharing your interests helps serve you better".localized
            popVC.popUpType = .single
            popVC.popUpImage = UIImage(named: "Favourite-graphic")
            if self.tabBarController != nil {
                self.tabBarController?.present(popVC, animated: true)
            } else {
                self.present(popVC, animated: true)
            }
            
        }
    }
    
    
}
extension CategoriesViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.categoriesArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! InterestCell
        
        cell.loadData(categoryObject: self.categoriesArray?[indexPath.row], categoriesIndexArr: self.selectedCategoriesIndex,index:indexPath.row)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let index = selectedCategoriesIndex.firstIndex(where: {$0 == indexPath.row}) {
            self.selectedCategoriesIndex.remove(at: index)
        } else {
            self.selectedCategoriesIndex.append(indexPath.row)
        }
        self.categoryCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = (collectionView.bounds.width/4.0)
        let yourHeight = CGFloat(120)
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

