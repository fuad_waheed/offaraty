//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Reviews: Codable {
    var rating                : Int?
    var review                : String?
    var review_options        : [String]?
    var user_name             : String?
}
