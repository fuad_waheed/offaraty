//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Categories: Codable {
    var category_id   : Int?
    var name          : String?
    var image         : String?
    var createdon          : String?
    var sub_category         : [SubCategory]?
}
