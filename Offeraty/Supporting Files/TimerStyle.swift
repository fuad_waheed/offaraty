//
//  MyStyle.swift
//  GradientCircularProgress
//
//  Created by keygx on 2015/11/25.
//  Copyright (c) 2015年 keygx. All rights reserved.
//

import GradientCircularProgress

public struct TimerStyle: StyleProperty {
    /*** style properties **********************************************************************************/
    
    // Progress Size
    public var progressSize: CGFloat = 63
    
    // Gradient Circular
    public var arcLineWidth: CGFloat = 1.5
    public var startArcColor: UIColor = AppColor.appPrimaryPurpleColor
    public var endArcColor: UIColor = AppColor.appPrimaryPurpleColor
    
    // Base Circular
    public var baseLineWidth: CGFloat? = 1.5
    public var baseArcColor: UIColor? = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.00)
    
    // Ratio
    public var ratioLabelFont: UIFont? = UIFont(name: "Verdana-Bold", size: 1.0)
    public var ratioLabelFontColor: UIColor? = AppColor.appPrimaryColor
    
    // Message
    public var messageLabelFont: UIFont? = UIFont.systemFont(ofSize: 1.0)
    public var messageLabelFontColor: UIColor? = AppColor.appPrimaryColor
    
    // Background
    public var backgroundStyle: BackgroundStyles = .none
    
    // Dismiss
    public var dismissTimeInterval: Double? = 0.0 // 'nil' for default setting.
    
    /*** style properties **********************************************************************************/
    
    public init() {}
}
