//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit
import iOSDropDown
protocol FilterViewControllerDelegate: AnyObject {
    func applyFilter(catID: Int?,rating: Int?,limited: Bool?,dealDay: Bool?,cityID:Int?,redemptionType:String?)
}

class FilterViewController: BaseViewController {
    
    @IBOutlet weak var categoryField: DropDown!
    //    @IBOutlet weak var ratingField: UITextField!
    //    @IBOutlet weak var limitedTimeField: UITextField!
    //    @IBOutlet weak var dealOfTheDayField: UITextField!
    @IBOutlet weak var cityField: DropDown!
    @IBOutlet weak var redemptionTypeField: DropDown!
    
    //MARK: UIView Outlets
    @IBOutlet weak var categoryView: UIView!
    //    @IBOutlet weak var ratingView: UIView!
    //    @IBOutlet weak var limitedTimeView: UIView!
    //    @IBOutlet weak var dealOfTheDayView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var redemptionTypeView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    var filtersObj : Filters?
    
    var categoryID : Int?
    var rating : Int?
    var limitedCheck : Bool?
    var dealOfTheDayCheck : Bool?
    var cityID : Int?
    var offerType : String?
    
    weak var delegate: FilterViewControllerDelegate?
    
    var user = Cache.extractUserObject(keyCurrentUser)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if filtersObj == nil {
            getFilters()
        } else {
            setFilterPreValue()
            makeFiltersVisible()
        }
        categoryField.checkMarkEnabled = false
        categoryField.arrowSize = 0
        categoryField.selectedRowColor = .clear
        
        cityField.checkMarkEnabled = false
        cityField.arrowSize = 0
        cityField.selectedRowColor = .clear
        
        redemptionTypeField.checkMarkEnabled = false
        redemptionTypeField.arrowSize = 0
        redemptionTypeField.selectedRowColor = .clear
        
        categoryField.didSelect{(selectedText , index ,id) in
            self.categoryField.text = selectedText
            self.categoryID = id
        }
        cityField.didSelect{(selectedText , index ,id) in
            self.cityField.text = selectedText
            self.cityID = id
        }
        redemptionTypeField.didSelect{(selectedText , index ,id) in
            self.redemptionTypeField.text = selectedText
            self.offerType = selectedText
        }
        mainView.tag = 900
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.FilterViewController)
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch = touches.first!
//        if touch.view?.tag != 900 {
//            self.dismissPresentedController()
//        }
//
//    }
    
    func setFilterPreValue() {
        if categoryID != nil {
            if let index = filtersObj?.nav_categories?.firstIndex(where: {$0.category_id == categoryID}) {
                self.categoryField.text = self.filtersObj?.nav_categories?[index].name ?? ""
            }
        }
        //        if rating != nil {
        //            if let index = filtersObj?.rating?.firstIndex(where: {$0 == rating}) {
        //                self.ratingField.text = "\(self.filtersObj?.rating?[index] ?? 0)"
        //            }
        //        }
        //        if limitedCheck != nil {
        //            self.limitedTimeField.text = "\(limitedCheck!)"
        //        }
        //        if dealOfTheDayCheck != nil {
        //            self.dealOfTheDayField.text = "\(dealOfTheDayCheck!)"
        //        }
        if cityID != nil {
            if let index = filtersObj?.city?.firstIndex(where: {$0.city_id == cityID}) {
                self.cityField.text = self.filtersObj?.city?[index].city_name ?? ""
            }
        }
        if offerType != nil {
            self.redemptionTypeField.text = "\(offerType!)"
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.categoryField.textAlignment = .right
            //            self.ratingField.textAlignment = .right
            //            self.limitedTimeField.textAlignment = .right
            //            self.dealOfTheDayField.textAlignment = .right
            self.cityField.textAlignment = .right
            self.redemptionTypeField.textAlignment = .right
        } else {
            self.categoryField.textAlignment = .left
            //            self.ratingField.textAlignment = .left
            //            self.limitedTimeField.textAlignment = .left
            //            self.dealOfTheDayField.textAlignment = .left
            self.cityField.textAlignment = .left
            self.redemptionTypeField.textAlignment = .left
        }
    }
    
    func getFilters()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.filters)/\(lang)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.filtersObj = try JSONDecoder().decode(Filters.self, from: userData)
                        self?.setFilterPreValue()
                        self?.makeFiltersVisible()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken()
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    func getRefreshedToken()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.getFilters()
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    func makeFiltersVisible() {
        self.categoryView.visibility = .visible
        //self.ratingView.visibility = .visible
        //self.limitedTimeView.visibility = .visible
        //self.dealOfTheDayView.visibility = .visible
        self.cityView.visibility = .visible
        self.redemptionTypeView.visibility = .visible
        self.animateChanges()
        var cityArr = [String]()
        var cityIDArr = [Int]()
        var categoryArr = [String]()
        var categoryIDArr = [Int]()
        var redemptionArr = [String]()
        for i in filtersObj?.city ?? [] {
            cityArr.append(i.city_name ?? "")
            cityIDArr.append(i.city_id ?? 0)
        }
        for i in filtersObj?.nav_categories ?? [] {
            categoryArr.append(i.name ?? "")
            categoryIDArr.append(i.category_id ?? 0)
        }
        for i in 0 ..< (filtersObj?.redemption_type?.count ?? 0) {
            redemptionArr.append(filtersObj?.redemption_type?[i] ?? "")
        }
        cityField.optionArray = cityArr
        cityField.optionIds = cityIDArr
        categoryField.optionArray = categoryArr
        categoryField.optionIds = categoryIDArr
        redemptionTypeField.optionArray = redemptionArr
    }
    func animateChanges() {
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func resetBtn(_ sender: Any) {
        self.categoryField.text = ""
        self.categoryID = nil
        
        //        self.ratingField.text = ""
        self.rating = nil
        
        //        self.limitedTimeField.text = ""
        self.limitedCheck = nil
        
        //        self.dealOfTheDayField.text = ""
        self.dealOfTheDayCheck = nil
        
        self.cityField.text = ""
        self.cityID = nil
        
        self.redemptionTypeField.text = ""
        self.offerType = nil
        
        self.callDelegate()
    }

    @IBAction func applyFilterButton(_ sender: Any) {
        if self.categoryID == nil && self.rating == nil && self.limitedCheck == nil && self.dealOfTheDayCheck == nil && self.cityID == nil && self.offerType == nil {
            self.navigationController?.view.makeToast("No Filter selected!".localized, duration: 3.0, position: .bottom)
        } else {
            self.callDelegate()
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismissPresentedController()
    }
    
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    
    func callDelegate() {
        if delegate != nil {
            delegate?.applyFilter(catID: self.categoryID, rating: self.rating, limited: self.limitedCheck, dealDay: self.dealOfTheDayCheck,cityID:self.cityID,redemptionType:self.offerType)
            dismissPresentedController()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        user = Cache.extractUserObject(keyCurrentUser)
        checkLangauge()
    }
}
