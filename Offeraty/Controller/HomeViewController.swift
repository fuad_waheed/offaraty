//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    //MARK: CollectionView Outlet
    @IBOutlet weak var todayDealCollectionView: UICollectionView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var dealOftheDayView: UIView!
    @IBOutlet weak var languageLabel: UILabel!
    
    //MARK: UIImageView Outlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: NSLayoutConstraints Outlets
    @IBOutlet weak var languageBtnWidth: NSLayoutConstraint!
    
    var user = Cache.extractUserObject(keyCurrentUser)
    var offerID = ""
    
    var offersObject : Offers?
    var categoryList = ["Offers","Discount","Experiences","Gift Cards","Gifts","Magazines","Exchange","Play & Win","Health"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        checkLangauge()
        // Do any additional setup after loading the view.
    }
    
    func getOffers()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            var params = [String:AnyObject]()
            params["user_id"]  = (user?.user_id ?? 0) as AnyObject
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.homeItems)/\(lang)?size=10&recomended_offers=false&deal_of_the_day=true&limited_time_offer=false", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.offersObject = try JSONDecoder().decode(Offers.self, from: userData)
                        self?.todayDealCollectionView.reloadData()
                        if (self?.offersObject?.deal_of_the_day?.count ?? 0) > 0 {
                            self?.dealOftheDayView.visibility = .visible
                            self?.animateChanges()
                        }
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        //                    Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: json["message"] as? String ?? "Something went wrong".localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                        self?.dealOftheDayView.visibility = .gone
                        self?.animateChanges()
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken()
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getRefreshedToken()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.getOffers()
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        FirebaseUtility.screenEvent(.HomeViewController)
        if offerID != "" {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller {
                secondViewController.offerID = offerID
                offerID = ""
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.languageLabel.text = "عربى"
        } else {
            self.languageLabel.text = "English"
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            if let arrayOfTabBarItems = self.tabBarController?.tabBar.items as AnyObject as? NSArray {
                if let tabBarItem = arrayOfTabBarItems[1] as? UITabBarItem {
                    tabBarItem.isEnabled = false
                }
                if let
                    tabBarItem2 = arrayOfTabBarItems[1] as? UITabBarItem {
                    tabBarItem2.isEnabled = false
                }
                if let
                    tabBarItem3 = arrayOfTabBarItems[2] as? UITabBarItem {
                    tabBarItem3.isEnabled = false
                }
                if let
                    tabBarItem4 = arrayOfTabBarItems[3] as? UITabBarItem {
                    tabBarItem4.isEnabled = false
                }
            }
        }
        getOffers()
        user = Cache.extractUserObject(keyCurrentUser)
        userNameLabel.text = user?.user_name ?? "Guest".localized
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
    }
    
    func animateChanges()
    {
        let indexPath = IndexPath(row: 0, section: 0)
        if self.offersObject?.deal_of_the_day?.indices.contains(0) ?? false {
            self.todayDealCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func languageBtnTapped(_ sender: Any) {
        let languageArr = ["English","عربى"]
        var currentIndex = 0
        if let index = languageArr.firstIndex(where: {$0 == (self.languageLabel.text ?? "")}) {
            currentIndex = index
        }
        let picker = ActionSheetStringPicker(title: "Select Language".localized,
                                     rows: languageArr,
                                     initialSelection: currentIndex,
                                     doneBlock: { picker, value, index in
                                        self.languageLabel.text = languageArr[value]
                                        if value == 0 {
                                            PPLocalization.sharedInstance.setLanguage(language: "en")
                                            appDelegate.getlanguageDirection()
                                            Utilities.setIsArabic(false)
                                        } else {
                                            PPLocalization.sharedInstance.setLanguage(language: "ar")
                                            appDelegate.getlanguageDirection()
                                            Utilities.setIsArabic(true)
                                        }
                                        appDelegate.initRootView()
                                        return
                                     },
                                     cancel: { picker in
                                        return
                                     },
                                     origin: sender)
        let okButton = UIButton()
        let cancelButton = UIButton()
        okButton.setTitleColor(UIColor.systemBlue, for: .normal)
        cancelButton.setTitleColor(UIColor.systemBlue, for: .normal)
        okButton.setTitle(Key.Messages.DoneTitle.localized, for: .normal)
        cancelButton.setTitle(Key.Messages.CancelTitle.localized, for: .normal)
        let customDoneButton = UIBarButtonItem.init(customView: okButton)
        let customCancelButton = UIBarButtonItem.init(customView: cancelButton)
        picker?.setDoneButton(customDoneButton)
        picker?.setCancelButton(customCancelButton)
        picker?.show()
    }
}
extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == todayDealCollectionView {
            return offersObject?.deal_of_the_day?.count ?? 0
        } else {
            return categoryList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == todayDealCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "todayCell", for: indexPath) as! InterestCell
            cell.loadDeal(dealObject: self.offersObject?.deal_of_the_day?[indexPath.row])
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.loadCategoryText(category: categoryList[indexPath.row].localized, index: indexPath.row)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == todayDealCollectionView {
            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
            if (guestCheck ?? false) {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = "Locked".localized
                    popVC.subTitleT = "Login to see offer details".localized
                    popVC.popUpType = .double
                    popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                    popVC.btnText1 = "Login".localized
                    popVC.btnText2 = "Cancel".localized
                    popVC.delegate = self
                    self.tabBarController?.present(popVC, animated: true)
                }
            } else {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
                {
                    secondViewController.dealOfTheDayObject = self.offersObject?.deal_of_the_day?[indexPath.row]
                    secondViewController.offerID = "\(offersObject?.deal_of_the_day?[indexPath.row].offer_id ?? 0)"
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            }
        } else {
            if indexPath.row == 0 {
                if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as? CategoryDetailViewController
                {
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
            } else {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = "Locked".localized
                    popVC.subTitleT = "This offer category will be available soon".localized
                    popVC.popUpType = .single
                    popVC.popUpImage = UIImage(named: "Locked-graphic")
                    self.tabBarController?.present(popVC, animated: true)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == categoryCollectionView {
            let noOfCellsInRow = 3
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                let totalSpace = flowLayout.sectionInset.left
                    + flowLayout.sectionInset.right
                    + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

                let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

                return CGSize(width: size, height: size)
        }
        else {
            if (Utilities.getIsArabic() ?? false) {
                return CGSize(width: CGFloat(77), height: CGFloat(110))
            } else {
                return CGSize(width: CGFloat(77), height: CGFloat(102))
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
extension HomeViewController: AlertPopUpVCDelegate {
    func buttonTapped(type: ButtonTappedType, additional: String) {
        if type == .ok {
            Cache.removeAll()
            appDelegate.initRootView()
        }
    }
}
