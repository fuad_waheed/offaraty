//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit

class FavouriteCell: UITableViewCell {

    //MARK: UIImage Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var heartImage: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var favView: UIView!
    //MARK: UIView Outlets
    @IBOutlet weak var imageOuterView: UIView!
    
    var offerObject: Offers?
    
    weak var delegate: DealCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageOuterView.clipsToBounds = true
        imageOuterView.layer.cornerRadius = 5.0
        imageOuterView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func loadOffer(offer: Offers?, fromFav: Bool) {
        self.offerObject = offer
        productImageView.contentMode = .scaleAspectFill
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        productImageView.sd_setImage(with: URL(string: offer?.offer_image ?? ""),placeholderImage:placeIm)
        brandNameLabel.text = offer?.merchant?.business_name ?? offer?.merchant?.business_name_arabic
        productNameLabel.text = offer?.offer_title
        categoryLabel.text = offer?.category?.name
        ratingLabel.text = String(format: "%.1f", Double(offer?.total_rating ?? 0))
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            favView.alpha = 0
        } else {
            favView.alpha = 1
            if fromFav {
                heartImage.image = fav
            } else {
                if offer?.favourite ?? false {
                    heartImage.image = fav
                } else {
                    heartImage.image = notFav
                }
            }
        }
    }
    
    @IBAction func favouriteButtonTap(_ sender: Any) {
        if delegate != nil {
            delegate?.favBtnTapped(offer: self.offerObject)
        }
    }
    
}
