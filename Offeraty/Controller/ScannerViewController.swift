//
//  ScannerViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 11/04/2021.
//

import UIKit

class ScannerViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate  {
    
    //MARK: UIView Outlets
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var redeemButtonView: UIView!
    
    //MARK: UIImage Outlets
    @IBOutlet weak var userImageView: UIImageView!
    //MARK: UITextField Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var reviewBtn: UIButton!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var offerObject : Offers?
    var dealOfTheDayObject : DealOfTheDay?
    var redeemedCheck = false
    var user = Cache.extractUserObject(keyCurrentUser)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLangauge()
        view.addSubview(redeemButtonView)
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            redeemButtonView.visibility = .gone
            setUpSession()
        }
        else
        {
            redeemButtonView.visibility = .gone
            let alert = UIAlertController(title:Key.Messages.WarningTitle.localized, message: Key.Messages.CameraNotAvailable.localized, preferredStyle:UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title:Key.Messages.OKTitle.localized,style: UIAlertAction.Style.default, handler:{_ in
                self.popBack()
            }));
            self.present(alert, animated:true, completion:nil)
        }
    }
    
    func getRefreshedToken(redeemCode: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.callRedeemApi(rCode: redeemCode)
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.ScanViewController)
    }
    
    func callRedeemApi(rCode: String)  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        var transactionID : Int?
        var voucherNumber : String?
        if self.dealOfTheDayObject != nil {
            transactionID = self.dealOfTheDayObject?.order_detail?.transaction_id
            voucherNumber = self.dealOfTheDayObject?.order_detail?.voucher_number
        } else {
            transactionID = self.offerObject?.order_detail?.transaction_id
            voucherNumber = self.offerObject?.order_detail?.voucher_number
        }
        
        var params = [String:AnyObject]()
        params["transaction_id"]  = transactionID as AnyObject
        params["voucher_number"]  = voucherNumber as AnyObject
        params["store_pin"]  = rCode as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.Spend)/\(lang)", params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.showSuccess()
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(redeemCode: rCode)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func showSuccess() {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "RedeemSuccessPopUpVC") as? RedeemSuccessPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.delegate = self
            popVC.offerObject = self.offerObject
            popVC.dealOfTheDayObject = self.dealOfTheDayObject
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    func startAgain() {
        self.titleLabel.text = "Scan QR Code".localized
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
           
            self.backButton.setImage(backRight, for: .normal)
        } else {
            
            self.backButton.setImage(backLeft, for: .normal)
        }
    }
    
    func setUpSession() {
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        cameraView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported".localized, message: "Your device does not support scanning a code from an item. Please use a device with a camera.".localized, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: Key.Messages.OKTitle.localized, style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        user = Cache.extractUserObject(keyCurrentUser)
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name
        if !redeemedCheck {
            startAgain()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopRunning()
    }
    
    @IBAction func backButtonTap(_ sender: Any) {
        if redeemedCheck {
            self.navigationController?.popToRootViewController(animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func doneButtonTap(_ sender: Any) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
            secondViewController.offerObject = offerObject
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    func stopRunning() {
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        stopRunning()
        self.callRedeemApi(rCode: code)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
extension ScannerViewController: RedeemSuccessPopUpVCDelegate {
    func goTo(screen: GoToScreen) {
        if screen == .review {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.delegate = self
                popVC.offerObject = self.offerObject
                popVC.dealOfTheDayObject = self.dealOfTheDayObject
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
extension ScannerViewController: ReviewViewControllerDelegate {
    func goToHome(reviewed: Bool) {
        if reviewed {
            let alert = UIAlertController(title: "Reviews".localized, message: "Thank you for your valuable review.\nCheck out more exciting offers waiting for you !".localized, preferredStyle: UIAlertController.Style.alert)
            let ok = UIAlertAction(title: Key.Messages.OKTitle.localized, style: .default) { (action:UIAlertAction) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
