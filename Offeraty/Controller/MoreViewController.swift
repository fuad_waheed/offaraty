//
//  MoreVC.swift
//  Esan
//
//  Created by Fuad Waheed on 26/07/2021.
//  Copyright © 2021 Macbook Pro. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var textArr = ["About","Settings","Logout","Version:"]
    var imageArr = ["icon_footer_about","icon-Settings","icon_footer_logout","Old-icon-version"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar()
        
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            textArr = ["About".localized,"Logout".localized,"Version:".localized]
            imageArr = ["icon_footer_about","icon_footer_logout","Old-icon-version"]
        } else {
            textArr = ["About".localized,"Settings".localized,"Logout".localized,"Version:".localized]
            imageArr = ["icon_footer_about","icon-Settings","icon_footer_logout","Old-icon-version"]
        }
        self.title = "More".localized
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        hideNavigationBar()
    }
}
extension MoreViewController: UITableViewDelegate, UITableViewDataSource
{
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textArr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! MoreCell
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            if indexPath.row == 2 {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .disclosureIndicator
            }
        } else {
            if indexPath.row == 3 {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .disclosureIndicator
            }
        }
        
        cell.selectionStyle = .none
        cell.loadData(image: UIImage(named: imageArr[indexPath.row]), text: textArr[indexPath.row], index: indexPath.row)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if let VC = storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController
            {
                self.navigationController?.pushViewController(VC, animated: true)
            }
        case 1:
            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
            if (guestCheck ?? false) {
                Cache.removeAll()
                appDelegate.initRootView()
            } else {
                if let VC = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
                {
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        case 2:
            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
            if (guestCheck ?? false) {
                
            } else {
                Cache.removeAll()
                appDelegate.initRootView()
            }
        default:
            break
        }
    }
    
}





