//
//  APIManager.swift
//  Marketplace SDK
//
//  Created by Waqar Khalid on 5/7/19.
//  Copyright © 2019 Finja. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift
import NewRelic
enum URLType {
    case userManagement
    case offers
}
class OffaratyAPIManager {
    
    var alamoFireManager : Session?
    
    
    static func request(
        
        withAPIName method:String,
        params:[String:AnyObject],
        shouldAddLoader:Bool?=true,
        isGetRequest:Bool? = false,
        isPutRequest:Bool? = false,
        isDeleteRequest:Bool? = false,
        urlType:URLType,
        imageToSend:UIImage?,
        successBlock success:@escaping (_ jsonResponse:[String:AnyObject]?) -> (),
        failureBlock failure: @escaping (_ jsonResponse:[String:AnyObject]?) -> ())
    
    //                        completionBlockWithSuccess : @escaping successBlock,
    //                        completionBlockWithFailure : @escaping failureBlock)
    {
        
        if NetworkState.isConnected() {
            let keyChain = KeychainSwift()
            //let baseUrl = APIConstants.BaseURL
            
            
            if shouldAddLoader == true {
                OfferatySingleton.shared.showHud()
            }
            
            var headers:HTTPHeaders = [:]
            let token = Cache.extractString(keyAccessToken)
            if token != nil {
                print("Access Token = \(token ?? "")")
                headers["Authorization"] = "Bearer \(token ?? "")"
            }
            
            var finalUrl = ""
            switch urlType {
            case .userManagement:
                finalUrl = APIConstants.currentURLForUserManagement! + method
            case .offers:
                finalUrl = APIConstants.currentURLForOffers! + method
            }
            
            let device_id = UIDevice.current.identifierForVendor?.uuidString
            var params = params
            if !(isGetRequest ?? false) && !(isPutRequest ?? false) {
                params["device_id"] = (device_id ?? "") as AnyObject
            }
            print("params \(params)")
            print(finalUrl)
            let systemVersion = UIDevice.current.systemVersion
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
            do {
                
                //print("KBaseURL \(baseUrl)")
                print("URL EndPoint \(method)")
                print("params \(params)")
                
                if imageToSend != nil {
                    OfferatySingleton.shared.alamoFireManager?.upload(multipartFormData: { (multipartFormData) in
                        multipartFormData.append(imageToSend!.jpegData(compressionQuality: 0.5)!, withName: "user_image", fileName: "file.jpeg", mimeType: "image/jpeg")
                        for (key, value) in params {
                            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                        }
                    }, to:URL(string: finalUrl)!, headers: headers).responseJSON
                    { (responseObj) in
                        switch responseObj.result {
                        case .success(let value):
                            
                            guard let json = value as? [String:AnyObject] else {
                                
                                success(nil)
                                return
                            }
                            print("api successfully called")
                            
                            print("got response \(json)")
                            
                            if let code = json["code"] as? Int{
                                if code > 199 && code < 301 {
                                    if let detail = json[keyMessage] as? String {
                                        let dict = [keyMessage:detail, "code":(responseObj.response?.statusCode ?? 0)] as? [String : AnyObject]
                                        failure(dict)
                                    } else {
                                        success(nil)
                                    }
                                } else {
                                    OfferatySingleton.shared.hideHud()
                                    Utilities.showAlert(title: "", message: json["message"] as? String, alertType: .Failure)
                                    success(nil)
                                }
                            }
                            else{
                                if let detail = json[keyMessage] as? String {
                                    let dict = [keyMessage:detail, "code":(responseObj.response?.statusCode ?? 0)] as? [String : AnyObject]
                                    failure(dict)
                                } else {
                                    success(nil)
                                }
                            }
                            
                        case .failure(let error):
                            print("error is \(error)")
                            print("\n\n===========Error===========")
                            print("Error Code: \(error._code)")
                            print("Error Messsage: \(error.localizedDescription)")
                            if let data = responseObj.data, let str = String(data: data, encoding: String.Encoding.utf8){
                                print("Server Error: " + str)
                            }
                            debugPrint(error as Any)
                            print("===========================\n\n")
                            print("error is \(error)")
                            if error._code == NSURLErrorTimedOut {
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time".localized, alertType: .Failure)
                                print("Request timeout!")
                            }
                            
                            else{
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time".localized, alertType: .Failure)
                                
                                print("500 error!")
                            }
                        }
                        
                    }
                }
                else if !isGetRequest! && !isPutRequest! && !isDeleteRequest! {
                    
                    OfferatySingleton.shared.alamoFireManager?.request(URL(string: finalUrl)!, method: .post, parameters: params.count>0 ? params : nil, headers: headers).responseJSON { (responseObj) in
                        switch responseObj.result {
                        case .success(let value):
                            
                            guard let json = value as? [String:AnyObject] else {
                                
                                success(nil)
                                return
                            }
                            print("api successfully called")
                            
                            print("got response \(json)")
                            
                            if let code = json["code"] as? Int{
                                if code > 199 && code < 301 {
                                    success(json)
                                } else {
                                    failure(json)
                                }
                            }
                            else{
                                if let code = responseObj.response?.statusCode {
                                    if code > 199 && code < 301 {
                                        success(json)
                                    } else {
                                        failure(json)
                                    }
                                } else {
                                    failure(json)
                                }
                            }
                            
                        case .failure(let error):
                            print("error is \(error)")
                            print("\n\n===========Error===========")
                            print("Error Code: \(error._code)")
                            print("Error Messsage: \(error.localizedDescription)")
                            if let data = responseObj.data, let str = String(data: data, encoding: String.Encoding.utf8){
                                print("Server Error: " + str)
                            }
                            debugPrint(error as Any)
                            print("===========================\n\n")
                            print("error is \(error)")
                            if error._code == NSURLErrorTimedOut {
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time".localized, alertType: .Failure)
                                print("Request timeout!")
                            }
                            
                            else{
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time".localized, alertType: .Failure)
                                
                                print("500 error!")
                            }
                        }
                        
                    }
                }
                else if isPutRequest! {
                    OfferatySingleton.shared.alamoFireManager?.request(URL(string: finalUrl)!, method: .put, parameters: params.count>0 ? params : nil, headers: headers).responseJSON { (responseObj) in
                        
                        switch responseObj.result {
                        case .success(let value):
                            
                            guard let json = value as? [String:AnyObject] else {
                                
                                success(nil)
                                return
                            }
                            
                            print("api successfully called")
                            print("got response \(json)")
                            if responseObj.response?.statusCode == 400 {
                                failure(json)
                                return
                            }
                            if let code = json["code"] as? Int{
                                if code > 199 && code < 301 {
                                    success(json)
                                } else {
                                    failure(json)
                                }
                            }
                            else{
                                if let code = responseObj.response?.statusCode {
                                    if code > 199 && code < 301 {
                                        success(json)
                                    } else {
                                        failure(json)
                                    }
                                } else {
                                    failure(json)
                                }
                            }
                            
                        case .failure(let error):
                            print("error is \(error)")
                            print("\n\n===========Error===========")
                            print("Error Code: \(error._code)")
                            print("Error Messsage: \(error.localizedDescription)")
                            if let data = responseObj.data, let str = String(data: data, encoding: String.Encoding.utf8){
                                print("Server Error: " + str)
                            }
                            debugPrint(error as Any)
                            print("===========================\n\n")
                            if error._code == NSURLErrorTimedOut {
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                print("Request timeout!")
                            }
                            
                            else{
                                
                                if method == APIConstants.changePassword {
                                    success(nil)
                                }
                                else{
                                    OfferatySingleton.shared.hideHud()
                                    Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                    print("500 error!")
                                }
                                
                            }
                        }
                        
                    }
                }
                else if isGetRequest!{
                    // print("URL is \((baseUrl! + method))")
                    
                    OfferatySingleton.shared.alamoFireManager?.request(URL(string: finalUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!, method: .get, parameters: params.count>0 ? params : nil, headers: headers).responseJSON { (responseObj) in
                        
                        switch responseObj.result {
                        case .success(let value):
                            
                            guard let json = value as? [String:AnyObject] else {
                                
                                success(nil)
                                return
                            }
                            print("api successfully called")
                            print("got response \(json)")
                            if let code = json["code"] as? Int{
                                if code > 199 && code < 301 {
                                    success(json)
                                } else {
                                    failure(json)
                                }
                            }
                            else{
                                if let code = responseObj.response?.statusCode {
                                    if code > 199 && code < 301 {
                                        success(json)
                                    } else {
                                        failure(json)
                                    }
                                } else {
                                    failure(json)
                                }
                            }
                            
                            
                        case .failure(let error):
                            print("error is \(error)")
                            print("\n\n===========Error===========")
                            print("Error Code: \(error._code)")
                            print("Error Messsage: \(error.localizedDescription)")
                            if let data = responseObj.data, let str = String(data: data, encoding: String.Encoding.utf8){
                                print("Server Error: " + str)
                            }
                            debugPrint(error as Any)
                            print("===========================\n\n")
                            if error._code == NSURLErrorTimedOut {
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                print("Request timeout!")
                            }
                            else{
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                print("500 error!")
                            }
                            
                        }
                        
                    }
                } else {
                    // print("URL is \((baseUrl! + method))")
                    OfferatySingleton.shared.alamoFireManager?.request(URL(string: finalUrl)!, method: .delete, parameters: params.count>0 ? params : nil, headers: headers).responseJSON { (responseObj) in
                        
                        switch responseObj.result {
                        case .success(let value):
                            
                            guard let json = value as? [String:AnyObject] else {
                                
                                success(nil)
                                return
                            }
                            print("api successfully called")
                            print("got response \(json)")
                            if let code = json["code"] as? Int{
                                if code > 199 && code < 301 {
                                    success(json)
                                } else {
                                    failure(json)
                                }
                            }
                            else{
                                if let code = responseObj.response?.statusCode {
                                    if code > 199 && code < 301 {
                                        success(json)
                                    } else {
                                        failure(json)
                                    }
                                } else {
                                    failure(json)
                                }
                            }
                            
                            
                        case .failure(let error):
                            print("error is \(error)")
                            print("\n\n===========Error===========")
                            print("Error Code: \(error._code)")
                            print("Error Messsage: \(error.localizedDescription)")
                            if let data = responseObj.data, let str = String(data: data, encoding: String.Encoding.utf8){
                                print("Server Error: " + str)
                            }
                            debugPrint(error as Any)
                            print("===========================\n\n")
                            if error._code == NSURLErrorTimedOut {
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                print("Request timeout!")
                            }
                            else{
                                OfferatySingleton.shared.hideHud()
                                Utilities.showAlert(title: "", message: "Unexpected system error. Please try after some time", alertType: .Failure)
                                print("500 error!")
                            }
                            
                        }
                        
                    }
                }
                
            }
        } else {
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: kInternetErrorMessage, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            print("Network Connection Error")
        }
        
        
        
    }
    struct AlamofireRequestModal {
        var isGetRequest: Bool
        var apiName: String
        var parameters: [String: AnyObject]?
        var headers: [String: String]?
        var shouldAddLoader : Bool
        let systemVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        init() {
            isGetRequest = false
            apiName = ""
            parameters = nil
            shouldAddLoader = false
            headers = ["Content-Type": "application/json"]
        }
    }
    class func showMaintanceModeError(errorMessage:String) {
        OfferatySingleton.shared.hideHud()
        Utilities.showAlert(title: "", message: errorMessage, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
    }
}
class NetworkState {
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
