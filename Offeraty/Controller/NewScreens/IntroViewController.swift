//
//  OnboardingVC.swift
//  Vaultdax
//
//  Created by Fuad Waheed on 25/06/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit

class IntroViewController: BaseViewController {
    
    //MARK: UICollectionView Outlets
    @IBOutlet weak var collectionV: UICollectionView!
    
    //MARK: UIPageControl Outlets
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var alreadylabel: UILabel!
    
    var currentIndex = 0
    
    var dataArr = [["Deals and discount offers","intro1","Grab the extraordinary"],["Do you give feedback?","intro2","Taking time to analyze"],["Surprise deals","intro3","Discover great local deals"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout();
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        flowLayout.scrollDirection = .horizontal
        if UIDevice.current.hasNotch {
            flowLayout.sectionInset = UIEdgeInsets(top: -48, left: 0, bottom: 0, right: 0)
        } else {
            flowLayout.sectionInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        }
        
        self.collectionV.collectionViewLayout = flowLayout
        self.collectionV.isPagingEnabled = true
        checkLangauge()
        // Do any additional setup after loading the view.
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            pageControl.semanticContentAttribute = .forceRightToLeft
            signInButton.contentHorizontalAlignment = .right
        } else {
            pageControl.semanticContentAttribute = .forceLeftToRight
            signInButton.contentHorizontalAlignment = .center
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    
    @IBAction func skipBtn(_ sender: Any) {
        let index = IndexPath(row: 2, section: 0)
        collectionV.isPagingEnabled = false
        collectionV.scrollToItem(
            at: index,
            at: .centeredHorizontally,
            animated: true
        )
        collectionV.isPagingEnabled = true
    }
    @IBAction func signInBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "OnBoarding", bundle: nil)
        if let secondViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterParentViewController") as? RegisterParentViewController {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    @IBAction func getStartedBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "OnBoarding", bundle: nil)
        if let secondViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterParentViewController") as? RegisterParentViewController {
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
}
extension IntroViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionV.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        if UIDevice.current.hasNotch {
            cell.loadData(head: dataArr[indexPath.row][0].localized, image: "introNotch\(indexPath.row)", desc: dataArr[indexPath.row][2].localized)
        } else {
            cell.loadData(head: dataArr[indexPath.row][0].localized, image: dataArr[indexPath.row][1], desc: dataArr[indexPath.row][2].localized)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
        currentIndex = indexPath.row
        if indexPath.row == 2 {
            self.getStartedButton.fadeIn()
            self.skipButton.fadeOut()
        } else {
            self.getStartedButton.fadeOut()
            self.skipButton.fadeIn()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionV.frame.width, height: self.collectionV.frame.height)
    }
    
}
