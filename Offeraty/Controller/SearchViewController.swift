//
//  PopUpViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 15/04/2021.
//

import UIKit
import FirebaseAnalytics

class SearchViewController: BaseViewController {
    
    //MARK: UITextField Outlets
    @IBOutlet weak var searchField: UITextField!
    
    //MARK: UITableView Outlet
    @IBOutlet weak var searchTableView: UITableView!
    
    var user = Cache.extractUserObject(keyCurrentUser)
    var searchObject: SearchOffers?
    
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        IQKeyboardManager.shared.disabledToolbarClasses = [SearchViewController.self]
        super.viewDidLoad()
        self.searchField.setLeftPaddingPoints(10.0)
        self.searchTableView.tableFooterView = UIView()
        searchTableView.register(UINib(nibName: "FavouriteCell", bundle: nil), forCellReuseIdentifier: "FavouriteCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchField.becomeFirstResponder()
        user = Cache.extractUserObject(keyCurrentUser)
        checkLangauge()
        hideNavigationBar()
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.searchField.textAlignment = .right
            self.backButton.setImage(blueNext, for: .normal)
        } else {
            self.searchField.textAlignment = .left
            self.backButton.setImage(blueBack, for: .normal)
        }
    }
    
    func searchOffer(searchString: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.search)/\(lang)?search_val=\(searchString)&user_id=\(user?.user_id ?? 0)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.searchObject = try JSONDecoder().decode(SearchOffers.self, from: userData)
                        if self?.searchObject == nil || self?.searchObject?.offers == nil {
                            self?.showNoOfferFound()
                        }
                        self?.searchTableView.reloadData()
                    }
                    
                    catch {
                        self?.showNoOfferFound()
                        print("could not decode \(error)")
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "search", id: 0, searchWord: searchString)
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func showNoOfferFound() {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.titleT = "No offers found".localized
            popVC.subTitleT = "Try another offer"
            popVC.popUpImage = UIImage(named: "no offer")
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    
    func callFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        var params = [String:AnyObject]()
        params["user_id"]  = (user?.user_id ?? 0) as AnyObject
        params["offer_id"]  = (offerID ?? 0) as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.favourite, params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = Key.Messages.AddedToFav.localized
                    popVC.subTitleT = Key.Messages.OfferAddedFavourites.localized
                    popVC.popUpType = .double
                    popVC.additionalCheck = "fav"
                    popVC.popUpImage = UIImage(named: "added to favourite")
                    popVC.btnText1 = Key.Messages.GoToFav.localized
                    popVC.btnText2 = Key.Messages.CloseTitle.localized
                    popVC.delegate = self
                    self?.tabBarController?.present(popVC, animated: true)
                }
                if let index = self?.searchObject?.offers?.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.searchObject?.offers?[index].favourite = true
                    UIView.performWithoutAnimation {
                        self?.searchTableView.reloadData()
                    }
                }
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "fav",id:offerID, searchWord: "")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }

        }
    }
    
    func callUnFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        let params = [String:AnyObject]()
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.unFavourite)?user_id=\(user?.user_id ?? 0)&offer_id=\(offerID ?? 0)", params: params,shouldAddLoader: true,isDeleteRequest: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                Utilities.showALertWithTag(title: Key.Messages.FavouriteTitle, message: Key.Messages.OfferRemovedFavourites.localized)
                if let index = self?.searchObject?.offers?.firstIndex(where: {$0.offer_id == offerID}) {
                    self?.searchObject?.offers?[index].favourite = false
                    UIView.performWithoutAnimation {
                        self?.searchTableView.reloadData()
                    }
                }
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "unfav", id: offerID, searchWord: "")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func getRefreshedToken(from: String, id:Int?, searchWord: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "unfav" {
                                self?.callUnFavouriteAPI(offerID: id)
                            }  else if from == "fav" {
                                self?.callFavouriteAPI(offerID: id)
                            } else {
                                self?.searchOffer(searchString: searchWord)
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
}
extension SearchViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchObject?.offers?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.loadOffer(offer: self.searchObject?.offers?[indexPath.row], fromFav: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Locked".localized
                popVC.subTitleT = "Login to see offer details".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "offer-locked-graphics")
                popVC.btnText1 = "Login".localized
                popVC.btnText2 = "Cancel".localized
                popVC.delegate = self
                self.tabBarController?.present(popVC, animated: true)
            }
        } else {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
            {
                secondViewController.offerObject = self.searchObject?.offers?[indexPath.row]
                secondViewController.offerID = "\(self.searchObject?.offers?[indexPath.row].offer_id ?? 0)"
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
    }
}
extension SearchViewController: AlertPopUpVCDelegate {
    func buttonTapped(type: ButtonTappedType, additional: String) {
        if type == .ok && additional == "" {
            Cache.removeAll()
            appDelegate.initRootView()
        } else if type == .ok {
            self.tabBarController?.selectedIndex = 2
        }
    }
}
extension SearchViewController: DealCellDelegate {
    func favBtnTapped(offer: Offers?) {
        if offer?.favourite ?? false {
            callUnFavouriteAPI(offerID: offer?.offer_id)
        } else {
            callFavouriteAPI(offerID: offer?.offer_id)
        }
    }
}
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.text ?? "") == "" {
            Utilities.showALertWithTag(title: "Alert", message: "No search text entered.".localized)
        } else {
            Analytics.logEvent("search", parameters: [
                "search_term": textField.text ?? ""
              ])
            self.searchObject = nil
            self.searchTableView.reloadData()
            searchOffer(searchString: textField.text!)
            self.view.endEditing(true)
        }
        return true
    }
    
}
