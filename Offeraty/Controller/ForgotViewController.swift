//
//  ForgotViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 03/06/2021.
//

import UIKit

class ForgotViewController: BaseViewController {
    
    //MARK: TextFields Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tokenLabel: UILabel!
    @IBOutlet weak var emailBorderLine: UIView!
    @IBOutlet weak var tokenBorderLine: UIView!
    @IBOutlet weak var tokenView: UIView!
    
    //MARK: Images Outlets
    @IBOutlet weak var tokenIconImage: UIImageView!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var headingLabel2: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    var tokenCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkLangauge()
        checkToken()
    }
    
    func checkToken() {
        headingLabel.text = "Forgot".localized
        if tokenCheck {
            headingLabel2.text = "Token?".localized
            subHeadingLabel.text = "Please enter the email address".localized
            tokenView.visibility = .gone
        } else {
            headingLabel2.text = "Password?".localized
            subHeadingLabel.text = "Please enter the email address and the token number".localized
            tokenView.visibility = .visible
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.emailTextField.textAlignment = .right
            self.tokenTextField.textAlignment = .right
            self.backBtn.setImage(backRight, for: .normal)
            self.nextBtn.setImage(goNext, for: .normal)
        } else {
            self.emailTextField.textAlignment = .left
            self.tokenTextField.textAlignment = .left
            self.backBtn.setImage(backLeft, for: .normal)
            self.nextBtn.setImage(goPrevious, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        if tokenCheck {
            callForgotTokenAPI()
        } else {
            callForgotPassAPI()
        }
    }
    
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            return false
        } else if(self.emailTextField.text?.isEmpty == true){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.EmailError.localized)
            return false
        } else if(Utilities.isValidEmail(email: self.emailTextField.text!) == false){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.InvalidEmailError.localized)
            return false
        } else if !tokenCheck {
            if(self.tokenTextField.text?.isEmpty == true) {
                Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.TokenError.localized)
                return false
            }
        }
        return true
    }
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        Utilities.showAlert(title: "", message: Key.Messages.EmployerTokenDescription.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Info)
    }
    
    func callForgotPassAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        
        var params = [String:AnyObject]()
        params["email"]  = emailTextField.text! as AnyObject
        params["token"]  = tokenTextField.text! as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.changePassword)/otp/\(lang)?email=\(emailTextField.text!)&token=\(tokenTextField.text!)", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
//                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self?.changePassword(params: params)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if (errorResponse?[keyCode] as? Int) == 429 {
//                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self.changePassword(params: params)
            } else {
                Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            }
        }
    }
    
    func callForgotTokenAPI() {
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        var params = [String:AnyObject]()
        params["email"]  = emailTextField.text! as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.forgotToken)/otp/\(lang)?email=\(emailTextField.text!)", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
//                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self?.changePassword(params: params)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if (errorResponse?[keyCode] as? Int) == 429 {
//                Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                self.changePassword(params: params)
            } else {
                Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            }
        }
    }
    
    func changePassword(params: [String:AnyObject]) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
        {
            secondViewController.tokenCheck = self.tokenCheck
            secondViewController.email = emailTextField.text!
            secondViewController.params = params
            secondViewController.delegate = self
            secondViewController.modalPresentationStyle = .overCurrentContext
            self.present(secondViewController, animated: true, completion: nil)
        }
    }
}
extension ForgotViewController: OTPVerificationVCDelegate {
    func goToLogin() {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ForgotViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailLabel.textColor = AppColor.appLoginSelected
            tokenLabel.textColor = AppColor.appLoginUnSelected
            emailBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
            tokenBorderLine.backgroundColor = AppColor.appBorderGray
        } else {
            emailLabel.textColor = AppColor.appLoginUnSelected
            tokenLabel.textColor = AppColor.appLoginSelected
            emailBorderLine.backgroundColor = AppColor.appBorderGray
            tokenBorderLine.backgroundColor = AppColor.appPrimaryGreenColor
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailLabel.textColor = AppColor.appLoginUnSelected
        tokenLabel.textColor = AppColor.appLoginUnSelected
        emailBorderLine.backgroundColor = AppColor.appBorderGray
        tokenBorderLine.backgroundColor = AppColor.appBorderGray
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tokenTextField {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
