//
//  LocationTableCell.swift
//  AirMiles
//
//  Created by Waqar Khalid on 4/3/20.
//  Copyright © 2020 Merit Incentive. All rights reserved.
//

import UIKit

class ShareTableCell: UITableViewCell {

    //MARK: UImageView Outlets
    @IBOutlet weak var iconImageView: UIImageView!
    
    //MARK: UILabels Outlets
    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    //MARK: UIImageView Outlets
    @IBOutlet weak var arrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(isArabicCheck:Bool) {
        if isArabicCheck {
            arrowImage.image = backLeft
        } else {
            arrowImage.image = backRight
        }
    }

}
