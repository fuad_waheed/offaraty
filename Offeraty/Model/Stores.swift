//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Stores: Codable {
    var submerchant_id             : Int?
    var business_name              : String?
    var about_merchant             : String?
    var address                    : String?
    var latitude                   : String?
    var longitude                  : String?
    var mobile                     : String?
    var distance                   : Double?
}
