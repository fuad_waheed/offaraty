//
//  ReviewViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 12/04/2021.
//

import UIKit
protocol ReviewViewControllerDelegate: AnyObject {
    func goToHome(reviewed: Bool)
}

class ReviewViewController: BaseViewController {
    
    //MARK: UILabel Outlets
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var scrollV: UIScrollView!
    
    //MARK: KMPlaceHolderTextView Outlets
    @IBOutlet weak var reviewTextView: KMPlaceholderTextView!
    
    //MARK: CosmosRatingView Outlets
    @IBOutlet weak var ratingView: CosmosView!
    
    
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    weak var delegate: ReviewViewControllerDelegate?
    
    var rating = 1
    var apiCheck = 0
    var user = Cache.extractUserObject(keyCurrentUser)
    var offerObject : Offers?
    var reviewOptions : [ReviewOptions]?
    var dealOfTheDayObject : DealOfTheDay?
    var selectedOptions = [ReviewOptions]()

    override func viewDidLoad() {
        super.viewDidLoad()
        ratingView.settings.fillMode = .full
        self.reviewTextView.placeholder = "Start typing here...".localized
        checkLangauge()
        ratingView.didFinishTouchingCosmos = { rating in
            self.rating = Int(rating)
        }
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func loadData() {
        productTitleLabel.text = dealOfTheDayObject?.offer_title ?? offerObject?.offer_title
        let df = DateFormatter()
        df.timeZone = .current
        df.dateFormat = "d MMM YYYY | h:mm a"
        dateTimeLabel.text = df.string(from: Date())
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.reviewTextView.textAlignment = .right
            self.ratingView.semanticContentAttribute = .forceRightToLeft
            self.ratingView.rating = 1
        } else {
            self.reviewTextView.textAlignment = .left
            self.ratingView.semanticContentAttribute = .forceLeftToRight
            self.ratingView.rating = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        user = Cache.extractUserObject(keyCurrentUser)
        getReviewOptions()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.ReviewViewController)
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
    }
    
    func getReviewOptions()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.reviewOptions)/\(lang)?offer_id=\(self.dealOfTheDayObject?.offer_id ?? self.offerObject?.offer_id ?? 0)", params: params, shouldAddLoader: false,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.reviewOptions = try JSONDecoder().decode([ReviewOptions].self, from: userData)
                        self?.reloadCollectionV()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        self?.reloadCollectionV()
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                self.reloadCollectionV()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "review")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func reloadCollectionV() {
        self.collectionV.reloadData {
            self.collectionHeight.constant = self.collectionV.contentSize.height
            self.animateChanges()
        }
    }
    
    func animateChanges() {
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func additionalFeedbackBtn(_ sender: Any) {
        self.textViewHeight.constant = 100
        self.animateChanges()
        self.scrollV.scrollToView(view: reviewTextView, animated: true)
        self.reviewTextView.becomeFirstResponder()
    }
    
    @IBAction func rateButton(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        
        callRateAPI()
        
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.goToHome(reviewed: false)
        })
    }
    
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
            return false
        }
        else if(rating == 0){
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.RatingError.localized)
            return false
        }
        return true
    }
    
    func callRateAPI()  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        let user = Cache.extractUserObject(keyCurrentUser)
        
        var offerID : Int?
        if dealOfTheDayObject != nil {
            offerID = dealOfTheDayObject?.offer_id
        } else {
            offerID = offerObject?.offer_id
        }
        var ids = [Int]()
        for i in selectedOptions {
            ids.append((i.id ?? 0))
        }
        let reviewOptionsStr = ids.map { String($0) }
            .joined(separator: ", ")
        
        var params = [String:AnyObject]()
        params["offer_id"]  = offerID as AnyObject
        params["user_id"]  = user?.user_id as AnyObject
        params["user_name"]  = user?.user_name as AnyObject
        params["rating"] = rating as AnyObject
        params["transaction_id"] = offerObject?.order_detail?.transaction_id as AnyObject
        params["review"] = (self.reviewTextView.text ?? "") as AnyObject
        if reviewOptionsStr != "" {
            params["review_options_ids"] = reviewOptionsStr as AnyObject
        }
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.rateReview)/\(lang)", params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.goToHome(reviewed: true)
                })
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "rate")
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
            
        }
    }
    
    func getRefreshedToken(from: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "review" {
                                self?.getReviewOptions()
                            } else {
                                self?.callRateAPI()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
}
extension ReviewViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviewOptions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewOptionsCell", for: indexPath) as! ReviewOptionsCell
        cell.loadData(index: indexPath.row, option: reviewOptions?[indexPath.row], selectedOp: selectedOptions)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = selectedOptions.firstIndex(where: {$0.id == reviewOptions?[indexPath.row].id}) {
            selectedOptions.remove(at: index)
        } else {
            if reviewOptions?[indexPath.row] != nil {
                selectedOptions.append((reviewOptions?[indexPath.row])!)
            }
        }
        collectionV.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat(149), height: CGFloat(45))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
