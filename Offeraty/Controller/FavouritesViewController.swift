//
//  PopUpViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 15/04/2021.
//

import UIKit

class FavouritesViewController: BaseViewController {
    
    //MARK: UILabel Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: UIImage Outlets
    @IBOutlet weak var userImageView: UIImageView!
    
    //MARK: UITableView Outlet
    @IBOutlet weak var favouritesTableView: UITableView!
    
    var user = Cache.extractUserObject(keyCurrentUser)
    var favArray : [Offers]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.favouritesTableView.tableFooterView = UIView()
        favouritesTableView.register(UINib(nibName: "FavouriteCell", bundle: nil), forCellReuseIdentifier: "FavouriteCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFavourites()
        user = Cache.extractUserObject(keyCurrentUser)
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name
        checkLangauge()
    }
    
    func checkLangauge() {
//        if (Utilities.getIsArabic() ?? false) {
//            self.searchField.textAlignment = .right
//        } else {
//            self.searchField.textAlignment = .left
//        }
    }
    
    func getFavourites()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.favourites_list)/\(lang)?user_id=\(user?.user_id ?? 0)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.favArray = try JSONDecoder().decode([Offers].self, from: userData)
                        self?.favouritesTableView.reloadData()
                    }
                    
                    catch {
                        self?.favArray?.removeAll()
                        self?.favouritesTableView.reloadData()
                        print("could not decode \(error)")
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                self.favArray?.removeAll()
                self.favouritesTableView.reloadData()
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "get", id: 0)
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    func getRefreshedToken(from: String, id:Int?)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "unfav" {
                                self?.callUnFavouriteAPI(offerID: id)
                            } else {
                                self?.getFavourites()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.FavoritesViewController)
    }
    
    func callUnFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        let params = [String:AnyObject]()
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.unFavourite)?user_id=\(user?.user_id ?? 0)&offer_id=\(offerID ?? 0)", params: params,shouldAddLoader: true,isDeleteRequest: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                Utilities.showALertWithTag(title: Key.Messages.FavouriteTitle.localized, message: Key.Messages.OfferRemovedFavourites.localized.localized)
                self?.getFavourites()
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "unfav", id: offerID)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
}
extension FavouritesViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.loadOffer(offer: self.favArray?[indexPath.row], fromFav: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DealDetailViewcontroller") as? DealDetailViewcontroller
        {
            secondViewController.offerObject = self.favArray?[indexPath.row]
            secondViewController.offerID = "\(self.favArray?[indexPath.row].offer_id ?? 0)"
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
}
extension FavouritesViewController: DealCellDelegate {
    func favBtnTapped(offer: Offers?) {
        callUnFavouriteAPI(offerID: offer?.offer_id)
    }
}
