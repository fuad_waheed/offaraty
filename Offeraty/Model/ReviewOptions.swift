//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class ReviewOptions: Codable {
    var id                : Int?
    var review_text       : String?
}
