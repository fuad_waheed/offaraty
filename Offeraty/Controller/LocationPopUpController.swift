//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit
import MapKit
import HMSegmentedControl
import GoogleMaps
import FirebaseDynamicLinks

class LocationPopUpController: BaseViewController {
    
    //MARK: UIImage Outlets
    @IBOutlet weak var storeImage: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    var offerObject: Offers?
    var storeObject: Stores?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1, animations: { () -> Void in
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.LocationViewController)
    }
    
    func setUpView() {
        let distance = Int(storeObject?.distance ?? 0.0)
        let km = "km".localized
        kmLabel.text = "\(distance) \(km)"
        nameLabel.text = storeObject?.business_name
        addressLabel.text = storeObject?.address
    }
    
    @IBAction func showDirectionsTap(_ sender: Any) {
        
        let latitude = Double(storeObject?.latitude ?? "0.0") ?? 0.0
        let longitude = Double(storeObject?.longitude ?? "0.0") ?? 0.0
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            UIApplication.shared.open(URL(string:
                "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!, options: [:], completionHandler: nil)
            
        } else {
            // if GoogleMaps App is not installed
            UIApplication.shared.open(URL(string:
                "https://maps.google.com/?q=@\(latitude),\(longitude)")!, options: [:], completionHandler: nil)
        }
    }
}
