//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Filters: Codable {
    var rating                  : [Int]?
    var nav_categories          : [Categories]?
    var limited_time_offer      : [Bool]?
    var deal_of_the_day         : [Bool]?
    var city                    : [Cities]?
    var redemption_type         : [String]?
}
