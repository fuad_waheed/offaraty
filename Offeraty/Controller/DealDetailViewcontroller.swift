//
//  CategoriesViewController.swift
//  Offaraty
//
//  Created by Fuad Waheed on 18/03/2021.
//  Copyright © 2021 Fuad Waheed. All rights reserved.
//

import UIKit
import MapKit
import HMSegmentedControl
import GoogleMaps
import FirebaseDynamicLinks
import FirebaseAnalytics

class DealDetailViewcontroller: BaseViewController {
    
    //MARK: NSLayoutConstraint Outlets
    @IBOutlet weak var locationReviewsTableHeight: NSLayoutConstraint!
    
    //MARK: UIScrollView Outlets
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    //MARK: UIImage Outlets
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var onlineStoreImage: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratedByLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var addOnsLabel: UILabel!
    @IBOutlet weak var validTillLabel: UILabel!
    @IBOutlet weak var estimatedSavingLabel: UILabel!
    @IBOutlet weak var mapLabel: UILabel!
    
    //MARK: UITableView Outlet
    @IBOutlet weak var locationReviewsTableView: UITableView!
    
    //MARK: UIView Outlets
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var locationReviewsView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    //MARK: UITextView Outlets
    @IBOutlet weak var textView: UITextView!
    
    //MARK: MapView Outlets
    @IBOutlet weak var mapView: GMSMapView!
    
    //MARK: UISegmentControl Outlets
    @IBOutlet weak var optionsSegmentView: HMSegmentedControl!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var favBtnImage: UIImageView!
    
    var currentIndex = 0
    
    var mapCheck = false
    
    var isArabic = false
    var latitude = 0.0
    var longitude = 0.0
    
    var offerObject : Offers?
    var dealOfTheDayObject : DealOfTheDay?
    var offerID = ""
    let locationManager = CLLocationManager()
    var user = Cache.extractUserObject(keyCurrentUser)
    var gotLocation = false
    var fromLimited = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.delegate = self
        setUpView()
        getOfferNewObject(showLoader: true)
        checkLangauge()
        setUpSegmentControl()
        textViewSegmentSelect(scrollCheck:true)
        self.locationReviewsTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        dealImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        dealImageView.contentMode = .scaleToFill
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        if offerObject?.banner_image != nil || dealOfTheDayObject?.banner_image != nil {
            dealImageView.sd_setImage(with: URL(string: offerObject?.banner_image ?? dealOfTheDayObject?.banner_image ?? ""), placeholderImage:placeIm)
        } else {
            dealImageView.sd_setImage(with: URL(string: offerObject?.offer_image ?? dealOfTheDayObject?.offer_image ?? ""), placeholderImage:placeIm)
        }
        ratingLabel.text = String(format: "%.1f", Double(offerObject?.total_rating ?? 0))
        ratedByLabel.text = "( \(offerObject?.reviews?.count ?? 0) )"
        brandNameLabel.text = offerObject?.merchant?.business_name
        productNameLabel.text = offerObject?.offer_title ?? dealOfTheDayObject?.offer_title
        addOnsLabel.text = offerObject?.category?.name
        textView.text = offerObject?.offer_detail ?? dealOfTheDayObject?.offer_detail ?? "                                                                                                                                                              "
        textView.isEditable = false
        let validTillText = "Valid till".localized
        validTillLabel.text = "\(validTillText) \(offerObject?.end_date ?? "")"
        if offerObject?.favourite ?? false {
            favBtnImage.image = fav
        } else {
            favBtnImage.image = notFav
        }
        let estimatedText = "Estimated Saving".localized
        estimatedSavingLabel.text = "\(estimatedText) \(offerObject?.estimated_savings ?? 0.0)"
        if (offerObject?.purchase_type == "online"  ) {
            onlineStoreImage.alpha = 1
        }
    }
    
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    
    func getRefreshedToken(from: String, id:Int?, goTo: Bool)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "get" {
                                self?.getOfferNewObject(showLoader: false)
                            } else if from == "unfav" {
                                self?.callUnFavouriteAPI(offerID: id)
                            }  else if from == "fav" {
                                self?.callFavouriteAPI(offerID: id)
                            } else {
                                self?.callSaveApi(goToRedeem: goTo)
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func getOfferNewObject(showLoader: Bool)  {
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        var url = "\(APIConstants.getOffer)/\(lang)/\(offerID)"
        if longitude != 0.0 && latitude != 0.0 {
            url = url + "?cur_lat=\(latitude)&cur_long=\(longitude)&user_id=\(user?.user_id ?? 0)"
        } else {
            url = url + "?user_id=\(user?.user_id ?? 0)"
        }
        OffaratyAPIManager.request(withAPIName: url, params: [:], shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                do {
                    self?.offerObject = try JSONDecoder().decode(Offers.self, from: userData)
                    self?.setUpView()
                }
                
                catch {
                    print("could not decode \(error)")
                    Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "get", id:0, goTo: false)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            isArabic = true
            onlineStoreImage.image = UIImage(named: "onlineStoreArabic")
            self.backButton.setImage(backRight, for: .normal)
            textView.textAlignment = .right
        } else {
            onlineStoreImage.image = UIImage(named: "onlineStoreEng")
            isArabic = false
            self.backButton.setImage(backLeft, for: .normal)
            textView.textAlignment = .left
        }
    }
    
    func setUpSegmentControl() {
        if (Utilities.getIsArabic() ?? false) {
            if (offerObject?.purchase_type == "online") {
                optionsSegmentView.sectionTitles = ["About".localized,"T&C".localized,"Reviews".localized].reversed()
                optionsSegmentView.setSelectedSegmentIndex(UInt(2), animated: false)
            } else {
                optionsSegmentView.sectionTitles = ["About".localized,"T&C".localized,"Location".localized,"Reviews".localized].reversed()
                optionsSegmentView.setSelectedSegmentIndex(UInt(3), animated: false)
            }
        } else {
            if (offerObject?.purchase_type == "online") {
                optionsSegmentView.sectionTitles = ["About".localized,"T&C".localized,"Reviews".localized]
            } else {
                optionsSegmentView.sectionTitles = ["About".localized,"T&C".localized,"Location".localized,"Reviews".localized]
            }
        }
        optionsSegmentView.autoresizingMask = [.flexibleRightMargin, .flexibleWidth]
        optionsSegmentView.selectionStyle = .textWidthStripe
        optionsSegmentView.selectionIndicatorLocation = .bottom
        optionsSegmentView.selectionIndicatorColor = AppColor.appPrimaryPurpleColor
        optionsSegmentView.selectionIndicatorHeight = 2
        optionsSegmentView.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "#747C91") ?? .darkGray,
            NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 15.0)!
        ]
        optionsSegmentView.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.appNewGreen,
            NSAttributedString.Key.font: UIFont(name: "Effra-Medium", size: 15.0)!
        ]
        optionsSegmentView.segmentWidthStyle = .fixed
        optionsSegmentView.type = .text
        optionsSegmentView.addTarget(self, action: #selector(segmentedControlChangedValue(segmentedControl:)), for: .valueChanged)
    }
    
    func textViewSegmentSelect(scrollCheck:Bool) {
        DispatchQueue.main.async {
            self.textView.alpha = 1
            self.locationReviewsTableView.visibility = .gone
            self.textView.visibility = .visible
            self.optionsView.visibility = .gone
            self.optionsView.alpha = 0
            self.mapView.alpha = 0
            self.adjustUITextViewHeight()
            //            if scrollCheck {
            //                self.mainScrollView.scrollToView(view: self.optionsSegmentView, animated: false)
            //            }
        }
    }
    
    func locationUse() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func adjustUITextViewHeight()
    {
        DispatchQueue.main.async {
            self.textView.translatesAutoresizingMaskIntoConstraints = true
            self.textView.sizeToFitHeight()
            self.textView.isScrollEnabled = false
        }
    }
    
    func adjustUITextViewHeightRevert()
    {
        DispatchQueue.main.async {
            self.textView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    @objc func segmentedControlChangedValue(segmentedControl: HMSegmentedControl) {
        DispatchQueue.main.async { [weak self] in
            self?.mapLabel.text = "Map View".localized
            self?.mapCheck = false
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                if (Utilities.getIsArabic() ?? false) {
                    self?.mapView.alpha = 0
                    self?.adjustUITextViewHeightRevert()
                    self?.optionsView.visibility = .gone
                    self?.optionsView.alpha = 0
                    self?.textView.alpha = 0
                    self?.locationReviewsTableView.visibility = .visible
                    self?.locationReviewsView.alpha = 1
                    if (self?.offerObject?.reviews?.count ?? 0) == 0 {
                        self?.locationReviewsTableHeight.constant = 30
                    }
                } else {
                    self?.textView.text = self?.offerObject?.offer_detail
                    self?.textViewSegmentSelect(scrollCheck:true)
                }
                break
            case 1:
                if (Utilities.getIsArabic() ?? false) {
                    if (self?.offerObject?.purchase_type == "online") {
                        self?.textViewSegmentSelect(scrollCheck:true)
                        self?.textView.text = self?.offerObject?.terms_condition
                    } else {
                        self?.adjustUITextViewHeightRevert()
                        self?.optionsView.visibility = .visible
                        self?.optionsView.alpha = 1
                        self?.textView.alpha = 0
                        self?.locationReviewsTableView.visibility = .visible
                        self?.mapView.alpha = 0
                        if (self?.offerObject?.stores?.count ?? 0) == 0 {
                            self?.locationReviewsTableHeight.constant = 100
                        }
                    }
                } else {
                    self?.textView.text = self?.offerObject?.terms_condition
                    self?.textViewSegmentSelect(scrollCheck:true)
                }
                break
            case 2:
                if (Utilities.getIsArabic() ?? false) {
                    if (self?.offerObject?.purchase_type == "online") {
                        self?.textViewSegmentSelect(scrollCheck:true)
                        self?.textView.text = self?.offerObject?.offer_detail
                    } else {
                        self?.textViewSegmentSelect(scrollCheck:true)
                        self?.textView.text = self?.offerObject?.terms_condition
                    }
                } else {
                    if (self?.offerObject?.purchase_type == "online") {
                        self?.mapView.alpha = 0
                        self?.adjustUITextViewHeightRevert()
                        self?.optionsView.visibility = .gone
                        self?.optionsView.alpha = 0
                        self?.textView.alpha = 0
                        self?.locationReviewsTableView.visibility = .visible
                        self?.locationReviewsView.alpha = 1
                        if (self?.offerObject?.reviews?.count ?? 0) == 0 {
                            self?.locationReviewsTableHeight.constant = 100
                        }
                    } else {
                        self?.adjustUITextViewHeightRevert()
                        self?.optionsView.visibility = .visible
                        self?.optionsView.alpha = 1
                        self?.textView.alpha = 0
                        self?.locationReviewsTableView.visibility = .visible
                        self?.mapView.alpha = 0
                        if (self?.offerObject?.stores?.count ?? 0) == 0 {
                            self?.locationReviewsTableHeight.constant = 30
                        }
                        
                    }
                }
                break
            case 3:
                if (Utilities.getIsArabic() ?? false) {
                    self?.textViewSegmentSelect(scrollCheck:true)
                    self?.textView.text = self?.offerObject?.offer_detail
                } else {
                    self?.mapView.alpha = 0
                    self?.adjustUITextViewHeightRevert()
                    self?.optionsView.visibility = .gone
                    self?.optionsView.alpha = 0
                    self?.textView.alpha = 0
                    self?.locationReviewsTableView.visibility = .visible
                    self?.locationReviewsView.alpha = 1
                    if (self?.offerObject?.reviews?.count ?? 0) == 0 {
                        self?.locationReviewsTableHeight.constant = 30
                    }
                }
                break
            default:break
            }
            self?.currentIndex = Int(segmentedControl.selectedSegmentIndex)
            DispatchQueue.main.async {
                self?.locationReviewsTableView.reloadData()
                self?.animateChanges()
                self?.viewWillLayoutSubviews()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self?.animateChanges()
                self?.view.layoutIfNeeded()
                self?.viewWillLayoutSubviews()
                self?.viewDidLayoutSubviews()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        locationUse()
        user = Cache.extractUserObject(keyCurrentUser)
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name
    }
    
    func animateChanges()
    {
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func redeemButtonTap(_ sender: Any) {
        DispatchQueue.main.async {
            // image source alert
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "Redeem".localized
                popVC.popUpType = .double
                popVC.popUpImage = UIImage(named: "Redeem-graphic")
                popVC.imWidth = 107.0
                popVC.imHeight = 113.0
                popVC.delegate = self
                popVC.btnText1 = "Redeem Now".localized
                popVC.btnText2 = "     Save to My Vouchers".localized
                self.tabBarController?.present(popVC, animated: true)
            }
        }
    }
    
    func callSaveApi(goToRedeem: Bool)  {
        self.view.endEditing(true)
        
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        var offerID : Int?
        if dealOfTheDayObject != nil {
            offerID = dealOfTheDayObject?.offer_id
        } else {
            offerID = offerObject?.offer_id
        }
        
        var params = [String:AnyObject]()
        params["user_id"]  = self.user?.user_id as AnyObject
        params["offer_id"]  = offerID as AnyObject
        params["email"]  = self.user?.email as AnyObject
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.PurchaseOffer)/\(lang)", params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                do {
                    let tempObj = try JSONDecoder().decode(Offers.self, from: userData)
                    self?.dealOfTheDayObject?.order_detail = tempObj.order_detail
                    self?.offerObject?.order_detail = tempObj.order_detail
                    if goToRedeem {
                        if let secondViewController = self?.storyboard?.instantiateViewController(withIdentifier: "EnterPinOfferViewController") as? EnterPinOfferViewController
                        {
                            secondViewController.dealOfTheDayObject = self?.dealOfTheDayObject
                            secondViewController.offerObject = self?.offerObject
                            self?.navigationController?.pushViewController(secondViewController, animated: true)
                        }
                    } else {
                        Utilities.showALertWithTag(title: "My Vouchers".localized, message: "Offer added to My Vouchers successfully!".localized)
                    }
                }
                
                catch {
                    print("could not decode \(error)")
                    Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                }
            }
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "save", id:0, goTo: goToRedeem)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func callFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        var params = [String:AnyObject]()
        params["user_id"]  = (user?.user_id ?? 0) as AnyObject
        params["offer_id"]  = (offerID ?? 0) as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.favourite, params: params,shouldAddLoader: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                let storyBoard = UIStoryboard(name: "App", bundle: nil)
                if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.titleT = Key.Messages.AddedToFav.localized
                    popVC.subTitleT = Key.Messages.OfferAddedFavourites.localized
                    popVC.popUpType = .double
                    popVC.additionalCheck = "fav"
                    popVC.popUpImage = UIImage(named: "added to favourite")
                    popVC.btnText1 = Key.Messages.GoToFav.localized
                    popVC.btnText2 = Key.Messages.CloseTitle.localized
                    popVC.delegate = self
                    self?.tabBarController?.present(popVC, animated: true)
                }
                self?.getOfferNewObject(showLoader: true)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "fav",id:offerID, goTo: false)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    func callUnFavouriteAPI(offerID: Int?)  {
        self.view.endEditing(true)
        
        let params = [String:AnyObject]()
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.unFavourite)?user_id=\(user?.user_id ?? 0)&offer_id=\(offerID ?? 0)", params: params,shouldAddLoader: true,isDeleteRequest: true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let _ = jsonResponse {
                Utilities.showALertWithTag(title: Key.Messages.FavouriteTitle.localized, message: Key.Messages.OfferRemovedFavourites.localized)
                self?.getOfferNewObject(showLoader: true)
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            if let detail = errorResponse?[keyMessage] as? String {
                if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                    if let code = errorResponse?[keyCode] as? Int {
                        if code == 401 || code == 407 || code == 409 {
                            self.logOut()
                        } else if code == 403 {
                            let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                            if (guestCheck ?? false) {
                                self.logOut()
                            } else {
                                self.getRefreshedToken(from: "unfav", id: offerID, goTo: false)
                            }
                        }
                    } else {
                        self.logOut()
                    }
                } else {
                    self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                }
            } else if let msg = errorResponse?[keyMessage] as? String {
                self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
            }
        }
    }
    
    @IBAction func favButtonTap(_ sender: Any) {
        var offerID : Int?
        offerID = offerObject?.offer_id
        if offerObject?.favourite ?? false {
            callUnFavouriteAPI(offerID: offerID)
        } else {
            callFavouriteAPI(offerID: offerID)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.logEvent("offerDetail", parameters: [
            "offer_name": offerObject?.offer_title ?? dealOfTheDayObject?.offer_title ?? ""
        ])
    }
    
    @IBAction func shareBtn(_ sender: Any) {
        
        guard let offerID = self.offerObject?.offer_id else {
            return
        }
        var components = URLComponents()
        components.scheme = "https"
        components.host = "offaraty.page.link"
        var queryItem = URLQueryItem(name: "", value: "\(offerID)")
        components.path = "/mobile"
        queryItem.name = "offerID"
        queryItem.value = "\(offerID)"
        components.queryItems = [queryItem]
        guard let linkParameter = components.url else {
            print("Could not create shareable dynamic link")
            return
        }
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://offaraty.page.link") else {
            print("Could not create shareable dynamic link")
            return
        }
        
        if let myBundleID = Bundle.main.bundleIdentifier
        {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleID)
        }
        
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.riyadhbank")
        shareLink.iOSParameters?.appStoreID = "1438542580"
        
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        shareLink.socialMetaTagParameters?.title = self.offerObject?.offer_title ?? ""
        shareLink.socialMetaTagParameters?.descriptionText = self.offerObject?.offer_detail
        let urlImg = URL(string: self.offerObject?.offer_image ?? "")
        shareLink.socialMetaTagParameters?.imageURL = urlImg
        
        if let longUrl = shareLink.url  {
            print("The longUrl = \(longUrl.absoluteString)")
        }
        else
        {
            return
        }
        
        shareLink.shorten(completion:  { [weak self](url,warnings,error) in
            
            if let error = error
            {
                print("Error  = \(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings
            {
                for warning in warnings
                {
                    print("Warning = \(warning)")
                }
            }
            guard let url = url else {
                return
            }
            print("short url to share = \(url.absoluteString)")
            UIPasteboard.general.string = url.absoluteString
            self?.navigationController?.view.makeToast("Offer link copied to clipboard.".localized, duration: 2.0, position: .bottom)
        })
    }
    
    @IBAction func mapListOptionButtonTap(_ sender: Any) {
        DispatchQueue.main.async {
            if self.mapCheck {
                self.mapLabel.text = "Map View".localized
                self.mapImageView.image = mapIcon
                self.mapView.alpha = 0
                DispatchQueue.main.async {
                    self.locationReviewsTableHeight.constant = self.locationReviewsTableView.contentSize.height
                }
            } else {
                self.mapLabel.text = "List View".localized
                self.mapImageView.image = listIcon
                DispatchQueue.main.async {
                    self.locationReviewsTableHeight.constant = 306
                }
                self.mapView.alpha = 1
                self.mapView.clear()
                self.showLocations()
            }
            self.mapCheck = !self.mapCheck
            self.animateChanges()
            self.viewWillLayoutSubviews()
        }
    }
    
    func showLocations() {
        mapView.delegate = self
        for store in offerObject?.stores ?? []{
            let location = CLLocationCoordinate2D(latitude: Double(store.latitude ?? "") ?? 0.0, longitude: Double(store.longitude ?? "") ?? 0.0)
            print("location: \(location)")
            let marker = GMSMarker()
            marker.position = location
            marker.appearAnimation = .pop;
            marker.title = store.business_name
            marker.snippet = store.address
            marker.map = mapView
            marker.userData = store as AnyObject
        }
        if offerObject?.stores?.indices.contains(0) ?? false {
            let camera = GMSCameraPosition.camera(withLatitude: Double(offerObject?.stores?[0].latitude ?? "") ?? 0.0, longitude: Double(offerObject?.stores?[0].longitude ?? "") ?? 0.0, zoom: 10.0)
            self.mapView.camera = camera
        }
    }
}
extension DealDetailViewcontroller: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (currentIndex == 2 && (self.offerObject?.purchase_type != "online")) || ((Utilities.getIsArabic() ?? false) && currentIndex == 1) {
            return self.offerObject?.stores?.count ?? 0
        } else {
            return self.offerObject?.reviews?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (currentIndex == 2 && (self.offerObject?.purchase_type != "online")) || ((Utilities.getIsArabic() ?? false) && currentIndex == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableCell", for: indexPath) as! LocationTableCell
            
            cell.selectionStyle = .none
            cell.delegate = self
            cell.loadData(isArabicCheck: self.isArabic, store:self.offerObject?.stores?[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableCell", for: indexPath) as! ReviewTableCell
            
            cell.selectionStyle = .none
            cell.loadData(isArabicCheck: self.isArabic, obj: self.offerObject?.reviews?[indexPath.row])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (currentIndex == 2 && (self.offerObject?.purchase_type != "online")) || ((Utilities.getIsArabic() ?? false) && currentIndex == 1) {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationPopUpController") as? LocationPopUpController {
                secondViewController.offerObject = offerObject
                secondViewController.storeObject = self.offerObject?.stores?[indexPath.row]
                secondViewController.modalPresentationStyle = .overCurrentContext
                self.tabBarController?.present(secondViewController, animated: true, completion: nil)
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.locationReviewsTableHeight.constant = self.locationReviewsTableView.contentSize.height
            self.animateChanges()
        }
    }
}
extension DealDetailViewcontroller:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.latitude = locValue.latitude
        self.longitude = locValue.longitude
        self.locationManager.stopUpdatingLocation()
        if !gotLocation {
            self.gotLocation = true
            self.getOfferNewObject(showLoader: false)
        }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
}
extension DealDetailViewcontroller: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if marker.userData != nil {
            guard let store = marker.userData as? Stores else {
                return false
            }
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationPopUpController") as? LocationPopUpController {
                secondViewController.offerObject = offerObject
                secondViewController.storeObject = store
                secondViewController.modalPresentationStyle = .overCurrentContext
                self.tabBarController?.present(secondViewController, animated: true, completion: nil)
            }
        }
        mapView.selectedMarker = marker
        
        return true
    }
}
extension DealDetailViewcontroller: LocationTableCellDelegate {
    func callStore(obj: Stores?) {
        contactSupportThroughNumber(number: obj?.mobile ?? "")
    }
    func contactSupportThroughNumber(number : String) {
        
        if let url = URL(string: "tel://\(number.whiteSpacesRemoved())"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            self.navigationController?.view.makeToast("Call functionality not available.".localized, duration: 2.0, position: .bottom)
            // add error message here
        }
    }
}
extension DealDetailViewcontroller: AlertPopUpVCDelegate {
    func buttonTapped(type: ButtonTappedType, additional: String) {
        if type == .ok && additional == "" {
            self.callSaveApi(goToRedeem: true)
        } else if type == .ok {
            self.tabBarController?.selectedIndex = 2
        } else {
            self.callSaveApi(goToRedeem: false)
        }
    }
}
extension DealDetailViewcontroller: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
    }
}
