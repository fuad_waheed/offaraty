//
//  RedeemSuccessPopUpVC.swift
//  Offeraty
//
//  Created by Fuad Waheed on 20/09/2021.
//

import UIKit
enum GoToScreen {
    case review
    case back
}

protocol RedeemSuccessPopUpVCDelegate: AnyObject {
    func goTo(screen: GoToScreen)
}

class RedeemSuccessPopUpVC: BaseViewController {
    
    //MARK: UIImageView Outlets
    @IBOutlet weak var dealImageView: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var productLabel: UILabel!
    
    weak var delegate: RedeemSuccessPopUpVCDelegate?
    
    var offerObject : Offers?
    var dealOfTheDayObject : DealOfTheDay?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.Redeem)
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
    }
    
    func loadData() {
        dealImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        dealImageView.sd_setImage(with: URL(string: offerObject?.offer_image ?? dealOfTheDayObject?.offer_image ?? ""),placeholderImage:placeIm)
        productLabel.text = offerObject?.offer_title ?? dealOfTheDayObject?.offer_title
    }

    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.goTo(screen: .back)
        })
    }
    
    @IBAction func reviewBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.goTo(screen: .review)
        })
    }
    
}
