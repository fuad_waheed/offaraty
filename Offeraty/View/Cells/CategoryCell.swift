//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit

class CategoryCell: UICollectionViewCell {

    //MARK: UIImage Outlets
    @IBOutlet weak var categoryImage: UIImageView!
    //MARK: UILabel Outlets
    @IBOutlet weak var categoryTitle: UILabel!
    //MARK: UIView Outlets
    @IBOutlet weak var lockView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCategoryText(category:String,index:Int) {
        self.categoryTitle.text = category
        categoryImage.image = UIImage(named: "mainCat\(index)")
        if index == 0 {
            lockView.alpha = 0
        } else {
            lockView.alpha = 1
        }
    }

}
