//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit

class AllOfferCell: UICollectionViewCell {

    //MARK: UIImageView Outlets
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var starImage: UIImageView!
    //MARK: UILabel Outlets
    @IBOutlet weak var offerTitle: UILabel!
    @IBOutlet weak var offerDesc: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    //MARK: UIView Outlets
    @IBOutlet weak var grayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        grayView.clipsToBounds = true
        grayView.layer.cornerRadius = 5.0
        offerImage.layer.cornerRadius = 5.0
        grayView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    func loadOffer(offer: Offers?) {
        offerImage.contentMode = .scaleAspectFill
        offerImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        offerImage.sd_setImage(with: URL(string: offer?.offer_image ?? ""),placeholderImage:placeIm)
        offerTitle.text = offer?.offer_title
        offerDesc.text = offer?.offer_detail
        if offer?.total_rating == nil {
            ratingLabel.alpha = 0
            starImage.alpha = 0
        } else {
            ratingLabel.alpha = 1
            starImage.alpha = 1
            ratingLabel.text = String(format: "%.1f", Double(offer?.total_rating ?? 0))
        }
    }

}
