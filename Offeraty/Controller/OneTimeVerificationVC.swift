//
//  ForgotViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 03/06/2021.
//

import UIKit
import OTPFieldView
import GradientCircularProgress

protocol OneTimeVerificationVCDelegate: AnyObject {
    func goToHome()
}

class OneTimeVerificationVC: BaseViewController {
    
    //MARK: Images Outlets
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    //MARK: UIButton Outlets
    @IBOutlet weak var resendBtn: UIButton!
    
    //MARK: OTPField Outlets
    @IBOutlet weak var otpField: OTPFieldView!
    
    weak var delegate: OneTimeVerificationVCDelegate?
    
    @IBOutlet weak var progressViewParent: UIView!
    let progress = GradientCircularProgress()
    var progressView: UIView?
    
    var otpText = ""
    var email = ""
    var timerValue = 0.0
    
    var timer: Timer?
    var totalTime = 180
    
    var bgTask = UIBackgroundTaskIdentifier(rawValue: 0)
    
    var params = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        progressViewParent.backgroundColor = .clear
        progressView = progress.showAtRatio(frame: progressViewParent.frame, display: true, style: TimerStyle())
        progressViewParent.addSubview(progressView!)
        progressView?.center = progressViewParent.convert(progressViewParent.center, from:progressViewParent.superview)
        self.progress.updateMessage(message: "")
        setUpView()
        checkLangauge()
        hideResendBtn()
        startOtpTimer()
    }
    
    func hideResendBtn() {
        resendBtn.alpha = 0.5
        resendBtn.isUserInteractionEnabled = false
    }
    
    func showResendBtn() {
        resendBtn.alpha = 1
        resendBtn.isUserInteractionEnabled = true
    }
    
    private func startOtpTimer() {
        stopOTPTimer()
        self.progress.updateRatio(CGFloat(self.timerValue))
        bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.bgTask)
        })
        self.totalTime = 180
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        if timer != nil {
            RunLoop.current.add(timer!, forMode: .default)
        }
    }
    @objc func updateTimer() {
        print(self.totalTime)
        timerValue = timerValue + 0.005555555555556
        progress.updateRatio(CGFloat(timerValue))
        self.countDownLabel.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            showResendBtn()
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    private func stopOTPTimer() {
        timer?.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.SignupOTPController)
        UIView.animate(withDuration: 0.4, delay: 0.0, options:[], animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion:nil)
    }
    
    func setUpView() {
        self.otpField.fieldsCount = 6
        self.otpField.fieldBorderWidth = 1
        self.otpField.defaultBorderColor = UIColor(red: 0.74, green: 0.75, blue: 0.76, alpha: 1.00)
        self.otpField.filledBorderColor = AppColor.appGreenColor
        self.otpField.cursorColor = AppColor.appGreenColor
        self.otpField.displayType = .roundedCorner
        self.otpField.cornerRadius = 6
        self.otpField.fieldSize = 36
        self.otpField.separatorSpace = 15
        self.otpField.fieldFont = UIFont(name: "Effra-Regular", size: 20.0)!
        self.otpField.shouldAllowIntermediateEditing = false
        self.otpField.delegate = self
        self.otpField.initializeUI()
    }
    
    func checkLangauge() {
        let text = "We have sent you the access code via email".localized
        self.subHeadingLabel.text = "\(text) \(email)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = .clear
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if(self.verifyAllFields() == false){
            return
        }
        callSignUpAPI()
    }
    func verifyAllFields() -> Bool{
        
        if !(Utilities.isConnected())
        {
            Utilities.showAlert(title: "", message: Key.Messages.NoInternet.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            return false
        } else if self.otpText == "" || self.otpText.count < 5 {
            Utilities.showAlert(title: "", message: Key.Messages.WrongOTP.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
            return false
        }
        return true
    }
    func resendOTP()  {
        self.view.endEditing(true)
        let email = params["email"] as? String
        var lang = "english"
        if (Utilities.getIsArabic() ?? false) {
            lang = "arabic"
        }
        OffaratyAPIManager.request(withAPIName: "\(APIConstants.signup_user)/otp/\(lang)?email=\(email ?? "")&token=\(params["token"] as? String ?? "")", params: [:],shouldAddLoader: true,isGetRequest: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                self?.hideResendBtn()
                self?.startOtpTimer()
                if let dataString = json[keyOfData] as? String {
                    Utilities.showAlert(title: "", message: "Otp sent via email".localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Success)
                    self?.params["encrypted_string"] = dataString as AnyObject
                }
                else {
                    Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    func callSignUpAPI()  {
        self.view.endEditing(true)
        
        params["otp"]  = self.otpText as AnyObject
        OffaratyAPIManager.request(withAPIName: APIConstants.signup_user, params: params,shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
            OfferatySingleton.shared.hideHud()
            if let json = jsonResponse {
                let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                do {
                    let userDataDic = try JSONDecoder().decode(User.self, from: userData)
                    Cache.saveUserObject(userDataDic, forKey: keyCurrentUser)
                    Cache.save(userDataDic.access_token ?? "", forKey: keyAccessToken)
                    Utilities.setIsUserLoggedIn(isUserLoggedIn: true)
                    self?.stopOTPTimer()
                    FirebaseUtility.screenEvent(.SignupViewController)
                    self?.dismiss(animated: true, completion: {
                        self?.delegate?.goToHome()
                    })
                }
                
                catch {
                    print("could not decode \(error)")
                    Utilities.showAlert(title: "", message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
                }
            }
            
        }) { (errorResponse) in
            print(errorResponse ?? [:])
            OfferatySingleton.shared.hideHud()
            Utilities.showAlert(title: "", message: errorResponse?[keyDetail] as? String ?? errorResponse?[keyMessage] as? String ?? "", doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        }
    }
    
    @IBAction func resendButtonTapped(_ sender: Any) {
        if !(Utilities.isConnected())
        {
            Utilities.showAlert(title: "", message: Key.Messages.NoInternet.localized, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        } else {
            resendOTP()
        }
    }
}
extension OneTimeVerificationVC: OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String)
    {
        otpText = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return false
    }
}
