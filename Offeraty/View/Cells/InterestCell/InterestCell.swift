//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit

class InterestCell: UICollectionViewCell {

    //MARK: UIImage Outlets
    @IBOutlet weak var interestImage: UIImageView!
    @IBOutlet weak var selectedImage: UIImageView!
    //MARK: UILabel Outlets
    @IBOutlet weak var interestTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadData(categoryObject:Categories?,categoriesIndexArr:[Int],index:Int) {
        self.interestTitle.text = categoryObject?.name
        if categoriesIndexArr.contains(where: {$0 == index}) {
            selectedImage.visibility = .visible
        } else {
            selectedImage.visibility = .invisible
        }
        interestImage.makeRoundedView()
        interestImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        interestImage.sd_setImage(with: URL(string: categoryObject?.image ?? ""))
    }

    func loadCategory(categoryObject:Categories?) {
        self.interestTitle.text = categoryObject?.name
        interestImage.makeRoundedView()
        interestImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        interestImage.sd_setImage(with: URL(string: categoryObject?.image ?? ""))
    }
    
    func loadCategoryText(category:String,index:Int) {
        self.interestTitle.text = category
        interestImage.image = UIImage(named: "mainCat\(index)")
    }
    
    func loadDeal(dealObject:DealOfTheDay?) {
        self.interestTitle.text = dealObject?.offer_title
        interestImage.contentMode = .scaleAspectFit
        interestImage.makeRoundedView()
        interestImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        interestImage.sd_setImage(with: URL(string: dealObject?.dod_image ?? dealObject?.offer_image ?? ""),placeholderImage:placeIm)
    }

}
