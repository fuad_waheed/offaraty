//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit

class OrderHistoryCell: UITableViewCell {

    //MARK: UIImage Outlets
    @IBOutlet weak var productImageView: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var boughtOnLabel: UILabel!
    
    //MARK: UIView Outlets
    @IBOutlet weak var imageOuterView: UIView!
    @IBOutlet weak var ratingView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageOuterView.clipsToBounds = true
        imageOuterView.layer.cornerRadius = 5.0
        imageOuterView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    @IBAction func writeReviewButtonTap(_ sender: Any) {
    }
    
}
