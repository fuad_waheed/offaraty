//
//  APIConstants.swift
//  AirMiles
//
//  Created by Waqar Khalid on 10/10/19.
//  Copyright © 2019 Merit Incentive. All rights reserved.
//

import Foundation
import UIKit
struct APIConstants {


// SIT version pattern = 2.0.X
// UAT version pattern = 2.X.0
//    static let stagingBaseURL = "https://lmc-airmiles-am-uat-r17.airmiles.aimiadev.com/airmileslmc/" // UAT
//    static let UATtravelShopBaseURL = "https://site-uat-r17.cms-airmiles.aimiadev.com/mobilessorequest?operation=mobileSSOAjaxRequest&goto=https://travel-uat.airmilesme.com/" // UAT
    static let liveBaseURLForUserManagement = "http://3.108.98.53:8015/user_management/apis/"
    static let liveBaseURLForOffers = "http://3.108.98.53:8010/offers/mobile_apps/apis/offer/"
    static let stagingBaseURLForUserManagement = "http://65.0.143.67:8015/user_management/apis/" // SIT
    static let stagingBaseURLForOffers = "http://65.0.143.67:8010/offers/mobile_apps/apis/offer/" // SIT
    
//    static var currentURLForUserManagement = liveBaseURLForUserManagement
//    static var currentURLForOffers = liveBaseURLForOffers


                        
    static let productionTravelShopBaseURL = "https://www.airmilesme.com/mobilessorequest?operation=mobileSSOAjaxRequest&goto=https://travel.airmilesme.com/"
    fileprivate static let chatStagingIntegrationId = "5fb39a125d483f000c2cb3f0"
    fileprivate static let chatLiveIntegrationId = "5fc4feb2a0458d000de360e9"
    
    static let currentURLForUserManagement:String? = {
        if AppConstants.isDemoMode == true {
            return APIConstants.stagingBaseURLForUserManagement
        }
        else{
            return APIConstants.liveBaseURLForUserManagement
        }
    }()
    
    static let currentURLForOffers:String? = {
        if AppConstants.isDemoMode == true {
            return APIConstants.stagingBaseURLForOffers
        }
        else{
            return APIConstants.liveBaseURLForOffers
        }
    }()

  


   
    // MARK: - EndPoints
    static let login = "login_for_access_token"
    static let forgotPassword = "forgotPassword"
    static let getBalance = "getBalanceWithTierInfo"
    static let register = "signup_user"
    static let profile = "profile"
    static let refreshToken = "refresh"
    static let categoryList = "category_list"
    static let offerList = "offer_list"
    static let getOffer    = "get_offer_detail"
    static let offers       = "offers"
    static let homeItems = "home_screen"
    static let allOffers = "home_screen/all_offers"
    static let collectItemDetails = "catalogue-items"
    static let redeemItems = "catalogue-reward-items"
    static let redeemItemDetails = "catalogue-items"
    static let redeem           = "redeem"
    static let favourites_list = "favourites_list"
    static let my_orders = "my_orders"
    static let signup_user = "signup_user"
    static let search = "search"
    static let reviewOptions = "review_option_list"
    static let changePassword = "change_password"
    static let forgotToken = "get_token"
    static let favourite = "favourite"
    static let guestLogin = "guest_login"
    static let userInterests = "user_interests"
    static let updaetUserInterests = "user_interests/update"
    static let unFavourite = "discard_favourite"
    static let rateReview = "rate_review_offer"
    static let myvouchers = "myvouchers"
    static let filters = "filters_for_offers"
    static let getUser = "get_user"
    static let getAbout = "app_content_view"
    static let getSupport = "support_details"
    static let updateOutlet = "update/outlet"
    static let updateProfile = "update_user"
    static let PurchaseOffer = "purchase_offer"
    static let Spend = "spend"
}
public struct AppConstants {
    static var isDemoMode = true
    static let AppDefaultColor = UIColor(named: "PrimaryLightBlue")
    static let AppPinkColor    = UIColor(named: "SecondaryPink")
    static let uaeContactEmail = "contactuae@airmilesme.com"
    static let qatarContactEmail = "contactqatar@airmilesme.com"
    static let bahrainContactEmail = "contactbahrain@airmilesme.com"
    static let uaeContactNumber = "0097143913400"
    static let qatarContactNumber = "0097444449210"
    static let bahrainContactNumber = "0097317583888"
    static let applicationAppStoreURL = "https://apps.apple.com/us/app/air-miles-me/id1098163713"
    
    static let googleMapsAPIKey: String? = {
        if AppConstants.isDemoMode == true {
            return "AIzaSyAMpLg0fMA33_Cxi8lQhD4V_ZjqWQiR77k"
        }
        else{
            return "AIzaSyAMpLg0fMA33_Cxi8lQhD4V_ZjqWQiR77k"
        }
    }()
    
}

//var navigationBar = CustomNavigationBar()
typealias typeAliasDictionary = [String:AnyObject]
typealias typeAliasDictionariesArray = [[String:AnyObject]]
let defaults = UserDefaults.standard
// MARK: - Constants
let kIsIntroSkipped = "kIsIntroSkipped"
let kIsUserLoggedIn = "kIsUserLoggedIn"
let kIsGuestLoggedIn = "kIsGuestLoggedIn"
let keyCurrentUser = "currentUser"
let keyRemeberCheck = "rememberCheck"
let keyUserInterestsCheck = "interestsCheck"
let keySelectedCategories = "selectedCategories"
let keyIsArabic = "isArabic"
let keyLanguageDiection = "selectedLanguage"
let keyOfData = "data"
let keyTotalPages = "total_pages"
let keyAccessToken = "access_token"
let keyMessage = "message"
let keyDetail = "detail"
let keyCode = "code"
let keyDynamicLink = "dynamicLink"
let kUserEmail =    "kUserEmail"
let kUserPassword = "kUserPassword"
let kClientToken = "kClientToken"
let kRefreshToken = "kRefreshToken"
let kIsTouchIDEnabled = "kIsTouchIDEnabled"
let kIsGridLayout     = "collectPartnerLink"
let kUserData = "kUserData"
let kUUID = "kUUID"
let kVariant = "ldpi"
let kUserFBID = "kUserFBID"
let kUserFBToken = "kUserFBToken"
let kLastSignedInUserEmail = "kLastSignedInUserEmail"
let kLogTime = "time"
let kLogScreenName = "screenName"
let kLogMemberId = "member_id"
let kMaintainanceErrorCode = "9999"
let kGridTyleHeight = 245
// MARK: - Error Constants
let kE0001 = "Your email address / password does not match please check and try again."
let kE0002 = "Sorry there is a login issue, please try again in 15 minutes."
let kE0003 = "Unable to login? Please tap Help for assistance."
let kE0004 = "Invalid email address or password. You have one more attempt before your account is locked for 15 minutes."
let kE0005 = "You have exceeded the maximum login attempts. Please try after 15 minutes."
let kE0007 = "The Facebook account you entered is not linked to an Air Miles account. ​To link, login using email and password,  navigate to Settings."
let kE0008 = "Facebook credentials invalid"
let kE0009 = "Please check the email sent to your registered address for further instructions to reset your password."
let kE0010 = "Please enter a valid email address"
let kE0011 = "Please enter a valid email address"
let kE0012 = "Please enter a valid email address"
let kE0013 = "Password entered does not meet the password requirements, please try again."
let kE0014 = "Name must contain letters only."
let kE0015 = "Mobile number incorrect, please tap the information icon for correct format."
let kE0016 = "To join you must agree to Air Miles T&Cs and Privacy Policy. Please tick check box to proceed."
let kE0017 = "Not all mandatory information has been supplied, please review the form."
let kE0018 = "Thank you for opening an Air Miles account. A verification email has been sent to your registered email address."
let kE0019 = "The email address you have entered is linked to an existing account, please enter a new email address."



let kE2001 = "Unable to verify account details for redemption. Before contacting Air Miles please check your profile is complete. For assistance please tap Help."
let kE2002 = "You do not have enough Air Miles balance for the selected quantity. Please ensure you have sufficient Air Miles balance or remove the items and try again."
let kE2003 = "Please enter quantity to continue with your order."
let kE2007 = "There was a problem with some of the items in your order. Please tap Help for assistance."
let kE2008 = "Phone call not supported on this device"
let kE2009 = "You do not have enough Air Miles balance for the selected quantity. Please ensure you have sufficient Air Miles balance or remove the items and try again."
let kE2010 = "Currently we are unable to process your order. Before contacting Air Miles please check your profile is complete. For assistance please tap Help."
let kE2011 = "Currently we are unable to process your order, please tap Help for assistance."
let kE2012 = "You need a minimum of <threshold> Air Miles to proceed with this redemption. Please try again or tap Help for assistance."
let kE2013 = "Unable to verify account details for redemption. Before contacting Air Miles please check your profile is complete. For assistance please tap Help."
let kE2014 = "Redemption is temporarily suspended, please try again later."
let kE2015 = "Currently there are no rewards available in this category"
let kE2016 = "Currently there are no collect partners available in this category"

let kE3001 = "Sorry we didn’t find any results, please change your search criteria and try again."
let kE3002 = "Please enter Partners or Rewards to search"
let kE3003 = "The PIN you entered is not valid, please try again."
let kE3004 = "The voucher selected has been used, please select a different one."
let kE3005 = "Please enter old password and new password."
let kE3006 = "The old password you entered is not valid, please try again."
let kE3007 = "The new password entered does not meet the password requirements. Please try again following the password rules."
let kE3008 = "Old password and new password cannot be the same."
let kE3009 = "Your password has been updated"
let kE3010 = "Currently we are unable to update your password, please try again in 15 minutes."
let kE3011 = "Your profile has been successfully updated."
let kE3012 = "Currently we are unable to update your profile, please try again in 15 minutes."
let kE3013 = "The voucher selected cannot be used. Please tap Help for assistance."
let kEFBPERMISSIONERROR = "Authenticaion error"
let kMaintanceModeERROR = "9999"
let kUpdateRequiredMessage = "You must upgrade to the latest version to continue using the Air Miles App"
let kInternetErrorMessage = "Please connect to internet"
let KNoTransactionDetailError = "Details are not available for this transaction. Please tap Help for assistance"
