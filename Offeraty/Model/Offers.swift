//
//  User.swift
//
//  Created by Fuad Waheed on 14/12/2018.
//  Copyright © 2018 Fuad Waheed. All rights reserved.


class Offers: Codable {
    var offer_id                : Int?
    var offer_title             : String?
    var can_review              : Bool?
    var offer_detail            : String?
    var brand_description       : String?
    var terms_condition         : String?
    var discount                : String?
    var refresh_date            : String?
    var start_date              : String?
    var end_date                : String?
    var offer_image             : String?
    var sponsor_image           : String?
    var isactive                : Int?
    var islocked                : Int?
    var favourite               : Bool?
    var createdon               : String?
    var merchant                : Merchant?
    var order_detail            : OfferDetail?
    var category                : Categories?
    var subcategory             : SubCategory?
    var stores                  : [Stores]?
    var total_rating            : Double?
    var reviews                 : [Reviews]?
    var deal_of_the_day         : [DealOfTheDay]?
    var limited_time_offer      : [DealOfTheDay]?
    var recomended_offers       : [Offers]?
    var estimated_savings       : Double?
    var purchase_type           : String?
    var offers                  : [Offers]?
    var banner_image           : String?
}
