//
//  AboutViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 07/06/2021.
//

import UIKit
import HMSegmentedControl
import MessageUI

class AboutViewController: BaseViewController {
    
    //MARK: UITextView Outlets
    @IBOutlet weak var aboutOffaratyTextView: UITextView!
    
    //MARK: UIView Outlets
    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var profilePic: UIImageView!
    
    //MARK: UILabel Outlets
    @IBOutlet weak var whatsAppNumberLabel: UILabel!
    @IBOutlet weak var callNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    
    //MARK: UISegmentControl Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var optionsSegmentView: HMSegmentedControl!
    
    var aboutObj: About?
    var supportObj: Support?

    var user = Cache.extractUserObject(keyCurrentUser)
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSegmentControl()
        getAboutData()
        getSupportData()
        // Do any additional setup after loading the view.
    }
    
    func setAboutData() {
        aboutOffaratyTextView.text = self.aboutObj?.content_value
    }
    
    func setSupportData() {
        whatsAppNumberLabel.text = self.supportObj?.whatsapp_no
        callNumberLabel.text = self.supportObj?.contact_no
        emailLabel.text = self.supportObj?.email
        addressLabel.text = self.supportObj?.address
    }
    
    func getSupportData()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
//            var lang = "english"
//            if (Utilities.getIsArabic() ?? false) {
//                lang = "arabic"
//            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.getSupport, params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let obj = try JSONDecoder().decode(Support.self, from: userData)
                        self?.supportObj = obj
                        self?.setSupportData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "support")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getAboutData()  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.getAbout)/\(lang)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        let objArr = try JSONDecoder().decode([About].self, from: userData)
                        let obj = objArr.first(where: {$0.content_key == "about_offaraty"})
                        self?.aboutObj = obj
                        self?.setAboutData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                        Utilities.showAlert(title: Key.Messages.AlertTitle.localized, message: Key.Messages.SomethingWentWrongError.localized, doneButtonTitle: Key.Messages.OKTitle.localized, alertType: .Failure)
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(from: "about")
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    
    func getRefreshedToken(from: String)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            if from == "about" {
                                self?.getAboutData()
                            } else {
                                self?.getSupportData()
                            }
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
    
    func setUpSegmentControl() {
        if (Utilities.getIsArabic() ?? false) {
            optionsSegmentView.sectionTitles = ["About Offaraty".localized, "Support".localized].reversed()
            optionsSegmentView.setSelectedSegmentIndex(UInt(1), animated: false)
        } else {
            optionsSegmentView.sectionTitles = ["About Offaraty".localized, "Support".localized]
        }
        optionsSegmentView.autoresizingMask = [.flexibleRightMargin, .flexibleWidth]
        optionsSegmentView.selectionStyle = .textWidthStripe
        optionsSegmentView.selectionIndicatorLocation = .bottom
        optionsSegmentView.selectionIndicatorColor = AppColor.appPrimaryPurpleColor
        optionsSegmentView.selectionIndicatorHeight = 2
        optionsSegmentView.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "#747C91") ?? .darkGray,
            NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 17.0)!
        ]
        optionsSegmentView.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.appNewGreen,
            NSAttributedString.Key.font: UIFont(name: "Effra-Medium", size: 17.0)!
        ]
        optionsSegmentView.backgroundColor = .clear
        optionsSegmentView.segmentWidthStyle = .fixed
        optionsSegmentView.type = .text
        optionsSegmentView.addTarget(self, action: #selector(segmentedControlChangedValue(segmentedControl:)), for: .valueChanged)
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.backButton.setImage(backRight, for: .normal)
            self.aboutOffaratyTextView.textAlignment = .right
        } else {
            self.backButton.setImage(backLeft, for: .normal)
            self.aboutOffaratyTextView.textAlignment = .left
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        mainView.roundCorners(corners: [.topLeft,.topRight], radius: 20.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.AboutOffaraty)
    }
    
    @objc func segmentedControlChangedValue(segmentedControl: HMSegmentedControl) {
        DispatchQueue.main.async { [weak self] in
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                
                FirebaseUtility.screenEvent(.AboutOffaraty)
                if (Utilities.getIsArabic() ?? false) {
                    self?.topImageView.image = UIImage(named: "supportImage")
                    self?.supportView.fadeIn()
                    self?.aboutOffaratyTextView.fadeOut()
                } else {
                    self?.topImageView.image = UIImage(named: "aboutImage")
                    self?.supportView.fadeOut()
                    self?.aboutOffaratyTextView.fadeIn()
                }
                break
            case 1:
                
                FirebaseUtility.screenEvent(.SupportViewController)
                if (Utilities.getIsArabic() ?? false) {
                    self?.topImageView.image = UIImage(named: "aboutImage")
                    self?.supportView.fadeOut()
                    self?.aboutOffaratyTextView.fadeIn()
                } else {
                    self?.topImageView.image = UIImage(named: "supportImage")
                    self?.supportView.fadeIn()
                    self?.aboutOffaratyTextView.fadeOut()
                }
                
                break
            default:break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        user = Cache.extractUserObject(keyCurrentUser)
        profilePic.makeRoundedView()
        profilePic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        profilePic.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name ?? "Guest".localized
        checkLangauge()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar()
    }
    
    @IBAction func whatsAppCallButton(_ sender: Any) {
        guard let whatsApp = self.supportObj?.whatsapp_no else {
            self.navigationController?.view.makeToast("Support not available.".localized, duration: 2.0, position: .bottom)
            return
        }
        contactSupportThroughWhatsapp(number: whatsApp)
    }
    
    func contactSupportThroughWhatsapp(number: String){
        let urlWhats = "whatsapp://send?phone=\(number)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    self.navigationController?.view.makeToast("Whatsapp not available.".localized, duration: 2.0, position: .bottom)
                    if let url = URL(string: "itms-apps://apps.apple.com/us/app/whatsapp-messenger/id310633997") {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url)
                        }
                    }
                }
            }
        }
    }
    
    func contactSupportThroughNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            self.navigationController?.view.makeToast("Call functionality not available.".localized, duration: 2.0, position: .bottom)
            // add error message here
        }
    }
    
    @IBAction func phoneCallButton(_ sender: Any) {
        guard let mobile = self.supportObj?.contact_no else {
            self.navigationController?.view.makeToast("Support not available.".localized, duration: 2.0, position: .bottom)
            return
        }
        contactSupportThroughNumber(number: mobile)
    }
    
    @IBAction func emailButton(_ sender: Any) {
        guard let mail = self.supportObj?.email else {
            self.navigationController?.view.makeToast("Support not available.".localized, duration: 2.0, position: .bottom)
            return
        }
        sendEmail(email: mail)
    }
    func sendEmail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("Contact Support", isHTML: true)

            present(mail, animated: true)
        } else {
            // show failure alert
            self.navigationController?.view.makeToast("Cannot open the email app".localized, duration: 2.0, position: .bottom)
        }
    }
}
extension AboutViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
