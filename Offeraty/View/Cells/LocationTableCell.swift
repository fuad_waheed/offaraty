//
//  LocationTableCell.swift
//  AirMiles
//
//  Created by Waqar Khalid on 4/3/20.
//  Copyright © 2020 Merit Incentive. All rights reserved.
//

import UIKit
protocol LocationTableCellDelegate: AnyObject {
    func callStore(obj: Stores?)
}
class LocationTableCell: UITableViewCell {
    
    //MARK: UILabel Outlets
    @IBOutlet weak var partnerAddressLabel: UILabel!
    @IBOutlet weak var partnerPhoneButton: UIButton!
    @IBOutlet weak var partnerDistanceLabel: UILabel!
    
    //MARK: UIIImageView Outlets
    @IBOutlet weak var partnerThumbnailImage: UIImageView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var storeObj: Stores?
    weak var delegate: LocationTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(isArabicCheck:Bool, store:Stores?) {
        storeObj = store
        if isArabicCheck {
            arrowImageView.image = smallLeft
        } else {
            arrowImageView.image = smallRight
        }
        let distance = Int(store?.distance ?? 0.0)
        let km = "km".localized
        partnerDistanceLabel.text = "\(distance) \(km)"
        if store?.mobile == nil || store?.mobile == "" {
            partnerPhoneButton.alpha = 0
        }
        partnerAddressLabel.text = store?.address
    }
    
    @IBAction func phoneTapped(_ sender: Any) {
        delegate?.callStore(obj: self.storeObj)
    }
    
}
