//
//  PopUpViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 15/04/2021.
//
enum MyVoucherType {
    case available
    case used
    case expired
}

import UIKit
import HMSegmentedControl

class MyVouchersViewController: BaseViewController {
    
    //MARK: UIImage Outlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: UITableView Outlet
    @IBOutlet weak var myVouchersTableView: UITableView!
    
    //MARK: UISegmentControl Outlets
    @IBOutlet weak var optionsSegmentView: HMSegmentedControl!
    
    var language = ""
    var user = Cache.extractUserObject(keyCurrentUser)
    var voucherObject : MyVouchers?
    var type: MyVoucherType = .available
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSegmentControl()
        self.myVouchersTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (Utilities.getIsArabic() ?? false) {
            self.language = "ar"
        } else {
            self.language = "en"
        }
        getVouchers()
        user = Cache.extractUserObject(keyCurrentUser)
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name
        checkLangauge()
    }
    
    func getVouchers() {
        switch type {
        case .available:
            getMyVouchers(available: true, used: false, expired: false)
        case .used:
            getMyVouchers(available: false, used: true, expired: false)
        case .expired:
            getMyVouchers(available: false, used: true, expired: false)
        }
    }
    
    func checkLangauge() {
        
    }
    
    func setUpSegmentControl() {
        if (Utilities.getIsArabic() ?? false) {
            optionsSegmentView.sectionTitles = ["Available".localized,"Used".localized,"Expired".localized].reversed()
            optionsSegmentView.setSelectedSegmentIndex(UInt(2), animated: false)
        } else {
            optionsSegmentView.sectionTitles = ["Available".localized,"Used".localized,"Expired".localized]
        }
        
        optionsSegmentView.autoresizingMask = [.flexibleRightMargin, .flexibleWidth]
        optionsSegmentView.selectionStyle = .textWidthStripe
        optionsSegmentView.selectionIndicatorLocation = .bottom
        optionsSegmentView.selectionIndicatorColor = AppColor.appPrimaryPurpleColor
        optionsSegmentView.selectionIndicatorHeight = 2
        optionsSegmentView.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "#747C91") ?? .darkGray,
            NSAttributedString.Key.font: UIFont(name: "Effra-Regular", size: 17.0)!
        ]
        optionsSegmentView.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: AppColor.appNewGreen,
            NSAttributedString.Key.font: UIFont(name: "Effra-Medium", size: 17.0)!
        ]
        optionsSegmentView.backgroundColor = .clear
        optionsSegmentView.segmentWidthStyle = .fixed
        optionsSegmentView.type = .text
        optionsSegmentView.addTarget(self, action: #selector(segmentedControlChangedValue(segmentedControl:)), for: .valueChanged)
    }
    
    @objc func segmentedControlChangedValue(segmentedControl: HMSegmentedControl) {
        DispatchQueue.main.async { [weak self] in
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                if self?.language == "ar" {
                    self?.type = .expired
                    self?.getMyVouchers(available: false, used: false, expired: true)
                } else {
                    self?.type = .available
                    self?.getMyVouchers(available: true, used: false, expired: false)
                }
                break
            case 1:
                self?.type = .used
                self?.getMyVouchers(available: false, used: true, expired: false)
                break
            case 2:
                if self?.language == "ar" {
                    self?.type = .available
                    self?.getMyVouchers(available: true, used: false, expired: false)
                } else {
                    self?.type = .expired
                    self?.getMyVouchers(available: false, used: false, expired: true)
                }
                break
            default:break
            }
            self?.myVouchersTableView.reloadData()
        }
    }
    
    func getMyVouchers(available: Bool, used: Bool, expired: Bool)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            var lang = "english"
            if (Utilities.getIsArabic() ?? false) {
                lang = "arabic"
            }
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: "\(APIConstants.my_orders)/\(lang)?available=\(available)&used=\(used)&expired=\(expired)&user_id=\(user?.user_id ?? 0)", params: params, shouldAddLoader: true,isGetRequest:true,urlType:.offers, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    let userData = try! JSONSerialization.data(withJSONObject: json[keyOfData] ?? [:],     options: .prettyPrinted)
                    do {
                        self?.voucherObject = try JSONDecoder().decode(MyVouchers.self, from: userData)
                        self?.myVouchersTableView.reloadData()
                    }
                    
                    catch {
                        print("could not decode \(error)")
                    }
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                if let detail = errorResponse?[keyMessage] as? String {
                    if detail.lowercased().contains("token") || detail.lowercased().contains("not authenticated") {
                        if let code = errorResponse?[keyCode] as? Int {
                            if code == 401 || code == 407 || code == 409 {
                                self.logOut()
                            } else if code == 403 {
                                let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
                                if (guestCheck ?? false) {
                                    self.logOut()
                                } else {
                                    self.getRefreshedToken(available: available, used: used, expired: expired)
                                }
                            }
                        } else {
                            self.logOut()
                        }
                    } else {
                        self.navigationController?.view.makeToast(detail, duration: 4.0, position: .bottom)
                    }
                } else if let msg = errorResponse?[keyMessage] as? String {
                    self.navigationController?.view.makeToast(msg, duration: 4.0, position: .bottom)
                }
            }
        }
    }
    func getRefreshedToken(available: Bool, used: Bool, expired: Bool)  {
        if !(Utilities.isConnected()) {
            Utilities.showALertWithTag(title: Key.Messages.AlertTitle.localized, message: Key.Messages.NoInternet.localized)
        } else {
            OfferatySingleton.shared.showHud()
            let params = [String:AnyObject]()
            OffaratyAPIManager.request(withAPIName: APIConstants.refreshToken, params: params, shouldAddLoader: true,urlType:.userManagement, imageToSend:nil, successBlock: {[weak self] (jsonResponse) in
                OfferatySingleton.shared.hideHud()
                if let json = jsonResponse {
                    if let dict = json[keyOfData] as? NSDictionary {
                        if let token = dict[keyAccessToken] as? String {
                            Cache.save(token, forKey: keyAccessToken)
                            self?.getMyVouchers(available: available, used: used, expired: expired)
                        } else {
                            self?.logOut()
                        }
                    } else {
                        self?.logOut()
                    }
                }  else {
                    self?.logOut()
                }
                
            }) { (errorResponse) in
                print(errorResponse ?? [:])
                OfferatySingleton.shared.hideHud()
                self.logOut()
            }
        }
    }
}
extension MyVouchersViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
        case .available:
            return self.voucherObject?.available?.count ?? 0
        case .used:
            return self.voucherObject?.used?.count ?? 0
        case .expired:
            return self.voucherObject?.expired?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyVouchersCell", for: indexPath) as! MyVouchersCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        switch type {
        case .available:
            cell.loadOffer(offer: self.voucherObject?.available?[indexPath.row], type: type, lang:self.language)
        case .used:
            cell.loadOffer(offer: self.voucherObject?.used?[indexPath.row], type: type, lang:self.language)
        case .expired:
            cell.loadOffer(offer: self.voucherObject?.expired?[indexPath.row], type: type, lang:self.language)
        }
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        FirebaseUtility.screenEvent(.MyVouchersViewController)
    }
}
extension MyVouchersViewController: MyVouchersCellDelegate {
    func writeReviewTapped(offer: Offers?) {
        let storyBoard = UIStoryboard(name: "App", bundle: nil)
        if let popVC = storyBoard.instantiateViewController(withIdentifier: "ReviewViewController") as? ReviewViewController {
            popVC.modalPresentationStyle = .overCurrentContext
            popVC.delegate = self
            popVC.offerObject = offer
            self.tabBarController?.present(popVC, animated: true)
        }
    }
    func mainButtonTapped(offer: Offers?) {
        if type == .available {
            if let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EnterPinOfferViewController") as? EnterPinOfferViewController {
                secondViewController.offerObject = offer
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        } else if type == .used {
            let str = "Reference number".localized
            let storyBoard = UIStoryboard(name: "App", bundle: nil)
            if let popVC = storyBoard.instantiateViewController(withIdentifier: "AlertPopUpVC") as? AlertPopUpVC {
                popVC.modalPresentationStyle = .overCurrentContext
                popVC.titleT = "This offer code is redeemed !".localized
                popVC.subTitleT = "\(str)\n\(offer?.order_detail?.reference_no ?? "")"
                popVC.popUpImage = UIImage(named: "icon refrence no")
                self.tabBarController?.present(popVC, animated: true)
            }
        }
    }
}
extension MyVouchersViewController: ReviewViewControllerDelegate {
    func goToHome(reviewed: Bool) {
        if reviewed {
            let alert = UIAlertController(title: "Reviews".localized, message: "Thank you for your valuable review.\nCheck out more exciting offers waiting for you !".localized, preferredStyle: UIAlertController.Style.alert)
            let ok = UIAlertAction(title: Key.Messages.OKTitle.localized, style: .default) { (action:UIAlertAction) in
                self.getVouchers()
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
