//
//  SettingsViewController.swift
//  Offeraty
//
//  Created by Fuad Waheed on 05/08/2021.
//

import UIKit
import LocalAuthentication

class SettingsViewController: BaseViewController {
    
    //MARK: UILabel Outlets
    @IBOutlet weak var enableTouchIdLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    //MARK: UISwitch Outlets
    @IBOutlet weak var touchIDSwitch: UISwitch!
    
    //MARK: UIImageView Outlets
    @IBOutlet weak var userImageView: UIImageView!
    
    //MARK: Button Outlet
    @IBOutlet weak var backButton: UIButton!
    
    let auth = BiometricIDAuth()
    var user = Cache.extractUserObject(keyCurrentUser)

    override func viewDidLoad() {
        super.viewDidLoad()

        touchIDSwitch.setOn(Utilities.getIsTouchIdEnabled(), animated: false)
        // Do any additional setup after loading the view.
        let bioMetricType = auth.biometricType()
        if bioMetricType == .touchID {
            enableTouchIdLabel.text = "Enable Touch id for Login".localized
        }
        else if bioMetricType == .faceID {
            enableTouchIdLabel.text = "Enable Face id for Login".localized
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar()
        touchIDSwitch.semanticContentAttribute = .forceLeftToRight
        user = Cache.extractUserObject(keyCurrentUser)
        userImageView.makeRoundedView()
        userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImageView.sd_setImage(with: URL(string: user?.user_image ?? ""),placeholderImage:dpPlaceHolder)
        userNameLabel.text = user?.user_name ?? "Guest".localized
        checkLangauge()
    }
    
    func checkLangauge() {
        if (Utilities.getIsArabic() ?? false) {
            self.backButton.setImage(backRight, for: .normal)
        } else {
            self.backButton.setImage(backLeft, for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar()
    }
    
    @IBAction func touchIdSwitchValueChanged(_ sender: UISwitch) {
    }
    
    
    @IBAction func touchIDSwitchBtn(_ sender: Any) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            var text = ""
            let bioMetricType = auth.biometricType()
            if bioMetricType == .touchID {
                text = "Login to enable Touch ID".localized
            }
            else if bioMetricType == .faceID {
                text = "Login to enable Face ID".localized
            }
            Utilities.showAlert(title: "", message: text, doneButtonTitle: Key.Messages.CloseTitle.localized, alertType: .Failure)
        } else {
            checkBiometricEligibilityAndEnable()
        }
    }
    
    // MARK: - Functions
    
    func checkBiometricEligibilityAndEnable() {
        
        self.view.endEditing(true)
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            if error == nil {
                if Utilities.getIsTouchIdEnabled() {
                    Utilities.setIsTouchIdEnabled(isTouchIdEnabled: false)
                    touchIDSwitch.setOn(false, animated: true)
                } else {
                    Utilities.setIsTouchIdEnabled(isTouchIdEnabled: true)
                    touchIDSwitch.setOn(true, animated: true)
                }
            } else {
                showTouchIdErrorPopUp()
            }
            
        } else {
            showTouchIdErrorPopUp()
        }
        
    }
    
    func showTouchIdErrorPopUp() {
        touchIDSwitch.setOn(false, animated: true)
        Utilities.setIsTouchIdEnabled(isTouchIdEnabled: false)
        let bioMetricType = auth.biometricType()
        var currentBioMetric = ""
        if bioMetricType == .touchID {
            currentBioMetric = "Touch ID".localized
        }
        else if bioMetricType == .faceID {
            currentBioMetric = "Face ID".localized
        }
        let notAText = "not available".localized
        let configText = "Your device is not configured for".localized
        let ac = UIAlertController(title: "\(currentBioMetric) \(notAText)", message: "\(configText) \(currentBioMetric).", preferredStyle: UIAlertController.Style.alert)
        ac.addAction(UIAlertAction(title: Key.Messages.CloseTitle.localized, style: .default))
        present(ac, animated: true)
    }

}
