//
//  AirMilesSinglteon.swift
//  AirMiles
//
//  Created by Waqar Khalid on 10/10/19.
//  Copyright © 2019 Merit Incentive. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Lottie

@objc class OfferatySingleton:NSObject {
  class var shared: OfferatySingleton {
        struct Singleton {
            static let instance = OfferatySingleton()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    class func sharedInstance() -> OfferatySingleton {
        return OfferatySingleton.shared
    }
   
    var hud = LottieHUD("loader", loop: true)
  
    var airMilesNavigationController = UINavigationController()
   
    func showHud() {
        hud.size = CGSize(width: 120 , height: 120)
        hud.showHUD()
    }
    func hideHud() {
        hud.stopHUD()
    }
    
    public lazy var alamoFireManager: Session? = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        configuration.urlCache = nil
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        let alamoFireManager = Alamofire.Session(configuration: configuration)
        return alamoFireManager
        
    }()
}
