//
//  MoreCell.swift
//  Esan
//
//  Created by Fuad Waheed on 26/07/2021.
//  Copyright © 2021 Macbook Pro. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {
    
    @IBOutlet weak var textL: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(image: UIImage?, text: String, index: Int) {
        let guestCheck = Cache.extractBool(kIsGuestLoggedIn)
        if (guestCheck ?? false) {
            if index == 2 {
                textL.text = "\(text) \(Bundle.main.releaseVersionNumberPretty)"
            } else {
                textL.text = text
            }
        } else {
            if index == 3 {
                textL.text = "\(text) \(Bundle.main.releaseVersionNumberPretty)"
            } else {
                textL.text = text
            }
        }
        imageV.image = image
    }

}
