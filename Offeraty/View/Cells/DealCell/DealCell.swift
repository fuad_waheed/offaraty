//
//  HomeCategory.swift
//  OAB
//
//  Created by God on 27/03/19.
//  Copyright © 2019 Keyur Italiya. All rights reserved.
//
import UIKit
protocol DealCellDelegate: AnyObject {
    func favBtnTapped(offer: Offers?)
}

class DealCell: UICollectionViewCell {

    //MARK: UIImage Outlets
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var ratingView: UIView!
    //MARK: UILabel Outlets
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var favBtn: UIButton!
    
    var offerObject: Offers?
    
    weak var delegate: DealCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadOffer(offer: Offers?) {
        self.offerObject = offer
        dealImageView.contentMode = .scaleToFill
        dealImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        var placeIm = imagePlaceHolder
        if (Utilities.getIsArabic() ?? false) {
            placeIm = imagePlaceHolderArabic
        }
        dealImageView.sd_setImage(with: URL(string: offer?.offer_image ?? ""),placeholderImage:placeIm)
        brandNameLabel.text = offer?.merchant?.business_name
        productNameLabel.text = offer?.offer_title
        categoryLabel.text = offer?.category?.name
        ratingLabel.text = String(format: "%.1f", Double(offer?.total_rating ?? 0))
        if offer?.favourite ?? false {
            favBtn.setImage(fav, for: .normal)
        } else {
            favBtn.setImage(notFav, for: .normal)
        }
    }
    
    @IBAction func favouriteButtonTap(_ sender: Any) {
        if delegate != nil {
            delegate?.favBtnTapped(offer: self.offerObject)
        }
    }
    
}
